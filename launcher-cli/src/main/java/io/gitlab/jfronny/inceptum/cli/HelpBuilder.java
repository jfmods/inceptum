package io.gitlab.jfronny.inceptum.cli;

import java.util.ArrayList;
import java.util.List;

public class HelpBuilder {
    private final StringBuilder builder;
    private final int level;
    private final List<String> upper;

    public HelpBuilder() {
        this.builder = new StringBuilder();
        this.level = 0;
        this.upper = List.of();
    }

    private HelpBuilder(int level, StringBuilder builder, List<String> upper) {
        this.upper = upper;
        this.builder = builder;
        this.level = level;
    }

    public String getResult() {
        return builder.length() > 0 ? builder.substring(1) : "";
    }

    public HelpBuilder beginSubcommand(String of) {
        List<String> newUpper = new ArrayList<>(upper);
        newUpper.add(of);
        return new HelpBuilder(level + 1, builder, List.copyOf(newUpper));
    }

    public void writeCommand(List<String> aliases, String help, String usage) {
        if (aliases.isEmpty()) return;
        String indent = " ".repeat(level * 2);
        builder.append('\n').append(indent).append("- ");
        for (int i = 0, aliasesSize = aliases.size(); i < aliasesSize; i++) {
            String alias = aliases.get(i);
            builder.append(alias);
            if (i < aliasesSize - 1)
                builder.append(", ");
        }
        builder.append(": ").append(help.replace("\n", "\n  " + indent));
        if (level == 0 && !usage.isBlank()) {
            StringBuilder usagePrefix = new StringBuilder("inceptum");
            for (String s : upper) {
                usagePrefix.append(" ").append(s);
            }
            usagePrefix.append(" ").append(aliases.get(0)).append(" ");
            builder.append("\n  ")
                    .append(indent)
                    .append("Usage: ")
                    .append(usagePrefix)
                    .append(usage.replace("\n", "\n  " + indent + usagePrefix));
        }
    }
}
