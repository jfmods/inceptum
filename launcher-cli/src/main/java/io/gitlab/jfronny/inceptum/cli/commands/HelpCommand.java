package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.inceptum.cli.*;
import io.gitlab.jfronny.inceptum.common.BuildMetadata;

import java.util.List;

public class HelpCommand extends Command {
    public HelpCommand() {
        super("Displays this screen", "[command]", "help");
    }

    @Override
    protected void invoke(CommandArgs args) {
        HelpBuilder help = new HelpBuilder();
        if (args.length == 0) {
            printHeader();
            System.out.println("\nCommands:");
            CliMain.COMMANDS_ROOT.buildHelp(help);
        } else {
            CommandResolution resolution = CliMain.COMMANDS_ROOT.resolve(args);
            if (resolution.resolvePath().isEmpty()) {
                System.err.println("Could not find command matching your input");
                invoke(new CommandArgs(List.of()));
                return;
            }
            printHeader();
            System.out.println("\nFound matching: \"" + String.join(" ", resolution.resolvePath()) + "\"");
            resolution.command().buildHelp(help);
        }
        System.out.println(help.getResult());
    }

    private static void printHeader() {
        System.out.println("Inceptum v" + BuildMetadata.VERSION);
    }
}
