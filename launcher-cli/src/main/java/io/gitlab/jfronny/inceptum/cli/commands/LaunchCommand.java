package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.inceptum.cli.*;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.api.account.AccountManager;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.launch.*;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Steps;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class LaunchCommand extends BaseInstanceCommand {
    private final boolean server;
    private final boolean restart;

    public LaunchCommand() {
        super("Launches an instance of the game (client by default). Non-blocking (batch commands will continue if this is ran)",
                "[game arguments...]",
                List.of("run", "instance.launch", "start"),
                List.of(
                        new LaunchCommand("Explicitly launch a client", "client", false, false),
                        new LaunchCommand("Launch a server", "server", true, false,
                                new LaunchCommand("Restart the server if it stops", "restart", true, true)
                        )
                ));
        this.server = false;
        this.restart = false;
    }

    private LaunchCommand(String help, String name, boolean server, boolean restart, Command... subCommands) {
        super(help, "[game arguments...]", List.of(name), Arrays.asList(subCommands));
        this.server = server;
        this.restart = restart;
    }

    @Override
    protected void invoke(CommandArgs args, Instance instance) throws IOException, LaunchException {
        if (instance.isSetupLocked()) {
            Utils.LOGGER.error("This instance is still being set up");
            return;
        }
        if (instance.isRunningLocked()) {
            Utils.LOGGER.error("This instance is already running");
            return;
        }
        if (args.length > 1) {
            InstanceMeta meta = instance.meta();
            meta.checkArguments();
            meta.arguments = meta.arguments
                    .withClient(Stream.concat(meta.arguments.client().stream(), args.after(0).stream()).toList())
                    .withServer(Stream.concat(meta.arguments.server().stream(), args.after(0).stream()).toList());
        }
        Steps.reDownload(instance, Steps.createProcessState());
        if (server) {
            InstanceLauncher.launch(instance, LaunchType.Server, restart, AccountManager.NULL_AUTH);
        } else {
            AccountManager.loadAccounts();
            InstanceLauncher.launchClient(instance);
        }
    }
}
