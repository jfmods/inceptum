package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.cli.Command;
import io.gitlab.jfronny.inceptum.cli.CommandArgs;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ListCommand extends Command {
    public ListCommand() {
        super("Lists all available instances", "", "list", "ls");
    }

    @Override
    protected void invoke(CommandArgs args) throws IOException {
        List<Path> paths = JFiles.list(MetaHolder.INSTANCE_DIR);
        if (paths.isEmpty()) System.out.println("No instances are currently present");
        for (Path path : paths) {
            if (!Files.exists(path.resolve(Instance.CONFIG_NAME))) {
                System.out.println("- Invalid instance: " + path + " (no instance metadata)");
                continue;
            }
            System.out.println("- \"" + path.getFileName().toString() + "\"");
            Instance instance;
            try {
                instance = InstanceList.read(path);
            } catch (IOException e) {
                Utils.LOGGER.error("  Could not load instance.json", e);
                continue;
            }
            if (instance.isSetupLocked()) {
                System.out.println("  Status: Setting up");
                continue;
            }
            System.out.println("  Status: " + (instance.isRunningLocked() ? "Running" : "Stopped"));
            System.out.println("  Version: " + instance.getGameVersion());
            if (instance.isFabric()) System.out.println("  Fabric Loader: " + instance.getLoaderVersion());
            if (instance.meta().java != null) System.out.println("  Custom Java: " + instance.meta().java);
            if (instance.meta().minMem != null || instance.meta().maxMem != null)
                System.out.println("  Memory:" + (instance.meta().minMem != null ? " Minimum: " + instance.meta().minMem : "")
                        + (instance.meta().maxMem != null ? " Maximum: " + instance.meta().maxMem : ""));
        }
    }
}
