package io.gitlab.jfronny.inceptum.cli;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public abstract class BaseInstanceCommand extends Command {
    protected BaseInstanceCommand(String help, String usage, List<String> aliases, List<Command> subCommands) {
        super(help, mutateUsage(usage), aliases, subCommands);
    }

    private static String mutateUsage(String usage) {
        StringBuilder sb = new StringBuilder();
        for (String s : usage.split("\n")) {
            if (s.isBlank())
                sb.append("\n<instance>");
            else
                sb.append("\n<instance> ").append(s);
        }
        return sb.substring(1);
    }

    @Override
    protected void invoke(CommandArgs args) throws Exception {
        if (args.length == 0) {
            Utils.LOGGER.error("You must specify an instance to commit in");
            return;
        }
        Instance instance;
        Path normalPath = Path.of(args.get(0));
        if (Files.exists(normalPath.resolve(Instance.CONFIG_NAME))) {
            instance = new Instance(normalPath, GC_InstanceMeta.deserialize(normalPath.resolve(Instance.CONFIG_NAME), GsonPreset.CONFIG));
        } else {
            Path instancePath = MetaHolder.INSTANCE_DIR.resolve(args.get(0)).normalize();
            if (!instancePath.startsWith(MetaHolder.INSTANCE_DIR)) {
                Utils.LOGGER.error("Specified instance path doesn't exist");
                return;
            }
            if (!Files.exists(instancePath)) {
                Utils.LOGGER.error("Invalid instance: \"" + args.get(0) + "\"");
                return;
            }
            try {
                instance = InstanceList.read(instancePath);
            } catch (IOException e) {
                Utils.LOGGER.error("Could not read instance metadata", e);
                return;
            }
        }
        invoke(args.subArgs(), instance);
    }

    protected abstract void invoke(CommandArgs args, Instance instance) throws Exception;
}
