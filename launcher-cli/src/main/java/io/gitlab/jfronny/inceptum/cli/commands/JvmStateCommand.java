package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.inceptum.cli.Command;
import io.gitlab.jfronny.inceptum.cli.CommandArgs;

import java.net.URLClassLoader;
import java.util.Arrays;

public class JvmStateCommand extends Command {
    public JvmStateCommand() {
        super("Displays information about the JVM state. For debugging", "", "jvmstate");
    }

    @Override
    protected void invoke(CommandArgs args) {
        System.out.println(System.getProperty("java.class.path"));
        dumpClasspath(JvmStateCommand.class.getClassLoader());
    }

    private static void dumpClasspath(ClassLoader loader) {
        System.out.println("Classloader " + loader + ":");

        if (loader instanceof URLClassLoader uc)
            System.out.println("\t" + Arrays.toString(uc.getURLs()));
        else
            System.out.println("\t(cannot display components as not a URLClassLoader)");

        if (loader.getParent() != null) dumpClasspath(loader.getParent());
    }
}
