package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.commons.logger.OutputColors;
import io.gitlab.jfronny.inceptum.cli.Command;
import io.gitlab.jfronny.inceptum.cli.CommandArgs;
import io.gitlab.jfronny.inceptum.launcher.system.importer.Importers;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.nio.file.Paths;
import java.util.List;

public class ImportCommand extends Command {
    public ImportCommand() {
        super("Import a CurseForge, Modrinth or MultiMC instance", "<pack file>", List.of("import"), List.of());
    }

    @Override
    protected void invoke(CommandArgs args) throws Exception {
        if (args.length == 0) throw new IllegalAccessException("You must specify a pack file");
        if (args.length != 1) throw new IllegalAccessException("Too many arguments");
        ProcessState state = new ProcessState();
        String name = Importers.importPack(Paths.get(args.get(0)), state).path().getFileName().toString();
        System.out.println(OutputColors.GREEN_BOLD + "Imported as " + name + OutputColors.RESET);
    }
}
