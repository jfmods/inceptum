package io.gitlab.jfronny.inceptum.cli;

import io.gitlab.jfronny.inceptum.cli.commands.*;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;

import java.util.*;

public class CliMain {
    public static Command DEFAULT = new HelpCommand();
    public static final List<Command> KNOWN_COMMANDS = new LinkedList<>(List.of(
            DEFAULT,
            new LaunchCommand(),
            new ListCommand(),
            new ModCommand(),
            new ImportCommand(),
            new ExportCommand(),
            new JvmStateCommand(),
            new BatchCommand()
    ));

    public static final Command COMMANDS_ROOT = new Command("Root command", "<command>", List.of(), KNOWN_COMMANDS) {
        @Override
        protected void invoke(CommandArgs args) {
            throw new RuntimeException("Could not find command: " + args.get(0));
        }

        @Override
        public CommandResolution resolve(CommandArgs args) {
            if (args.length == 0) return new CommandResolution(DEFAULT, args, new ArrayList<>());
            return super.resolve(args);
        }
    };

    public static CommandResolution resolve(String[] args) {
        return COMMANDS_ROOT.resolve(new CommandArgs(Arrays.asList(args)));
    }

    public static void main(String[] args) throws Exception {
        LauncherEnv.initialize(new CliEnvBackend());

        CommandResolution command = resolve(args);
        if (command.command().enableLog()) {
            Utils.LOGGER.info("Launching Inceptum v" + BuildMetadata.VERSION);
            Utils.LOGGER.info("Loading from " + MetaHolder.BASE_PATH);
        }

        try {
            command.invoke();
        } catch (Exception e) {
            Utils.LOGGER.error("Could not execute command", e);
        } finally {
            LauncherEnv.terminate();
        }
    }
}
