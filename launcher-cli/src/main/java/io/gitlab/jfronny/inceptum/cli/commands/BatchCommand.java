package io.gitlab.jfronny.inceptum.cli.commands;

import io.gitlab.jfronny.commons.ArgumentsTokenizer;
import io.gitlab.jfronny.inceptum.cli.*;
import io.gitlab.jfronny.inceptum.common.Utils;

import java.nio.file.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class BatchCommand extends Command {
    public BatchCommand() {
        super("Executes several commands specified in a text file in one setting", "<source file>", "batch");
    }

    @Override
    protected void invoke(CommandArgs args) throws Exception {
        if (args.length == 0) {
            Utils.LOGGER.error("Could not start batch execution: No source file specified");
            return;
        }
        Set<CommandResolution> resolved = new LinkedHashSet<>();
        for (String arg : args) {
            Path p = Paths.get(arg);
            if (!Files.exists(p)) {
                Utils.LOGGER.error("Could not find " + p);
                return;
            }
            if (Files.isDirectory(p)) {
                Utils.LOGGER.error("Path is a directory: " + p);
                return;
            }
            try {
                for (String line : Files.readAllLines(p)) {
                    CommandResolution resolution = CliMain.resolve(ArgumentsTokenizer.tokenize(line));
                    resolved.add(resolution);
                }
            } catch (Exception e) {
                Utils.LOGGER.error("Could not read file", e);
                return;
            }
        }
        for (CommandResolution resolution : resolved) resolution.invoke();
    }

    @Override
    public boolean enableLog() {
        return true;
    }
}
