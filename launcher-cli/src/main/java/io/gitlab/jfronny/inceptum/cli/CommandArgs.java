package io.gitlab.jfronny.inceptum.cli;

import java.util.*;

public class CommandArgs implements Iterable<String> {
    protected final List<String> args;
    public final int length;

    public CommandArgs(List<String> args) {
        this.args = List.copyOf(args);
        this.length = args.size();
    }

    public boolean contains(String param) {
        return args.contains(param.replaceAll("^[-/]*", "").toLowerCase(Locale.ROOT));
    }

    public String last() {
        return args.get(args.size() - 1);
    }

    public String get(int index) {
        return args.get(index);
    }

    public List<String> after(String param) {
        List<String> yes = null;
        for (String arg : args) {
            if (yes != null)
                yes.add(arg);
            else if (arg.equals(param.replaceAll("^[-/]*", "").toLowerCase(Locale.ROOT)))
                yes = new ArrayList<>();
        }
        return yes;
    }

    public List<String> after(int index) {
        return index + 1 < length ? args.subList(index + 1, length) : new ArrayList<>();
    }

    public List<String> getArgs() {
        return args;
    }

    @Override
    public Iterator<String> iterator() {
        return args.iterator();
    }

    public CommandArgs subArgs() {
        return new CommandArgs(args.subList(1, args.size()));
    }

    @Override
    public String toString() {
        return "[" + String.join(", ", args) + "]";
    }
}
