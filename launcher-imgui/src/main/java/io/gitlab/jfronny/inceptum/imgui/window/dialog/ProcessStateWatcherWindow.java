package io.gitlab.jfronny.inceptum.imgui.window.dialog;

import imgui.ImGui;
import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.throwable.ThrowingRunnable;
import io.gitlab.jfronny.inceptum.imgui.window.Window;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.util.function.Consumer;

public class ProcessStateWatcherWindow extends Window {
    private final ProcessState state;
    private boolean finished;
    public ProcessStateWatcherWindow(String title, String errorMessage, ProcessState state, ThrowingRunnable<?> executor) {
        this(title, errorMessage, state, executor, R::nop);
    }

    public ProcessStateWatcherWindow(String title, String errorMessage, ProcessState state, ThrowingRunnable<?> executor, Consumer<Throwable> onFail) {
        super(title);
        this.state = state;
        new Thread(() -> {
            try {
                executor.run();
            } catch (Throwable e) {
                state.cancel();
                onFail.accept(e);
                LauncherEnv.showError(errorMessage, e);
            } finally {
                finished = true;
                close();
            }
        }).start();
    }

    public ProcessState getState() {
        return state;
    }

    @Override
    public void draw() {
        ImGui.progressBar(state.getProgress());
        ImGui.textUnformatted(state.getCurrentStep());
        if (ImGui.button("Cancel")) {
            state.cancel();
            close();
        }
    }

    @Override
    public void close() {
        super.close();
        if (!finished) state.cancel();
    }

    @Override
    public boolean isCloseable() {
        return false;
    }
}
