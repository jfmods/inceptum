package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.ImGui;
import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.control.InstanceManageControls;
import io.gitlab.jfronny.inceptum.imgui.window.dialog.ProcessStateWatcherWindow;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.system.importer.Importers;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Steps;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class NewInstanceWindow extends Window {
    private final InstanceManageControls imc = new InstanceManageControls(null);

    public NewInstanceWindow() {
        super("New Instance");
    }

    @Override
    public void draw() {
        if (ImGui.beginTabBar("NewInstanceBar")) {
            if (ImGui.beginTabItem("New")) {
                imc.snapshotsBox();
                imc.versionBox(R::nop);
                imc.nameBox("OK", name -> {
                    try {
                        GuiUtil.createInstance(new SetupStepInfo(imc.getVersionInfo(),
                                imc.getLoaderInfo(),
                                name,
                                Steps.createProcessState()));
                    } catch (IOException e) {
                        Utils.LOGGER.error("Could not initialize instance creation", e);
                    }
                    close();
                });
                ImGui.endTabItem();
            }
            if (ImGui.beginTabItem("Import")) {
                ImGui.text("You can also just add an instance directory and it'll be loaded automatically");
                ImGui.text("Using git to manage it is recommended if you do so");
                ImGui.spacing();
                ImGui.text("Below, you can import a CurseForge, Modrinth or MultiMC pack");
                if (ImGui.button("Import")) {
                    List<Path> packs = GuiMain.openFileDialog("Import Pack", null, new String[]{"*.zip", "*.mrpack"}, "Modpack", true);
                    if (!packs.isEmpty()) {
                        ProcessState state = new ProcessState(Importers.MAX_STEPS * packs.size(), "Initializing");
                        GuiMain.open(new ProcessStateWatcherWindow("Importing", "Could not import packs", state, () -> {
                            for (Path pack : packs) {
                                if (state.isCancelled()) return;
                                Path imported = Importers.importPack(pack, state).path();
                                if (!state.isCancelled()) LauncherEnv.showInfo(pack.getFileName() + " has been successfully imported as " + imported.getFileName(), "Imported pack");
                            }
                        }));
                    }
                }
                ImGui.endTabItem();
            }
            ImGui.endTabBar();
        }
    }
}
