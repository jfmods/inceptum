package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.ImGui;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.api.account.*;

import java.net.URI;
import java.net.URISyntaxException;

public class MicrosoftLoginWindow extends Window {
    private final MicrosoftAuthServer server;

    public MicrosoftLoginWindow() {
        this(null);
    }

    public MicrosoftLoginWindow(MicrosoftAccount account) {
        super("Microsoft Login");

        this.server = new MicrosoftAuthServer(account);
        try {
            this.server.start();
        } catch (Exception e) {
            Utils.LOGGER.error("Could not start mc login server", e);
        }
    }

    @Override
    public void draw() {
        ImGui.text("This feature uses modified ATLauncher code, so the login prompt will ask you to log in to ATLauncher");
        ImGui.text("Click the button below to begin");
        if (ImGui.button("Open in Browser")) {
            try {
                Utils.openWebBrowser(new URI(MicrosoftAuthAPI.MICROSOFT_LOGIN_URL));
            } catch (URISyntaxException e) {
                Utils.LOGGER.error("Could not open browser", e);
            }
        }
    }

    @Override
    public void close() {
        super.close();
        server.close();
    }
}
