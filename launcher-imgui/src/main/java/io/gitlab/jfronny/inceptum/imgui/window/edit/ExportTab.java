package io.gitlab.jfronny.inceptum.imgui.window.edit;

import imgui.ImGui;
import imgui.type.ImString;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.control.Tab;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.imgui.window.dialog.ProcessStateWatcherWindow;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.system.exporter.Exporter;
import io.gitlab.jfronny.inceptum.launcher.system.exporter.Exporters;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.nio.file.Path;

public class ExportTab extends Tab {
    private final InstanceEditWindow window;
    private final ImString instanceVersion = new ImString("", GuiUtil.INPUT_FIELD_LENGTH);

    public ExportTab(InstanceEditWindow window) {
        super("Export");
        this.window = window;
        instanceVersion.set(window.instance.meta().instanceVersion);
    }

    @Override
    protected void renderInner() {
        if (ImGui.inputTextWithHint("Version", "Version of the exported Pack", instanceVersion)) {
            window.instance.meta().instanceVersion = instanceVersion.get();
            window.instance.writeMeta();
        }
        if (window.instance.mds().isComplete(ScanStage.DOWNLOAD)) {
            for (Exporter<?> exporter : Exporters.EXPORTERS) {
                if (ImGui.button(exporter.getName())) {
                    String defaultName = exporter.getDefaultFileName(window.instance);
                    String filter = "*." + exporter.getFileExtension();
                    Path exportPath = GuiMain.saveFileDialog("Export " + exporter.getName() + " Pack", defaultName, new String[]{filter}, exporter.getName() + " packs (" + filter + ")");
                    if (exportPath != null) {
                        ProcessState state = new ProcessState(Exporters.STEP_COUNT, "Initializing...");
                        GuiMain.open(new ProcessStateWatcherWindow("Exporting", "Could not export pack", state, () -> {
                            exporter.generate(state, window.instance, exportPath);
                            if (!state.isCancelled()) LauncherEnv.showInfo(window.instance.getName() + " has been successfully exported to " + exportPath, "Successfully exported");
                        }));
                    }
                }
            }
        } else {
            ImGui.text("The mods directory scan must be completed.\nThe progress for this can be observed in the mods tab");
        }
    }
}
