package io.gitlab.jfronny.inceptum.imgui.window.dialog;

import imgui.ImGui;
import imgui.type.ImString;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.imgui.window.Window;

import java.util.function.Consumer;

public class TextBoxWindow extends Window {
    private final String message;
    private final Consumer<String> onOk;
    private final Runnable onCancel;
    private final ImString selected;
    private boolean success = false;

    public TextBoxWindow(String name, String message, String defaultValue, Consumer<String> onOk, Runnable onCancel) {
        super(name);
        this.message = message;
        this.onOk = onOk;
        this.onCancel = onCancel;
        selected = new ImString(defaultValue, GuiUtil.INPUT_FIELD_LENGTH);
    }

    @Override
    public void draw() {
        ImGui.text(message);
        ImGui.inputText("##yes", selected);
        if (ImGui.button("OK")) {
            success = true;
            close();
        }
        if (onOk != null || onCancel != null) {
            ImGui.sameLine();
            if (ImGui.button("Cancel")) {
                success = false;
                close();
            }
        }
    }

    @Override
    public void close() {
        super.close();
        if (success) {
            if (onOk != null) onOk.accept(selected.get());
        } else {
            if (onCancel != null) onCancel.run();
        }
    }
}
