package io.gitlab.jfronny.inceptum.imgui.window.edit;

import imgui.ImGui;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.control.Tab;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.imgui.window.Window;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;

public class InstanceEditWindow extends Window {
    protected final Instance instance;
    protected final Closeable focus;
    private final List<Tab> tabs;
    protected boolean reDownload = false;
    protected boolean lastTabWasMods = false;

    public InstanceEditWindow(Instance instance) throws IOException {
        super(instance.getName() + " - Edit");
        this.instance = instance;
        this.focus = instance.mds().focus();
        this.instance.mds().start();
        this.tabs = List.of(
                new GeneralTab(this),
                new ArgumentsTab(this),
                new ModsTab(this),
                new ExportTab(this)
        );
    }

    @Override
    public void draw() {
        if (instance.isSetupLocked()) {
            ImGui.text("This instance is still being set up.");
            return;
        }
        if (instance.isRunningLocked()) {
            ImGui.text("This instance is running. Edits in this state will result in breakage.");
        }
        lastTabWasMods = false;
        if (ImGui.beginTabBar("InstanceEdit" + instance.id())) {
            for (Tab tab : tabs) tab.render();
            ImGui.endTabBar();
        }
    }

    @Override
    public void close() {
        super.close();
        try {
            focus.close();
        } catch (IOException e) {
            Utils.LOGGER.error("Could not release focus on MDS", e);
        }
        if (reDownload) {
            GuiUtil.reload(instance);
        }
    }

    @Override
    public int getFlags() {
        return lastTabWasMods ? 0 : super.getFlags();
    }
}
