package io.gitlab.jfronny.inceptum.imgui.window;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.window.dialog.ProcessStateWatcherWindow;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList;
import io.gitlab.jfronny.inceptum.launcher.system.setup.*;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class GuiUtil {
    public static final int INPUT_FIELD_LENGTH = 1024; // Arbitrary value, can be any int 0<=x<MAX_VALUE. Be aware that an array of this length will be allocated for every input box

    public static void reload() {
        try {
            List<Path> paths = JFiles.list(MetaHolder.INSTANCE_DIR);
            ProcessState state = Steps.createProcessState().extend(paths.size());
            GuiMain.open(new ProcessStateWatcherWindow("Reloading data", "Could not reload resources", state, () -> {
                InstanceList.reset();
                InstanceList.forEach(instance -> Steps.reDownload(instance, state));
            }));
        } catch (IOException e) {
            LauncherEnv.showError("Could not reload", e);
        }
    }

    public static void reload(Instance instance) {
        ProcessState state = Steps.createProcessState();
        GuiMain.open(new ProcessStateWatcherWindow("Reloading data", "Could not reload resources", state, () -> {
            Steps.reDownload(instance, state);
        }));
    }

    public static void createInstance(SetupStepInfo state) {
        ProcessState pState = Steps.createProcessState();
        pState.updateStep("Starting install process");
        GuiMain.open(new ProcessStateWatcherWindow("Creating Instance", "Could not create instance", pState, () -> {
            for (Step step : Steps.STEPS) {
                if (state.isCancelled()) {
                    state.tryRemoveInstance();
                    return;
                }
                pState.incrementStep(step.getName());
                step.execute(state);
            }
            state.clearSetupLock();
            InstanceList.read(MetaHolder.INSTANCE_DIR.resolve(state.name())).mds().start();
            LauncherEnv.showInfo("The instance was successfully created. You can now launch it using the main menu", "Successfully installed");
        }, t -> state.tryRemoveInstance()));
    }
}
