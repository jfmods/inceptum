package io.gitlab.jfronny.inceptum.imgui.window.edit;

import imgui.ImGui;
import imgui.type.ImBoolean;
import imgui.type.ImString;
import io.gitlab.jfronny.commons.OSUtils;
import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;
import io.gitlab.jfronny.inceptum.imgui.control.InstanceManageControls;
import io.gitlab.jfronny.inceptum.imgui.control.Tab;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class GeneralTab extends Tab {
    private final InstanceManageControls imc;
    private final InstanceEditWindow window;
    private final ImBoolean customJava;
    private final ImString customJavaPath = new ImString(GuiUtil.INPUT_FIELD_LENGTH);

    public GeneralTab(InstanceEditWindow window) {
        super("General");
        this.window = window;
        imc = new InstanceManageControls(window.instance);
        String java = window.instance.meta().java;
        customJava = new ImBoolean(java != null);
        if (java != null) customJavaPath.set(java);
    }

    @Override
    protected void renderInner() {
        if (ImGui.button("Open Directory")) {
            Utils.openFile(window.instance.path().toFile());
        }
        imc.nameBox("Rename", name -> {
            try {
                Path newPath = MetaHolder.INSTANCE_DIR.resolve(name);
                Files.move(window.instance.path(), newPath);
                GuiMain.open(new InstanceEditWindow(window.instance));
                window.close();
            } catch (IOException e) {
                LauncherEnv.showError("Could not rename", e);
            }
        });
        imc.snapshotsBox();
        imc.versionBox(ver -> {
            window.reDownload = true;
            window.instance.meta().gameVersion = ver;
            window.instance.writeMeta();
        });
        if (ImGui.button("Delete"))
            LauncherEnv.showOkCancel("This instance will be removed forever (a long time)", "Are you sure?", () -> {
                try {
                    JFiles.deleteRecursive(window.instance.path());
                } catch (IOException e) {
                    LauncherEnv.showError("Could not delete the instance", e);
                }
                window.close();
            });
        if (ImGui.checkbox("Custom Java", customJava)) {
            if (customJava.get()) {
                window.instance.meta().java = OSUtils.getJvmBinary();
                customJavaPath.set(window.instance.meta().java);
            } else {
                window.instance.meta().java = null;
            }
            window.instance.writeMeta();
        }
        if (customJava.get() && ImGui.inputText("Path", customJavaPath)) {
            window.instance.meta().java = customJavaPath.get();
            window.instance.writeMeta();
        }
    }
}
