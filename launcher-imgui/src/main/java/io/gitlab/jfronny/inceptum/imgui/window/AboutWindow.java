package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.ImGui;
import io.gitlab.jfronny.inceptum.common.BuildMetadata;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;

import java.net.URI;
import java.net.URISyntaxException;

public class AboutWindow extends Window {
    public AboutWindow() {
        super("About");
    }

    @Override
    public void draw() {
        ImGui.text("Inceptum " + BuildMetadata.VERSION + " Copyright (C) 2021-2023 JFronny");
        ImGui.text("This program comes with ABSOLUTELY NO WARRANTY.");
        ImGui.text("This is free software, and you are welcome to redistribute it under certain conditions.");

        ImGui.separator();

        int vm = Runtime.version().feature();
        if (!BuildMetadata.IS_PUBLIC) {
            ImGui.text("This is a custom build. No support will be provided");
        } else if (BuildMetadata.VM_VERSION != vm) {
            ImGui.text("This build was designed for Java " + BuildMetadata.VM_VERSION + ", but you are using Java " + vm + ". No support will be provided");
        } else {
            try {
                ImGui.text("For support, you can head to my ");
                ImGui.sameLine();
                if (ImGui.button("Matrix")) {
                    Utils.openWebBrowser(new URI("https://matrix.to/#/%23jfronny%3Amatrix.org"));
                }
                ImGui.sameLine();
                ImGui.text(" space");
            } catch (URISyntaxException e) {
                LauncherEnv.showError("Could not complete task", e);
            }
        }
    }
}
