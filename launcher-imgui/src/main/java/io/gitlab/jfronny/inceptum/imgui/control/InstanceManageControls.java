package io.gitlab.jfronny.inceptum.imgui.control;

import imgui.ImGui;
import imgui.type.*;
import io.gitlab.jfronny.commons.io.cache.MemoryOperationResultCache;
import io.gitlab.jfronny.commons.tuple.Tuple;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.imgui.window.GuiUtil;
import io.gitlab.jfronny.inceptum.launcher.api.FabricMetaApi;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricVersionLoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.*;
import io.gitlab.jfronny.inceptum.launcher.system.instance.*;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class InstanceManageControls {
    private static final VersionsList VERSIONS = McApi.getVersions();
    private static final MemoryOperationResultCache<Tuple<String, String>, VersionInfo> VERSION_INFO_CACHE = new MemoryOperationResultCache<>(Utils.CACHE_SIZE);

    private final ImInt version = new ImInt(0);
    private final ImString name = new ImString("", GuiUtil.INPUT_FIELD_LENGTH);
    private final ImInt fabricVersion = new ImInt(0);
    private final ImBoolean snapshots = new ImBoolean(InceptumConfig.snapshots);
    private final ImBoolean fabric = new ImBoolean(true);

    private VersionsListInfo selected;
    private FabricVersionLoaderInfo selectedFabric;

    public InstanceManageControls(@Nullable Instance instance) {
        selected = getVersions(false).get(0);
        if (instance != null) {
            for (VersionsListInfo ver : getVersions(true)) {
                if (ver.id.equals(instance.getGameVersion()))
                    selected = ver;
            }
        }
        version.set(getVersions(snapshots.get()).indexOf(selected));
        name.set(instance == null ? InstanceNameTool.getDefaultName(selected.id, fabric.get()) : instance.getName());
        fabric.set(instance == null || instance.isFabric());
        List<FabricVersionLoaderInfo> versions = FabricMetaApi.getLoaderVersions(selected);
        for (int i = 0, fabricLoaderInfoSize = versions.size(); i < fabricLoaderInfoSize; i++) {
            FabricVersionLoaderInfo version = versions.get(i);
            if (instance != null && instance.isFabric()
                    ? version.loader.version().equals(instance.getLoaderVersion())
                    : version.loader.stable()) {
                selectedFabric = version;
                fabricVersion.set(i);
                break;
            }
        }
    }

    public void nameBox(String okText, Consumer<String> ok) {
        ImGui.inputTextWithHint("Name", "Select a name", name);
        if (!Utils.VALID_FILENAME.matcher(name.get()).matches())
            ImGui.text("Invalid name");
        else if (Files.exists(MetaHolder.INSTANCE_DIR.resolve(name.get())))
            ImGui.text("Already exists");
        else if (ImGui.button(okText)) {
            ok.accept(name.get());
        }
    }

    public void snapshotsBox() {
        if (ImGui.checkbox("Show snapshots", snapshots)) {
            boolean prev = InceptumConfig.snapshots;
            InceptumConfig.snapshots = snapshots.get();
            InceptumConfig.saveConfig();
            //fix version index
            int i = getVersions(InceptumConfig.snapshots).indexOf(getVersions(prev).get(version.get()));
            if (i == -1) version.set(0);
            else version.set(i);
        }
    }

    public void versionBox(Consumer<String> modifiedVersion) {
        List<VersionsListInfo> vil = getVersions(InceptumConfig.snapshots);
        String originalStr = null;
        try {
            originalStr = getVersionInfo().id;
        } catch (Throwable e) {
            Utils.LOGGER.error("Could not get version string", e);
        }
        if (ImGui.combo("Version", version, vil.stream().map(info -> info.id).toArray(String[]::new))) {
            VersionsListInfo prev = selected;
            selected = vil.get(version.get());
            exchangeNameIfDefault(prev.id, fabric.get());
        }
        List<FabricVersionLoaderInfo> versions = FabricMetaApi.getLoaderVersions(selected);
        if (versions.isEmpty()) {
            fabric.set(false);
            exchangeNameIfDefault(selected.id, true);
        } else {
            if (ImGui.checkbox("Fabric support", fabric)) {
                exchangeNameIfDefault(selected.id, !fabric.get());
            }
            if (fabric.get()) {
                ImGui.sameLine();
                if (ImGui.combo("Loader", fabricVersion, versions.stream().map(info -> info.loader.version()).toArray(String[]::new))) {
                    selectedFabric = versions.get(fabricVersion.get());
                }
            }
        }
        try {
            if (originalStr != null && !originalStr.equals(getVersionInfo().id))
                modifiedVersion.accept(getVersionInfo().id);
        } catch (IOException e) {
            Utils.LOGGER.error("Could not compare version string", e);
        }
    }

    private void exchangeNameIfDefault(String id, boolean fabric) {
        if (InstanceNameTool.getDefaultName(id, fabric).equals(InstanceNameTool.namePart(this.name.get()))) {
            String defName = InstanceNameTool.getDefaultName(this.selected.id, this.fabric.get());
            try {
                defName = InstanceNameTool.getNextValid(defName);
            } catch (IOException e) {
                Utils.LOGGER.error("Could not find valid number suffix", e);
            }
            name.set(defName);
        }
    }

    public VersionInfo getVersionInfo() throws IOException {
        return VERSION_INFO_CACHE.get(Tuple.of(selected.id, fabric.get() ? selectedFabric.loader.version() : ""), () -> {
            VersionInfo vi = McApi.getVersionInfo(selected);
            if (fabric.get())
                vi = FabricMetaApi.addFabric(vi, selectedFabric.loader.version(), FabricMetaApi.FabricVersionInfoType.Both);
            return vi;
        });
    }

    public LoaderInfo getLoaderInfo() {
        return fabric.get() ? new LoaderInfo(selectedFabric.loader) : LoaderInfo.NONE;
    }

    private List<VersionsListInfo> getVersions(boolean snapshots) {
        ArrayList<VersionsListInfo> res = new ArrayList<>(VERSIONS.versions());
        res.removeIf(info -> !snapshots && !info.type.equals("release"));
        return res;
    }
}
