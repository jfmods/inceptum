package io.gitlab.jfronny.inceptum.imgui.control;

import imgui.ImGui;

public abstract class Tab {
    private final String name;

    public Tab(String name) {
        this.name = name;
    }

    protected abstract void renderInner();

    public void render() {
        if (isVisible() && ImGui.beginTabItem(name)) {
            renderInner();
            ImGui.endTabItem();
        }
    }

    protected boolean isVisible() {
        return true;
    }
}
