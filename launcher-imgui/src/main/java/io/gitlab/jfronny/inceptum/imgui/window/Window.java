package io.gitlab.jfronny.inceptum.imgui.window;

import imgui.flag.ImGuiWindowFlags;
import imgui.type.ImBoolean;
import io.gitlab.jfronny.inceptum.imgui.GuiMain;

import java.io.Closeable;

public abstract class Window implements Closeable {
    private final String name;
    private final ImBoolean openState = new ImBoolean(true);
    private State state = State.New;

    public Window(String name) {
        this.name = name;
    }

    public void preFirstDraw() {
    }

    public abstract void draw();

    public String getName() {
        return name;
    }

    @Override
    public void close() {
        state = State.Closed;
        openState.set(false);
        GuiMain.WINDOWS.remove(this);
    }

    public boolean isNew() {
        if (state == State.New) {
            state = State.Open;
            return true;
        }
        return false;
    }

    public boolean isClosed() {
        return state == State.Closed;
    }

    public int getFlags() {
        return ImGuiWindowFlags.AlwaysAutoResize;
    }

    public ImBoolean getOpenState() {
        return openState;
    }

    public boolean isCloseable() {
        return true;
    }

    public enum State {
        New, Open, Closed
    }
}
