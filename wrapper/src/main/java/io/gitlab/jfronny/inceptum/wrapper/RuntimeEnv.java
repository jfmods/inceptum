package io.gitlab.jfronny.inceptum.wrapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

public final class RuntimeEnv implements Runnable {
    public static void start(Iterable<Path> jars, String mainClass, String[] args) throws IOException, URISyntaxException {
        NioClassLoader loader = new NioClassLoader(jars, ClassLoader.getPlatformClassLoader());
        Thread th = new Thread(new RuntimeEnv(loader, mainClass, args), getThreadName(mainClass));
        th.setContextClassLoader(loader);
        th.start();
    }

    private static String getThreadName(String className) {
        String[] split = className.split("\\.");
        return "[WS/" + split[split.length - 1] + "]";
    }

    private final NioClassLoader loader;
    private final String mainClass;
    private final String[] args;
    private boolean ran = false;

    private RuntimeEnv(NioClassLoader loader, String mainClass, String[] args) {
        this.loader = loader;
        this.mainClass = mainClass;
        this.args = args;
    }

    @Override
    public synchronized void run() {
        if (ran) throw new IllegalStateException("Attempted to run environment which already finished executing");
        ran = true;
        try {
            wrapperInit();
            try {
                loader.loadClass(mainClass)
                        .getDeclaredMethod("main", String[].class)
                        .invoke(null, new Object[]{args});
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } finally {
            loader.close();
        }
    }

    private void wrapperInit() {
        try {
            loader.loadClass("io.gitlab.jfronny.inceptum.common.Utils")
                    .getMethod("wrapperInit", ClassLoader.class)
                    .invoke(null, ClassLoader.getSystemClassLoader());
        } catch (Throwable e) {
            System.out.println("Could not perform wrapper init, utils class probably moved");
            e.printStackTrace();
        }
    }
}
