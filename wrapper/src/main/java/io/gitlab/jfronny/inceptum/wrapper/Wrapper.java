package io.gitlab.jfronny.inceptum.wrapper;

import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.common.model.inceptum.*;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class Wrapper {
    public static void main(String[] args) throws IOException, URISyntaxException, Updater.UpdateCheckException {
        System.out.println("Inceptum Wrapper v" + BuildMetadata.VERSION);
        System.out.println("Loading from " + MetaHolder.BASE_PATH);
        InceptumEnvironmentInitializer.initialize();
        if (!Files.exists(MetaHolder.WRAPPER_CONFIG_PATH)) {
            UpdateMetadata update = Updater.check(UpdateChannel.Stable, false, true, R::nop);
            if (update == null) {
                throw new FileNotFoundException("Could not identify a valid inceptum version. Are you connected to the internet?");
            }
            Updater.update(update, false);
            if (!Files.exists(MetaHolder.WRAPPER_CONFIG_PATH)) {
                throw new FileNotFoundException("Something went wrong while downloading the latest version.");
            }
        }
        List<Path> classpath = Updater.getLaunchClasspath(GC_WrapperConfig.deserialize(MetaHolder.WRAPPER_CONFIG_PATH, GsonPreset.CONFIG));
        if (!BuildMetadata.IS_RELEASE) {
            System.out.println("Using classpath: " + classpath.stream().map(Path::toString).collect(Collectors.joining("" + File.pathSeparator)));
        }
        System.out.println("Starting Inceptum ClassLoader");
        RuntimeEnv.start(classpath, "io.gitlab.jfronny.inceptum.Inceptum", args);
    }
}
