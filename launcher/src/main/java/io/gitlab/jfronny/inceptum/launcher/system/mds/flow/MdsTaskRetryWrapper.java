package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;
import io.gitlab.jfronny.inceptum.launcher.system.mds.MdsMod;
import io.gitlab.jfronny.inceptum.launcher.util.ExponentialBackoff;

import java.io.IOException;
import java.util.Map;

public record MdsTaskRetryWrapper(Map<String, ExponentialBackoff> backoffs, ThrowingConsumer<MdsMod, IOException> inner) implements ThrowingConsumer<MdsMod, IOException> {
    @Override
    public void accept(MdsMod mod) throws IOException {
        String key = mod.getMetadataPath().getFileName().toString() + "/" + inner.getClass().getTypeName();
        ExponentialBackoff backoff = backoffs.computeIfAbsent(key, _ -> new ExponentialBackoff());
        backoff.sleep();
        try {
            inner.accept(mod);
            backoff.success();
        } catch (IOException e) {
            backoff.fail();
            throw e;
        }
    }
}
