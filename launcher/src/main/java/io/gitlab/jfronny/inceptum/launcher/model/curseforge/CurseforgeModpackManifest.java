package io.gitlab.jfronny.inceptum.launcher.model.curseforge;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Set;

@GSerializable
public record CurseforgeModpackManifest(Minecraft minecraft,
                                        String manifestType,
                                        int manifestVersion,
                                        String name,
                                        String version,
                                        String author,
                                        Set<File> files,
                                        String overrides) {
    @GSerializable
    public record Minecraft(String version, Set<ModLoader> modLoaders) {
        @GSerializable
        public record ModLoader(String id, boolean primary) {
        }
    }

    @GSerializable
    public record File(int projectID, int fileID, boolean required) {
    }
}
