package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.inceptum.common.GList;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.*;

import java.util.List;
import java.util.Set;

public class MinecraftArgumentAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(MinecraftArgument rules, Writer writer) throws TEx {
        throw new UnsupportedOperationException();
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> MinecraftArgument deserialize(Reader reader) throws TEx, MalformedDataException {
        if (reader.peek() == Token.STRING) return new MinecraftArgument(Set.of(reader.nextString()));
        Rules rules = null;
        List<String> value = null;
        reader.beginObject();
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "rules" -> rules = GC_Rules.deserialize(reader);
                case "value" -> value = GList.read(reader, Reader::nextString);
            }
        }
        reader.endObject();
        if (rules == null || value == null) throw new MalformedDataException("Not a valid minecraft argument");
        if (!rules.allow()) return new MinecraftArgument(Set.of());
        return new MinecraftArgument(Set.copyOf(value));
    }
}
