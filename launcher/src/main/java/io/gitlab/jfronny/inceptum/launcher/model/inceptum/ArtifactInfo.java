package io.gitlab.jfronny.inceptum.launcher.model.inceptum;

import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;

public record ArtifactInfo(String path, String sha1, int size, String url, boolean isNative) {
    public ArtifactInfo(VersionInfo.Library.Downloads.Artifact artifact, boolean isNative) {
        this(artifact.path, artifact.sha1, artifact.size, artifact.url, isNative);
    }
}
