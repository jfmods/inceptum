package io.gitlab.jfronny.inceptum.launcher.util;

import io.gitlab.jfronny.inceptum.common.Utils;

public class ProcessState {
    private final int maxSteps;
    private int stepIndex;
    private String stepDescription;
    private boolean isCancelled = false;

    public ProcessState() {
        this(0, "");
    }

    public ProcessState(int maxSteps, String defaultText) {
        this(maxSteps, defaultText, 0);
    }

    private ProcessState(int maxSteps, String stepDescription, int stepIndex) {
        if (maxSteps < 0) throw new IllegalArgumentException("maxSteps must be a positive integer");
        this.maxSteps = maxSteps;
        this.stepIndex = stepIndex;
        this.stepDescription = stepDescription;
    }

    public ProcessState extend(int multiplier) {
        return new ProcessState(maxSteps * multiplier, stepDescription, stepIndex);
    }

    public void incrementStep(String description) {
        this.stepIndex++;
        updateStep(description);
    }

    public void updateStep(String description) {
        this.stepDescription = description;
        Utils.LOGGER.info(description);
    }

    public String getCurrentStep() {
        return stepDescription;
    }

    public float getProgress() {
        return ((float) stepIndex) / maxSteps;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void cancel() {
        isCancelled = true;
    }
}
