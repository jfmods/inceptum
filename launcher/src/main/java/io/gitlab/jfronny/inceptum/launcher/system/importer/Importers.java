package io.gitlab.jfronny.inceptum.launcher.system.importer;

import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.api.CurseforgeApi;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Steps;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Importers {
    public static final int MAX_STEPS = Steps.STEPS.size() + 3;
    public static final CurseForgeImporter CURSE_FORGE = new CurseForgeImporter();
    public static final ModrinthImporter MODRINTH = new ModrinthImporter();
    public static final MultiMCImporter MULTI_MC = new MultiMCImporter();
    public static final List<Importer<?>> IMPORTERS = List.of(CURSE_FORGE, MODRINTH, MULTI_MC);

    public static Instance importPack(Path zipPath, ProcessState state) throws IOException {
        try (FileSystem fs = Utils.openZipFile(zipPath, false)) {
            for (Importer<?> importer : IMPORTERS) {
                if (importer.canImport(fs.getPath("."))) {
                    return importer.importPack(fs.getPath("."), state);
                }
            }
        }
        throw new IOException("Could not import pack: unsupported format");
    }

    private static final Pattern CURSEFORGE_URL = Pattern.compile("curseforge://install\\?addonId=(\\d+)&fileId=(\\d+)");

    public static Instance importPack(String url, ProcessState state) throws IOException {
        Path tmp = Files.createTempFile("inceptum", url.endsWith(".mrpack") ? ".mrpack" : ".zip");
        try {
            state.updateStep("Downloading Pack");
            Matcher m = CURSEFORGE_URL.matcher(url);
            if (m.matches()) {
                url = CurseforgeApi.getFile(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))).downloadUrl();
            }
            Net.downloadFile(url, tmp);
            return importPack(tmp, state);
        } catch (URISyntaxException e) {
            throw new IOException("Invalid URL", e);
        } finally {
            Files.deleteIfExists(tmp);
        }
    }
}
