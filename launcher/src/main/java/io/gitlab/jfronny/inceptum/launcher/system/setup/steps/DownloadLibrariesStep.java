package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.common.api.MavenApi;
import io.gitlab.jfronny.inceptum.common.model.maven.ArtifactMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ArtifactInfo;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;
import io.gitlab.jfronny.inceptum.launcher.util.*;
import org.xml.sax.SAXException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;

public class DownloadLibrariesStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        execute(info.version(), info.currentState());
    }

    @Override
    public String getName() {
        return "Downloading Libraries";
    }

    public static void execute(VersionInfo version, ProcessState currentState) throws IOException {
        for (ArtifactInfo artifact : VersionInfoLibraryResolver.getRelevant(version)) {
            if (currentState.isCancelled()) return;
            Path path = MetaHolder.LIBRARIES_DIR.resolve(artifact.path());
            if (!Files.exists(path)) {
                currentState.updateStep("Downloading library: " + artifact.path());
                if (!Files.exists(path.getParent())) Files.createDirectories(path.getParent());
                if (!artifact.url().endsWith(".jar")) {
                    Utils.LOGGER.info("Not a valid URL for a jar: " + artifact.url());
                    continue;
                }
                try {
                    Net.downloadFile(artifact.url(), artifact.sha1(), path);
                } catch (URISyntaxException e) {
                    throw new IOException("Could not download library", e);
                }
            }
            if (artifact.isNative()) {
                currentState.updateStep("Extracting natives");
                try (FileSystem libFs = Utils.openZipFile(path, false)) {
                    JFiles.copyRecursive(libFs.getPath("."), MetaHolder.NATIVES_DIR.resolve(GameVersionParser.getGameVersion(version.id)));
                } catch (Throwable t) {
                    Files.delete(path);
                    throw new IOException("Could not extract native", t);
                }
            }
        }

        ArtifactMeta artifact = getLaunchWrapperArtifact();
        if (!Files.exists(artifact.getLocalPath())) {
            try {
                MavenApi.downloadLibrary(Updater.PROJECT_MAVEN, MavenApi.getMetadata(Updater.PROJECT_MAVEN, artifact.getMavenNotation()));
            } catch (URISyntaxException | SAXException e) {
                throw new IOException("Could not download launchwrapper", e);
            }
        }
    }

    public static ArtifactMeta getLaunchWrapperArtifact() throws FileNotFoundException {
        String version = BuildMetadata.IS_PUBLIC ? BuildMetadata.VERSION : null;
        try {
            if (version == null) version = Updater.getUpdate(false, false).version();
        } catch (Updater.UpdateCheckException ignored) {
        }
        if (version == null) throw new FileNotFoundException("Could not find a valid launch wrapper version");
        return new ArtifactMeta("io.gitlab.jfronny.inceptum", "launchwrapper", version);
    }
}
