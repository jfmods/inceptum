package io.gitlab.jfronny.inceptum.launcher.model.microsoft.response;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public record LoginResponse(String username,
                            @SerializedName("access_token") String accessToken,
                            @SerializedName("token_type") String tokenType,
                            @SerializedName("expires_in") Integer expiresIn) {
}
