package io.gitlab.jfronny.inceptum.launcher.system.source;

import java.io.IOException;

public class DistributionDisabledException extends IOException {
    public DistributionDisabledException(String mod) {
        super("The author of the mod \"" + mod + "\" has chosen to deliberately break your ability of downloading it.\n"
                + "Please let them know that disabling third party downloads does nothing but make the users life harder for no reason.");
    }
}
