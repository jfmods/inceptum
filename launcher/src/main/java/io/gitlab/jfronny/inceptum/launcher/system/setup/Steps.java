package io.gitlab.jfronny.inceptum.launcher.system.setup;

import io.gitlab.jfronny.inceptum.launcher.api.FabricMetaApi;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionsListInfo;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.LoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.steps.*;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.util.*;

public class Steps {
    public static Set<Step> STEPS = new LinkedHashSet<>(List.of(
            new SetupDirsStep(),
            new WriteMetadataStep(),
            new DownloadJavaStep(),
            new DownloadClientStep(),
            new DownloadLibrariesStep(),
            new DownloadAssetsStep(),
            new RunMdsStep()
    ));

    public static ProcessState createProcessState() {
        return new ProcessState(STEPS.size(), "Initializing");
    }

    public static void reDownload(Instance instance, ProcessState state) throws IOException {
        if (instance.isLocked()) return;
        try {
            boolean found = false;
            for (VersionsListInfo version : McApi.getVersions().versions()) {
                if (version.id.equals(instance.getGameVersion())) {
                    found = true;
                    VersionInfo vi = McApi.getVersionInfo(version);
                    if (instance.isFabric())
                        vi = FabricMetaApi.addFabric(vi, instance.getLoaderVersion(), FabricMetaApi.FabricVersionInfoType.Both);
                    LoaderInfo li = instance.isFabric()
                            ? new LoaderInfo(LoaderInfo.Type.Fabric, instance.getLoaderVersion())
                            : LoaderInfo.NONE;
                    SetupStepInfo info = new SetupStepInfo(vi, li, instance.getName(), state);
                    for (Step step : Steps.STEPS) {
                        state.incrementStep(step.getName());
                        step.execute(info);
                        if (state.isCancelled()) return;
                    }
                }
            }
            if (!found) throw new IOException("Could not identify minecraft version " + instance.getGameVersion());
        } finally {
            instance.setSetupLock(false);
        }
    }
}
