package io.gitlab.jfronny.inceptum.launcher.system.setup;

import java.io.IOException;

public interface Step {
    void execute(SetupStepInfo info) throws IOException;
    String getName();
}
