package io.gitlab.jfronny.inceptum.launcher.system.instance;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ModsDirScanner;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModDownload;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.nio.file.Path;

public class ModManager {
    public static DownloadMeta download(ModSource ms, Path metaFile, ModsDirScanner mds) throws IOException {
        for (Mod value : mds.getMods()) {
            for (ModSource source : value.getMetadata().sources().keySet()) {
                if (ms.equals(source)) {
                    return new DownloadMeta(new ModDownload(value.getMetadata().sha1(), value.getMetadata().murmur2(), value.getJarPath()), value.getMetadata(), source, metaFile);
                }
            }
        }
        ModDownload md = ms.download();
        ModMeta manifest = ModMeta.fromDownload(md, ms, mds.getGameVersion());
        for (ModSource depSrc : ms.getDependencies(mds.getGameVersion())) {
            DownloadMeta depMeta = download(depSrc, metaFile.getParent().resolve(depSrc.getShortName() + ModPath.EXT_IMOD), mds);
            depMeta.meta.dependents().add(metaFile.getFileName().toString());
            manifest.dependencies().add(depMeta.metaFile.getFileName().toString());
            depMeta.write();
        }
        return new DownloadMeta(md, manifest, ms, metaFile);
    }

    public record DownloadMeta(ModDownload download, ModMeta meta, ModSource source, Path metaFile) {
        public void write() throws IOException {
            GC_ModMeta.serialize(meta, metaFile, GsonPreset.CONFIG);
        }
    }
}
