package io.gitlab.jfronny.inceptum.launcher.system.mds.noop;

import io.gitlab.jfronny.commons.ref.R;
import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.util.VoidClaimPool;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.function.BiConsumer;

public record NoopMds(String gameVersion) implements ModsDirScanner {
    @Override
    public void close() throws IOException {
    }

    @Override
    public boolean isComplete(ScanStage stage) {
        return true;
    }

    @Override
    public void start() {
    }

    @Override
    public Closeable focus() {
        return R::nop;
    }

    @Override
    public String getGameVersion() {
        return gameVersion;
    }

    @Override
    public Set<Mod> getMods() throws IOException {
        return Set.of();
    }

    @Override
    public Mod get(Path path) {
        return new NoopMod(path);
    }

    @Override
    public void invalidate(Path path) {
    }

    @Override
    public boolean hasScanned(Path path) {
        return true;
    }

    @Override
    public void runOnce(ScanStage targetStage, ThrowingBiConsumer<Path, Mod, IOException> discovered) {
    }
}
