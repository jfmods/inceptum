package io.gitlab.jfronny.inceptum.launcher.util;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.*;

public class FileBackedRef<T> implements Closeable {
    private final Path filePath;
    private final ThrowingFunction<Path, T, IOException> read;
    private final WatchService service;
    private T cache = null;

    public FileBackedRef(Path filePath, ThrowingFunction<Path, T, IOException> read) throws IOException {
        this.filePath = filePath;
        this.read = read;
        this.service = FileSystems.getDefault().newWatchService();
        filePath.getParent().register(service, StandardWatchEventKinds.ENTRY_MODIFY);
    }

    public FileBackedRef(Path filePath, ThrowingFunction<JsonReader, T, IOException> read, JsonTransport transport) throws IOException {
        this(filePath, path -> {
            try (JsonReader reader = transport.createReader(Files.newBufferedReader(path))) {
                return read.apply(reader);
            }
        });
    }

    public T get() throws IOException {
        WatchKey key = service.poll();
        boolean update = cache == null;
        if (key != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                update |= event.context().equals(filePath);
            }
            key.reset();
        }
        if (update) cache = read.apply(filePath);
        return cache;
    }

    @Override
    public void close() throws IOException {
        service.close();
    }
}
