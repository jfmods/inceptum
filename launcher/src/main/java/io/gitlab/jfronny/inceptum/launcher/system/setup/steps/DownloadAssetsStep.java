package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DownloadAssetsStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        Path o = MetaHolder.ASSETS_DIR.resolve("objects");
        try {
            for (var entry : McApi.getAssetIndex(info.version()).objects().entrySet()) {
                if (info.isCancelled()) return;
                Path fPath = o.resolve(entry.getValue().hash().substring(0, 2));
                if (!Files.exists(fPath)) Files.createDirectories(fPath);
                fPath = fPath.resolve(entry.getValue().hash());
                if (Files.exists(fPath)) continue;
                info.setState("Downloading asset: " + entry.getKey());
                McApi.downloadAsset(entry.getValue(), fPath);
            }
        } catch (Throwable e) {
            throw new IOException("Could not download assets", e);
        }
    }

    @Override
    public String getName() {
        return "Downloading Assets";
    }
}
