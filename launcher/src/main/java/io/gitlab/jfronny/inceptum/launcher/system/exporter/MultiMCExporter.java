package io.gitlab.jfronny.inceptum.launcher.system.exporter;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.multimc.GC_MMCPackMeta;
import io.gitlab.jfronny.inceptum.launcher.model.multimc.MMCPackMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;

public class MultiMCExporter extends Exporter<MMCPackMeta> {
    public MultiMCExporter() {
        super("MultiMC", "zip", ".minecraft");
    }

    @Override
    protected MMCPackMeta generateManifests(Path root, Instance instance, String version) throws IOException {
        {
            Files.writeString(root.resolve("instance.cfg"), String.format("""
                    ForgeVersion=
                    InstanceType=OneSix
                    IntendedVersion=
                    JoinServerOnLaunch=false
                    LWJGLVersion=
                    LiteloaderVersion=
                    LogPrePostOutput=true
                    MCLaunchMethod=LauncherPart
                    OverrideCommands=false
                    OverrideConsole=false
                    OverrideGameTime=false
                    OverrideJavaArgs=false
                    OverrideJavaLocation=false
                    OverrideMCLaunchMethod=false
                    OverrideMemory=false
                    OverrideNativeWorkarounds=false
                    OverrideWindow=false
                    iconKey=default
                    lastLaunchTime=%s
                    lastTimePlayed=0
                    name=%s
                    notes=
                    totalTimePlayed=0
                    """, Instant.now().toEpochMilli(), instance.getName()));
        }
        {
            MMCPackMeta manifest = new MMCPackMeta(new ArrayList<>(), 1);
            //TODO get version automatically
            manifest.components().add(new MMCPackMeta.Component(true, false, "org.lwjgl3", "3.2.2"));
            manifest.components().add(new MMCPackMeta.Component(false, true, "net.minecraft", instance.getGameVersion()));
            if (instance.isFabric()) {
                manifest.components().add(new MMCPackMeta.Component(true, false, "net.fabricmc.intermediary", instance.getGameVersion()));
                manifest.components().add(new MMCPackMeta.Component(false, false, "net.fabricmc.fabric-loader", instance.getLoaderVersion()));
            }
            GC_MMCPackMeta.serialize(manifest, root.resolve("mmc-pack.json"), GsonPreset.API);
            return manifest;
        }
    }

    @Override
    protected void addMods(Path root, Instance instance, Iterable<Mod> mods, MMCPackMeta mmcPackMeta, Path modsOverrides) throws IOException {
        Files.createDirectories(modsOverrides);
        for (Mod mod : mods) Files.copy(mod.getJarPath(), modsOverrides.resolve(mod.getJarPath().getFileName().toString()));
    }
}
