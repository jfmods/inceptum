package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.MojangFileDownload;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;

public class DownloadJavaStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        VersionInfo.JavaVersion ver = info.version().javaVersion;
        Path jvmDir = MetaHolder.NATIVES_DIR.resolve(ver.component()).resolve(Integer.toString(ver.majorVersion()));
        if (Files.exists(jvmDir)) return;
        info.setState("Downloading JVM");
        Files.createDirectories(jvmDir);
        for (var entry : McApi.getJvm(ver.component(), ver.majorVersion()).entrySet()) {
            Path tPath = jvmDir.resolve(entry.getKey());
            switch (entry.getValue().type()) {
                case "file" -> {
                    MojangFileDownload mf = entry.getValue().downloads().raw();
                    info.setState("jvm: Downloading " + tPath);
                    try {
                        Net.downloadFile(mf.url, mf.sha1, tPath);
                    } catch (URISyntaxException e) {
                        throw new IOException("Could not download jvm", e);
                    }
                    if (entry.getValue().executable()) {
                        if (!tPath.toFile().setExecutable(true)) info.setState("Could not set executable bit for " + tPath);
                    }
                }
                case "directory" -> Files.createDirectories(tPath);
            }
        }
    }

    @Override
    public String getName() {
        return "Downloading Java";
    }
}
