package io.gitlab.jfronny.inceptum.launcher.model.inceptum;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;
import java.util.Objects;

@GSerializable
public class InstanceMeta {
    public String instanceVersion = "1.0";
    public String gameVersion;
    public String java;
    public Long minMem;
    public Long maxMem;
    public Long lastLaunched;
    public Arguments arguments;

    public void setVersion(String version) {
        this.gameVersion = version;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (InstanceMeta) obj;
        return Objects.equals(this.gameVersion, that.gameVersion) &&
                Objects.equals(this.java, that.java) &&
                Objects.equals(this.minMem, that.minMem) &&
                Objects.equals(this.maxMem, that.maxMem) &&
                Objects.equals(this.lastLaunched, that.lastLaunched) &&
                Objects.equals(this.arguments, that.arguments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gameVersion, java, minMem, maxMem, lastLaunched, arguments);
    }

    public void checkArguments() {
        arguments = Arguments.checked(arguments);
    }

    @GSerializable
    public record Arguments(List<String> jvm, List<String> client, List<String> server) {
        public static final Arguments EMPTY = new Arguments(List.of(), List.of(), List.of());

        @GPrefer
        public Arguments {}

        public static Arguments checked(Arguments of) {
            if (of == null) return EMPTY;
            return new Arguments(
                    of.jvm == null ? List.of() : of.jvm,
                    of.client == null ? List.of() : of.client,
                    of.server == null ? List.of() : of.server
            );
        }

        public Arguments withJvm(List<String> jvm) {
            return new Arguments(jvm, client, server);
        }

        public Arguments withClient(List<String> client) {
            return new Arguments(jvm, client, server);
        }

        public Arguments withServer(List<String> server) {
            return new Arguments(jvm, client, server);
        }
    }
}
