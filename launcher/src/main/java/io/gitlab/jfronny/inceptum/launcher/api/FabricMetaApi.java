package io.gitlab.jfronny.inceptum.launcher.api;

import io.gitlab.jfronny.commons.io.cache.MemoryOperationResultCache;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.inceptum.common.GList;
import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.common.model.maven.ArtifactMeta;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricVersionLoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricVersionLoaderInfo.WithMeta.LauncherMeta.Libraries.Library;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.GC_FabricVersionLoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.*;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

public class FabricMetaApi {
    private static final String META_URL = "https://meta.fabricmc.net/";
    private static final MemoryOperationResultCache<VersionsListInfo, List<FabricVersionLoaderInfo>> LOADER_VERSIONS_CACHE = new MemoryOperationResultCache<>(Utils.CACHE_SIZE);

    public static List<FabricVersionLoaderInfo> getLoaderVersions(VersionsListInfo version) {
        try {
            return LOADER_VERSIONS_CACHE.get(version, () -> {
                return Net.downloadJObject(META_URL + "v2/versions/loader/" + version.id, s -> {
                    return GList.read(s, GC_FabricVersionLoaderInfo::deserialize);
                });
            });
        } catch (IOException e) {
            throw new RuntimeException("Could not get fabric loader versions", e);
        }
    }

    public static FabricVersionLoaderInfo getLoaderVersion(String gameVersion, String fabricVersion) throws IOException {
        return Net.downloadJObject(META_URL + "v2/versions/loader/" + gameVersion + "/" + fabricVersion, GC_FabricVersionLoaderInfo.WithMeta::deserialize);
    }

    public static VersionInfo addFabric(VersionInfo version, String fabricVersion, FabricVersionInfoType type) throws IOException {
        VersionInfo result = version.clone();
        FabricVersionLoaderInfo ver = getLoaderVersion(version.id, fabricVersion);
        if (!(ver instanceof FabricVersionLoaderInfo.WithMeta verWithMeta))
            throw new IOException("Doesn't hold metadata");
        FabricVersionLoaderInfo.WithMeta.LauncherMeta meta = verWithMeta.launcherMeta;
        if (meta.version() != 1) throw new IOException("Unsupported fabric launcherMeta version: " + meta.version());
        result.mainClass = type == FabricVersionInfoType.Server ? meta.mainClass().server() : meta.mainClass().client();
        List<VersionInfo.Library> libs = new ArrayList<>(version.libraries);
        for (Library library : meta.libraries().common())
            libs.add(convertLib(library));
        if (type == FabricVersionInfoType.Client || type == FabricVersionInfoType.Both) {
            for (Library library : meta.libraries().client())
                libs.add(convertLib(library));
        }
        if (type == FabricVersionInfoType.Server || type == FabricVersionInfoType.Both) {
            for (Library library : meta.libraries().server())
                libs.add(convertLib(library));
        }
        libs.add(convertLib(new Library(
                "net.fabricmc:fabric-loader:" + fabricVersion,
                "https://maven.fabricmc.net/"
        )));
        libs.add(convertLib(new Library(
                ver.intermediary.maven(),
                "https://maven.fabricmc.net/"
        )));
        result.libraries = List.copyOf(libs);
        result.id = GameVersionParser.createVersionWithFabric(version.id, fabricVersion);
        return result;
    }

    private static VersionInfo.Library convertLib(Library library) {
        String path = ArtifactMeta.parse(library.name()).getJarPath(true);
        return new VersionInfo.Library(
                new VersionInfo.Library.Downloads(
                        new VersionInfo.Library.Downloads.Artifact(
                                path,
                                -1,
                                null,
                                library.url() + path
                        ),
                        null
                ),
                library.name(),
                new HashMap<>(),
                new Rules(true)
        );
    }

    public enum FabricVersionInfoType {
        Client, Server, Both
    }
}
