package io.gitlab.jfronny.inceptum.launcher.system.mds;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricModJson;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModManager;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class MdsMod extends Mod {
    private final ProtoInstance instance;
    private final Path imodPath;
    private final Path initialJarPath;
    private final ModMeta meta;
    private @NotNull ScanStage scanStage = ScanStage.DISCOVER;
    private @Nullable DownloadAux downloadAux = null;

    public MdsMod(ProtoInstance instance, Path imodPath, Path initialJarPath, ModMeta meta) {
        this.instance = instance;
        this.imodPath = imodPath;
        this.initialJarPath = initialJarPath;
        this.meta = meta;
    }

    private record DownloadAux(Path imodPath, Path jarPath, boolean managedJar, @Nullable FabricModJson fmj) {}

    public void markDownloaded(Path imodPath, Path jarPath, boolean managedJar, @Nullable FabricModJson fmj) {
        this.downloadAux = new DownloadAux(imodPath, jarPath, managedJar, fmj);
        this.scanStage = ScanStage.DOWNLOAD;
    }

    public void markCrossReferenced() {
        if (!this.scanStage.contains(ScanStage.CROSSREFERENCE)) this.scanStage = ScanStage.CROSSREFERENCE;
    }

    public void markUpdateChecked() {
        if (!this.scanStage.contains(ScanStage.UPDATECHECK)) this.scanStage = ScanStage.UPDATECHECK;
    }

    private Optional<FabricModJson> getFmj() {
        return Optional.ofNullable(downloadAux).map(DownloadAux::fmj);
    }

    private DownloadAux requireDownload() {
        if (downloadAux == null) throw new NotYetScannedException(ScanStage.DOWNLOAD, getScanStage());
        return downloadAux;
    }

    @Override
    public String getName() {
        return getFmj().map(fmj -> {
            String version = fmj.version() == null ? "" : ' ' + fmj.version();
            if (fmj.name() != null) return fmj.name() + version;
            if (fmj.id() != null) return fmj.id() + version;
            return null;
        }).orElse(imodPath.getFileName().toString());
    }

    @Override
    public String[] getDescription() {
        return getFmj().map(fmj -> {
            if (fmj.description() != null) return fmj.description().split("\n");
            return new String[]{"No description is available", "This mods' fabric.mod.json has no description"};
        }).orElse(new String[]{"No description is available", "You may need to wait for its metadata to be scanned"});
    }

    @Override
    public boolean getNeedsInject() {
        return requireDownload().managedJar;
    }

    @Override
    public Path getJarPath() {
        return downloadAux == null ? initialJarPath : requireDownload().jarPath;
    }

    @Override
    public Path getMetadataPath() {
        return downloadAux == null ? imodPath : requireDownload().imodPath;
    }

    @Override
    public ScanStage getScanStage() {
        return scanStage;
    }

    @Override
    public void delete() throws IOException {
        Files.delete(imodPath);
        if (downloadAux != null && !downloadAux.managedJar) Files.delete(downloadAux.jarPath);
        scanStage = ScanStage.NONE;
        downloadAux = null;
        if (meta != null) {
            for (String dependency : meta.dependencies()) {
                instance.mds().get(instance.modsDir().resolve(dependency)).removeDependent(this);
            }
            for (String dependent : meta.dependents()) {
                instance.mds().get(instance.modsDir().resolve(dependent)).removeDependency(this);
            }
        }
    }

    @Override
    public Path update(ModSource update) throws IOException {
        ModManager.DownloadMeta download = ModManager.download(update, imodPath, instance.mds());
        download.write();
        for (String dependency : meta.dependencies()) {
            if (!download.meta().dependencies().contains(dependency)) {
                instance.mds().get(instance.modsDir().resolve(dependency)).removeDependent(this);
            }
        }
        instance.mds().invalidate(this);
        return download.metaFile();
    }

    @Override
    public Set<Mod> getDependencies() {
        return meta.dependencies().stream()
                .map(instance.modsDir()::resolve)
                .map(instance.mds()::get)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public Set<Mod> getDependents() {
        return meta.dependents().stream()
                .map(instance.modsDir()::resolve)
                .map(instance.mds()::get)
                .collect(Collectors.toUnmodifiableSet());
    }

    @Override
    public void removeDependency(Mod dependency) throws IOException {
        meta.dependencies().remove(meta.dependencies().stream()
                .filter(s -> dependency.equals(instance.mds().get(instance.modsDir().resolve(s))))
                .findFirst()
                .orElseThrow(FileNotFoundException::new));
        write();
    }

    @Override
    public void removeDependent(Mod dependent) throws IOException {
        meta.dependents().remove(meta.dependents().stream()
                .filter(s -> dependent.equals(instance.mds().get(instance.modsDir().resolve(s))))
                .findFirst()
                .orElseThrow(FileNotFoundException::new));
        write();
        if (!meta.explicit() && meta.dependents().isEmpty()) delete();
    }

    @Override
    public ModMeta getMetadata() {
        return meta;
    }

    @Override
    public boolean isEnabled() {
        return ModPath.isEnabled(imodPath);
    }

    private void write() throws IOException {
        GC_ModMeta.serialize(meta, imodPath, GsonPreset.CONFIG);
    }
}
