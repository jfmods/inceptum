package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.List;

@GSerializable
public record ModrinthSearchResult(List<ModResult> hits, int offset, int limit, int total_hits) {
    @GSerializable
    public record ModResult(
            String project_id,
            ModrinthProjectType project_type,
            String slug,
            String author,
            String title,
            String description,
            List<String> categories,
            List<String> versions,
            long downloads,
            long follows,
            String icon_url,
            Date date_created,
            Date date_modified,
            String latest_version,
            String license,
            String client_side,
            String server_side,
            List<String> gallery
    ) {
    }
}
