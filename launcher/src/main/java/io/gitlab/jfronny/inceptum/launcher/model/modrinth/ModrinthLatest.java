package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import org.jetbrains.annotations.Nullable;

public record ModrinthLatest(@Nullable ModrinthVersion stable, @Nullable ModrinthVersion beta, @Nullable ModrinthVersion latest) {
    public @Nullable ModrinthVersion get(ModrinthVersion.VersionType type) {
        return switch (type) {
            case alpha -> latest;
            case beta -> beta;
            case release -> stable;
        };
    }

    public @Nullable ModrinthVersion getBest() {
        if (stable != null) return stable;
        if (beta != null) return beta;
        if (latest != null) return latest;
        return null;
    }
}
