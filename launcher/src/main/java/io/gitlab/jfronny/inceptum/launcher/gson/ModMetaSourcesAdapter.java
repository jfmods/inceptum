package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta.Sources;
import io.gitlab.jfronny.inceptum.launcher.system.source.GC_ModSource;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.util.Optional;

public class ModMetaSourcesAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(Sources value, Writer writer) throws TEx, MalformedDataException {
        writer.beginArray();
        for (ModSource source : value.keySet()) GC_ModSource.serialize(source, writer);
        writer.endArray();
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> Sources deserialize(Reader reader) throws TEx, MalformedDataException {
        reader.beginArray();
        Sources sources = new Sources();
        while (reader.hasNext()) {
            sources.put(GC_ModSource.deserialize(reader), Optional.empty());
        }
        reader.endArray();
        return sources;
    }
}
