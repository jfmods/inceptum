package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.*;

@SuppressWarnings("MethodDoesntCallSuperMethod")
@GSerializable
public class VersionInfo extends VersionsListInfo implements Cloneable {
    public Arguments arguments;
    public AssetIndex assetIndex;
    public String assets;
    public Downloads downloads;
    public JavaVersion javaVersion;
    public List<Library> libraries;
    public String mainClass;
    public String minecraftArguments;
    public Integer minimumLauncherVersion;

    @Override
    public VersionInfo clone() {
        VersionInfo clone = new VersionInfo();
        clone.copyFrom(this);
        if (arguments != null) clone.arguments = arguments.clone();
        if (assetIndex != null) clone.assetIndex = assetIndex.clone();
        clone.assets = assets;
        if (downloads != null) clone.downloads = downloads.clone();
        if (javaVersion != null) clone.javaVersion = javaVersion.clone();
        clone.libraries = new LinkedList<>();
        if (libraries != null) libraries.forEach(library -> clone.libraries.add(library.clone()));
        clone.mainClass = mainClass;
        clone.minecraftArguments = minecraftArguments;
        clone.minimumLauncherVersion = minimumLauncherVersion;
        return clone;
    }

    @GSerializable
    public record Arguments(List<MinecraftArgument> game, List<MinecraftArgument> jvm) implements Cloneable {
        @Override
        public Arguments clone() {
            Arguments clone = new Arguments(new LinkedList<>(), new LinkedList<>());
            if (game != null) game.forEach(argument -> clone.game.add(argument.clone()));
            if (jvm != null) jvm.forEach(argument -> clone.jvm.add(argument.clone()));
            return clone;
        }
    }

    @GSerializable
    public static class AssetIndex extends MojangFileDownload {
        public String id;
        public int totalSize;

        @Override
        public AssetIndex clone() {
            AssetIndex clone = new AssetIndex();
            clone.copyFrom(this);
            clone.id = id;
            clone.totalSize = totalSize;
            return clone;
        }
    }

    @GSerializable
    public record Downloads(MojangFileDownload client,
                            MojangFileDownload client_mappings,
                            MojangFileDownload server,
                            MojangFileDownload server_mappings) implements Cloneable {
        @Override
        public Downloads clone() {
            return new Downloads(
                    client == null ? null : client.clone(),
                    client_mappings == null ? null : client_mappings.clone(),
                    server == null ? null : server.clone(),
                    server_mappings == null ? null : server_mappings.clone()
            );
        }
    }

    @GSerializable
    public record JavaVersion(String component, int majorVersion) implements Cloneable {
        @Override
        public JavaVersion clone() {
            return new JavaVersion(component, majorVersion);
        }
    }

    @GSerializable
    public record Library(Downloads downloads, String name, Map<String, String> natives, Rules rules) implements Cloneable {
        @Override
        public Library clone() {
            Library clone = new Library(
                    downloads == null ? null : downloads.clone(),
                    name,
                    new LinkedHashMap<>(),
                    rules == null ? null : rules.clone()
            );
            if (natives != null) clone.natives.putAll(natives);
            return clone;
        }

        @GSerializable
        public record Downloads(Artifact artifact, Map<String, Artifact> classifiers) implements Cloneable {
            @Override
            public Downloads clone() {
                Downloads clone = new Downloads(artifact == null ? null : artifact.clone(), new LinkedHashMap<>());
                if (classifiers != null) classifiers.forEach((key, value) -> clone.classifiers.put(key, value.clone()));
                return clone;
            }

            @GSerializable
            public static class Artifact extends MojangFileDownload implements Cloneable {
                public String path;

                @GPrefer
                public Artifact() {}

                public Artifact(String path, int size, String sha1, String url) {
                    this.path = path;
                    this.size = size;
                    this.sha1 = sha1;
                    this.url = url;
                }

                @Override
                public Artifact clone() {
                    Artifact clone = new Artifact();
                    clone.copyFrom(this);
                    clone.path = path;
                    return clone;
                }
            }
        }
    }
}
