package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.instance.LoaderInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SetupDirsStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        info.setState("Setting up instance dirs");
        Path iDir = MetaHolder.INSTANCE_DIR.resolve(info.name());
        Instance.setSetupLock(iDir, true);
        if (!Files.exists(iDir)) {
            Files.createDirectories(iDir.resolve("resourcepacks"));
            Files.createDirectories(iDir.resolve("saves"));
            Files.createDirectories(iDir.resolve("screenshots"));
        }
        if (info.loader().type() != LoaderInfo.Type.None) {
            if (!Files.exists(iDir.resolve("mods")))
                Files.createDirectories(iDir.resolve("mods"));
            if (!Files.exists(iDir.resolve("config")))
                Files.createDirectories(iDir.resolve("config"));
        }
        Path eulaTxt = iDir.resolve("eula.txt");
        if (!Files.exists(eulaTxt)) {
            Files.writeString(eulaTxt, "eula=true");
        }
        Files.createDirectories(MetaHolder.NATIVES_DIR.resolve(GameVersionParser.getGameVersion(info.version().id)));
    }

    @Override
    public String getName() {
        return "Setting up directories";
    }
}
