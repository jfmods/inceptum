package io.gitlab.jfronny.inceptum.launcher.model.multimc;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.List;

@GSerializable
public record MMCPackMeta(List<Component> components, int formatVersion) {
    @GSerializable
    public record Component(boolean dependencyOnly, boolean important, String uid, String version) {
    }
}
