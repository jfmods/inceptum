package io.gitlab.jfronny.inceptum.launcher.util;

import io.gitlab.jfronny.commons.OSUtils;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ArtifactInfo;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.VersionInfo;

import java.util.LinkedHashSet;
import java.util.Set;

public class VersionInfoLibraryResolver {
    public static Set<ArtifactInfo> getRelevant(VersionInfo version) {
        Set<ArtifactInfo> artifacts = new LinkedHashSet<>();
        for (VersionInfo.Library library : version.libraries) {
            if (library.rules() != null && !library.rules().allow()) continue;
            if (library.downloads().classifiers() != null && library.natives() != null && library.natives().containsKey(OSUtils.TYPE.mojName)) {
                artifacts.add(new ArtifactInfo(library.downloads().classifiers().get(library.natives().get(OSUtils.TYPE.mojName)), true));
            }
            if (library.downloads().artifact() == null) {
                Utils.LOGGER.info("Null library artifact @ " + library.name());
                continue;
            }
            artifacts.add(new ArtifactInfo(library.downloads().artifact(), false));
        }
        return artifacts;
    }
}
