package io.gitlab.jfronny.inceptum.launcher.model.microsoft;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.List;

@GSerializable
public record Entitlements(List<StoreItem> items, String signature) {
    @GSerializable
    public record StoreItem(String name, String signature) {
    }
}
