package io.gitlab.jfronny.inceptum.launcher.system.source;

import java.nio.file.Path;

public record ModDownload(String sha1, long murmur2, Path file) {
}
