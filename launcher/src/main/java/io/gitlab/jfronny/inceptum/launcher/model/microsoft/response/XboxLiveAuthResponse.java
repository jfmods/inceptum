package io.gitlab.jfronny.inceptum.launcher.model.microsoft.response;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.Date;
import java.util.List;

@GSerializable
public record XboxLiveAuthResponse(@SerializedName("IssueInstant") Date issueInstant,
                                   @SerializedName("NotAfter") Date notAfter,
                                   @SerializedName("Token") String token,
                                   @SerializedName("DisplayClaims") DisplayClaims displayClaims) {
    @GSerializable
    public record DisplayClaims(List<XUIClaim> xui) {
        @GSerializable
        public record XUIClaim(String uhs) {
        }
    }
}
