package io.gitlab.jfronny.inceptum.launcher.model.microsoft.request;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;

@GSerializable
public record XstsTokenRequest(@SerializedName("Properties") Properties properties,
                               @SerializedName("RelyingParty") String relyingParty,
                               @SerializedName("TokenType") String tokenType) {
    @GSerializable
    public record Properties(@SerializedName("SandboxId") String sandboxId, @SerializedName("UserTokens") List<String> userTokens) {
    }
}
