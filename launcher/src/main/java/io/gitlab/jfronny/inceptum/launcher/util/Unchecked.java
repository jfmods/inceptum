package io.gitlab.jfronny.inceptum.launcher.util;

public class Unchecked extends RuntimeException {
    public final Exception exception;

    public Unchecked(Exception exception) {
        this.exception = exception;
    }
}
