package io.gitlab.jfronny.inceptum.launcher.system.exporter;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.InceptumConfig;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.CurseforgeModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.GC_CurseforgeModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.source.CurseforgeModSource;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;

public class CurseForgeExporter extends Exporter<CurseforgeModpackManifest> {
    private static final String OVERRIDES_DIR_DEFAULT = "overrides";

    public CurseForgeExporter() {
        super("CurseForge", "zip", OVERRIDES_DIR_DEFAULT);
    }

    @Override
    protected CurseforgeModpackManifest generateManifests(Path root, Instance instance, String version) throws IOException {
        CurseforgeModpackManifest manifest = new CurseforgeModpackManifest(
                new CurseforgeModpackManifest.Minecraft(
                        instance.getGameVersion(),
                        new LinkedHashSet<>()
                ),
                "minecraftModpack",
                1,
                instance.getName(),
                version,
                InceptumConfig.authorName,
                null, // files
                OVERRIDES_DIR_DEFAULT
        );
        if (instance.isFabric()) {
            manifest.minecraft().modLoaders().add(new CurseforgeModpackManifest.Minecraft.ModLoader(
                    "fabric-" + instance.getLoaderVersion(),
                    true
            ));
        }
        GC_CurseforgeModpackManifest.serialize(manifest, root.resolve("manifest.json"), GsonPreset.API);
        return manifest;
    }

    @Override
    protected void addMods(Path root, Instance instance, Iterable<Mod> mods, CurseforgeModpackManifest manifest, Path modsOverrides) throws IOException {
        modsLoop: for (Mod mod : mods) {
            if (mod.getNeedsInject()) {
                for (ModSource source : mod.getMetadata().sources().keySet()) {
                    if (source instanceof CurseforgeModSource cms) {
                        manifest.files().add(cms.toManifest());
                        continue modsLoop;
                    }
                }
            }
            // Not available on CF
            Files.createDirectories(modsOverrides);
            Files.copy(mod.getJarPath(), modsOverrides.resolve(mod.getJarPath().getFileName().toString()));
        }
        GC_CurseforgeModpackManifest.serialize(manifest, root.resolve("manifest.json"), GsonPreset.API);
    }
}
