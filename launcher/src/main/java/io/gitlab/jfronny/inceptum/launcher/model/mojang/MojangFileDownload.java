package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public class MojangFileDownload implements Cloneable {
    public String sha1;
    public int size;
    public String url;

    @Override
    public MojangFileDownload clone() {
        MojangFileDownload clone = new MojangFileDownload();
        clone.copyFrom(this);
        return clone;
    }

    public void copyFrom(MojangFileDownload mfd) {
        this.sha1 = mfd.sha1;
        this.size = mfd.size;
        this.url = mfd.url;
    }
}
