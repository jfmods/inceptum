package io.gitlab.jfronny.inceptum.launcher.system.mds;

public class NotYetScannedException extends RuntimeException {
    public NotYetScannedException(ScanStage required, ScanStage current) {
        super("Mod has not been scanned to stage " + required + " yet, currently at " + current);
    }
}
