package io.gitlab.jfronny.inceptum.launcher.system.importer;

import org.jetbrains.annotations.Nullable;

public record IntermediaryManifest(String name, @Nullable String overridesPath, String gameVersion, @Nullable String fabricVersion) {
}
