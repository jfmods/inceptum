package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

public enum ModrinthDependencyType {
    required, optional, unsupported
}
