package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Map;

@GSerializable
public record JvmFileInfo(Map<String, File> files) {
    @GSerializable
    public record File(Downloads downloads, boolean executable, String type) {
        @GSerializable
        public record Downloads(MojangFileDownload lzma, MojangFileDownload raw) {
        }
    }
}
