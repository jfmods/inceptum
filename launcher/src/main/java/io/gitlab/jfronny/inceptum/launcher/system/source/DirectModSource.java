package io.gitlab.jfronny.inceptum.launcher.system.source;

import io.gitlab.jfronny.commons.io.HashUtils;
import io.gitlab.jfronny.inceptum.common.Net;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Set;

public record DirectModSource(String fileName, String url, Set<ModSource> dependencies) implements ModSource {
    public DirectModSource {
        if (dependencies == null) dependencies = Set.of();
    }

    @Override
    public ModDownload download() throws IOException {
        Path p = getJarPath();
        try {
            Net.downloadFile(url, p); //TODO test
        } catch (URISyntaxException e) {
            throw new IOException("Could not download file", e);
        }
        byte[] data = Files.readAllBytes(p);
        return new ModDownload(HashUtils.sha1(data), HashUtils.murmur2(data), p);
    }

    @Override
    public Set<ModSource> getDependencies(String gameVersion) throws IOException {
        return dependencies;
    }

    @Override
    public Optional<ModSource> getUpdate(String gameVersion) throws IOException {
        return Optional.empty();
    }

    @Override
    public String getVersion() {
        return "";
    }

    @Override
    public String getName() {
        return "direct/" + (url.contains(":") ? url.split(":")[1] : url).replaceAll("^/+", "");
    }

    @Override
    public String getShortName() {
        return fileName;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getDescription() {
        return "Downloaded directly, no description is available";
    }

    @Override
    public String getSummary() {
        return "Downloaded directly, no description is available";
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof ModSource ms && equals(ms);
    }

    @Override
    public boolean equals(ModSource other) {
        return other instanceof DirectModSource dm
                && dm.url.equals(url)
                && dm.fileName.equals(fileName)
                && dm.dependencies.equals(dependencies);
    }

    @Override
    public boolean projectMatches(ModSource other) {
        return other instanceof DirectModSource dm && dm.url.equals(url);
    }
}
