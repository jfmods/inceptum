package io.gitlab.jfronny.inceptum.launcher.system.exporter;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.GC_ModrinthModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.ModrinthModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModrinthModSource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

public class ModrinthExporter extends Exporter<ModrinthModpackManifest> {
    public ModrinthExporter() {
        super("Modrinth", "mrpack", "overrides");
    }

    @Override
    protected ModrinthModpackManifest generateManifests(Path root, Instance instance, String version) throws IOException {
        ModrinthModpackManifest manifest = new ModrinthModpackManifest(
                1,
                "minecraft",
                version,
                instance.getName(),
                null, // summary
                new ArrayList<>(),
                new ModrinthModpackManifest.Dependencies(
                        instance.getGameVersion(),
                        null,
                        instance.isFabric() ? instance.getLoaderVersion() : null,
                        null
                )
        );
        GC_ModrinthModpackManifest.serialize(manifest, root.resolve("modrinth.index.json"), GsonPreset.API);
        return manifest;
    }

    @Override
    protected void addMods(Path root, Instance instance, Iterable<Mod> mods, ModrinthModpackManifest manifest, Path modsOverrides) throws IOException {
        modsLoop: for (Mod mod : mods) {
            if (mod.getNeedsInject()) {
                for (ModSource source : mod.getMetadata().sources().keySet()) {
                    if (source instanceof ModrinthModSource cms) {
                        manifest.files().add(cms.toManifest());
                        continue modsLoop;
                    }
                }
            }
            // Not available on modrinth
            Files.createDirectories(modsOverrides);
            Files.copy(mod.getJarPath(), modsOverrides.resolve(mod.getJarPath().getFileName().toString()));
        }
        GC_ModrinthModpackManifest.serialize(manifest, root.resolve("modrinth.index.json"), GsonPreset.API);
    }
}
