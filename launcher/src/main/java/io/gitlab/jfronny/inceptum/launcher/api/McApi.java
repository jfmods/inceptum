package io.gitlab.jfronny.inceptum.launcher.api;

import io.gitlab.jfronny.commons.OSUtils;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.launcher.model.mojang.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import static io.gitlab.jfronny.inceptum.common.Net.downloadJObject;
import static io.gitlab.jfronny.inceptum.common.Net.downloadObject;

public class McApi {
    public static VersionsList getVersions() {
        try {
            return downloadJObject("https://launchermeta.mojang.com/mc/game/version_manifest_v2.json", GC_VersionsList::deserialize);
        } catch (IOException e) {
            throw new RuntimeException("Could not load version manifest", e);
        }
    }

    public static VersionInfo getVersionInfo(VersionsListInfo listInfo) throws IOException {
        return downloadJObject(listInfo.url, listInfo.sha1, GC_VersionInfo::deserialize);
    }

    public static AssetIndex getAssetIndex(VersionInfo info) throws IOException, URISyntaxException {
        Path file = MetaHolder.ASSETS_DIR.resolve("indexes");
        if (!Files.exists(file)) Files.createDirectories(file);
        file = file.resolve(info.assetIndex.url.substring(info.assetIndex.url.lastIndexOf("/") + 1));
        try {
            Net.downloadFile(info.assetIndex.url, info.assetIndex.sha1, file);
        } catch (IOException | URISyntaxException e) {
            if (!Files.exists(file)) throw e;
        }
        return GC_AssetIndex.deserialize(file, GsonPreset.API);
    }

    public static Map<String, JvmFileInfo.File> getJvm(String component, int majorVersion) throws IOException {
        // https://github.com/ATLauncher/ATLauncher/blob/master/src/main/java/com/atlauncher/constants/Constants.java#L123
        JvmInfo info = downloadJObject("https://launchermeta.mojang.com/v1/products/java-runtime/2ec0cc96c44e5a76b9c8b7c39df7210883d12871/all.json", GC_JvmInfo::deserialize);
        Map<String, List<JvmInfo.Jvm>> vms = switch (OSUtils.TYPE) {
            case WINDOWS -> info.windowsX64();
            case LINUX -> info.linux();
            case MAC_OS -> info.macOs();
        };
        List<JvmInfo.Jvm> vmList = vms.get(component);
        if (vmList == null)
            throw new IOException("Invalid component: " + component + " (available: " + String.join(", ", vms.keySet()));
        for (JvmInfo.Jvm jvm : vmList) {
            if (jvm.version().name().startsWith(Integer.toString(majorVersion))) {
                return downloadJObject(jvm.manifest().url, jvm.manifest().sha1, GC_JvmFileInfo::deserialize).files();
            }
        }
        throw new IOException("JVM not found");
    }

    public static void downloadAsset(AssetIndex.Asset asset, Path path) throws IOException, URISyntaxException {
        Net.downloadFile("https://resources.download.minecraft.net/" + asset.hash().substring(0, 2) + "/" + asset.hash(), asset.hash(), path);
    }
}
