package io.gitlab.jfronny.inceptum.launcher.model.curseforge;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.List;

@GSerializable
public record CurseforgeMod(
        int id,
        int gameId,
        String name,
        String slug,
        Links links,
        String summary, // optional
        /* Possible values:
        1=New
        2=ChangesRequired
        3=UnderSoftReview
        4=Approved
        5=Rejected
        6=ChangesMade
        7=Inactive
        8=Abandoned
        9=Deleted
        10=UnderReview*/
        int status,
        long downloadCount,
        boolean isFeatured,
        int primaryCategoryId,
        List<Category> categories,
        int classId,
        List<Author> authors,
        Logo logo,
        List<Screenshot> screenshots,
        int mainFileId,
        List<CurseforgeFile> latestFiles,
        List<LatestFileIndex> latestFilesIndexes,
        Date dateCreated,
        Date dateModified,
        Date dateReleased,
        boolean allowModDistribution,
        int gamePopularityRank,
        boolean isAvailable,
        int thumbsUpCount
) {
    @GSerializable
    public record Links(String websiteUrl, String wikiUrl, String issuesUrl, String sourcesUrl) {
    }

    @GSerializable
    public record Category(int id,
                           int gameId,
                           String name,
                           String slug,
                           String url,
                           String iconUrl,
                           Date dateModified,
                           boolean isClass,
                           int classId,
                           int primaryCategoryId) {
    }

    @GSerializable
    public record Author(int id, String name, String url) {
    }

    @GSerializable
    public record Logo(int id,
                       int modId,
                       String title,
                       String description,
                       String thumbnailUrl,
                       String url) {
    }

    @GSerializable
    public record Screenshot(int id,
                             int modId,
                             String title,
                             String description,
                             String thumbnailUrl,
                             String url) {
    }

    @GSerializable
    public record LatestFileIndex(String gameVersion,
                                  int fileId,
                                  String filename,
                                  int releaseType,
                                  int gameVersionTypeId,
                                  Integer modLoader) {
    }
}
