package io.gitlab.jfronny.inceptum.launcher.system.mds.threaded;

import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.FabricModJson;
import io.gitlab.jfronny.inceptum.launcher.model.fabric.GC_FabricModJson;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.mds.noop.NoopMod;
import io.gitlab.jfronny.inceptum.launcher.system.source.DistributionDisabledException;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;

public record FileScanTask(ProtoInstance instance, Path file, ThrowingBiConsumer<Path, Mod, IOException> discovered, String gameVersion) implements Runnable {
    @Override
    public void run() {
        if (!Files.exists(file)) return;
        if (Files.isDirectory(file)) return; // Directories are not supported
        try {
            if (ModPath.isJar(file)) discover(file, ModPath.appendImod(file));
            else if (ModPath.isImod(file)) discover(ModPath.trimImod(file), file);
            else discovered.accept(file, new NoopMod(file));
        } catch (IOException | URISyntaxException e) {
            Utils.LOGGER.error("Could not scan file for mod info", e);
        }
    }

    private void discover(Path jarPath, Path imodPath) throws IOException, URISyntaxException {
        boolean managed = false;
        ModMeta meta;
        Path initialJarPath = jarPath;
        if (Files.exists(imodPath)) meta = GC_ModMeta.deserialize(imodPath, GsonPreset.CONFIG);
        else {
            meta = ModMeta.fromJar(jarPath);
            GC_ModMeta.serialize(meta, imodPath, GsonPreset.CONFIG);
        }
        boolean modified = false;
        if (meta.crossReference()) {
            GC_ModMeta.serialize(meta, imodPath, GsonPreset.CONFIG);
            modified = true;
        }
        meta.updateCheck(gameVersion);
        ModSource selectedSource = null;
        DistributionDisabledException distributionDisabledException = null;
        for (ModSource source : meta.sources().keySet()) {
            try {
                if (!Files.exists(source.getJarPath())) source.download();
                selectedSource = source;
            } catch (DistributionDisabledException de) {
                distributionDisabledException = de;
            }
        }
        if (selectedSource != null) {
            if (Files.exists(jarPath)) {
                Files.delete(jarPath);
                Path newImod = imodPath.getParent().resolve(selectedSource.getShortName() + ModPath.EXT_IMOD);
                Files.move(imodPath, newImod);
                imodPath = newImod;
                modified = true;
            }
            jarPath = selectedSource.getJarPath();
            managed = true;
        } else if (!Files.exists(jarPath)) {
            IOException exception = new IOException("Mod has no jar and no sources");
            if (distributionDisabledException != null) exception.addSuppressed(distributionDisabledException);
            throw exception;
        }
        if (modified) meta = GC_ModMeta.deserialize(imodPath, GsonPreset.CONFIG);

        FabricModJson fmj;
        try (FileSystem fs = Utils.openZipFile(jarPath, false)) {
            Path fmjPath = fs.getPath("fabric.mod.json");
            if (Files.exists(fmjPath)) fmj = GC_FabricModJson.deserialize(fmjPath, GsonPreset.API);
            else fmj = null;
        }

        MdsMod result = new MdsMod(instance, imodPath, initialJarPath, meta);
        result.markDownloaded(imodPath, jarPath, managed, fmj);
        result.markCrossReferenced();
        result.markUpdateChecked();
        discovered.accept(imodPath, result);
    }
}
