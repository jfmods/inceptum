package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.throwable.ThrowingConsumer;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;

import java.io.IOException;

public record MdsCrossReferenceTask(ProtoInstance instance) implements ThrowingConsumer<MdsMod, IOException> {
    @Override
    public void accept(MdsMod mod) throws IOException {
        if (mod.getScanStage().contains(ScanStage.CROSSREFERENCE)) return;
        if (HttpClient.wasOnline()) {
            if (mod.getMetadata().crossReference()) {
                GC_ModMeta.serialize(mod.getMetadata(), mod.getMetadataPath(), GsonPreset.CONFIG);
            }
            mod.markCrossReferenced();
        }
    }
}
