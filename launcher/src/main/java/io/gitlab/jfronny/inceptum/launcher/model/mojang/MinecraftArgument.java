package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.gson.MinecraftArgumentAdapter;

import java.util.LinkedHashSet;
import java.util.Set;

@GSerializable(with = MinecraftArgumentAdapter.class)
public record MinecraftArgument(Set<String> arg) implements Cloneable {
    @Override
    protected MinecraftArgument clone() {
        Set<String> newArgs = new LinkedHashSet<>();
        newArgs.addAll(arg);
        return new MinecraftArgument(newArgs);
    }
}
