package io.gitlab.jfronny.inceptum.launcher.system.importer;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.CurseforgeModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.model.curseforge.GC_CurseforgeModpackManifest;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.source.CurseforgeModSource;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModDownload;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CurseForgeImporter extends Importer<CurseforgeModpackManifest> {
    public CurseForgeImporter() {
        super("CurseForge", "manifest.json", path -> GC_CurseforgeModpackManifest.deserialize(path, GsonPreset.API));
    }

    @Override
    protected IntermediaryManifest validateManifest(CurseforgeModpackManifest manifest, Path sourceRoot) throws IOException {
        if (manifest.manifestVersion() != 1) throw new IOException("Unsupported CurseForge modpack manifest version");
        if (!"minecraftModpack".equals(manifest.manifestType())) throw new IOException("Invalid input: not a minecraft modpack");
        if (manifest.minecraft() == null) throw new IOException("Modpack missing minecraft metadata");
        if (manifest.minecraft().version() == null) throw new IOException("Modpack missing minecraft version");

        String fabric = null;
        if (manifest.minecraft().modLoaders() != null) {
            for (CurseforgeModpackManifest.Minecraft.ModLoader loader : manifest.minecraft().modLoaders()) {
                final String idPrefix = "fabric-";
                if (!loader.id().startsWith(idPrefix)) throw new IOException("Unsupported mod loader");
                fabric = loader.id().substring(idPrefix.length());
            }
        }
        return new IntermediaryManifest(manifest.name(), manifest.overrides(), manifest.minecraft().version(), fabric);
    }

    @Override
    protected void downloadMods(CurseforgeModpackManifest manifest, Path instanceDir, ProcessState state) throws IOException {
        if (manifest.files() != null) {
            Path modsPath = instanceDir.resolve("mods");
            for (CurseforgeModpackManifest.File file : manifest.files()) {
                if (!file.required()) continue;
                CurseforgeModSource source = new CurseforgeModSource(file.projectID(), file.fileID());
                state.updateStep("Downloading " + source.getName());
                ModDownload download = source.download();
                ModMeta imod = ModMeta.fromDownload(download, source, manifest.minecraft().version());
                Files.createDirectories(modsPath);
                GC_ModMeta.serialize(imod, modsPath.resolve(source.getShortName() + ModPath.EXT_IMOD), GsonPreset.CONFIG);
            }
        }
    }
}
