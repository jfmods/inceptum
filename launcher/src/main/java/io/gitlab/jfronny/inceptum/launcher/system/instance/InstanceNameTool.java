package io.gitlab.jfronny.inceptum.launcher.system.instance;

import io.gitlab.jfronny.inceptum.common.MetaHolder;

import java.io.IOException;
import java.nio.file.Files;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstanceNameTool {
    private static final Pattern NUMBER_PATTERN = Pattern.compile("^(.*)\\s*\\(\\d+\\)$");

    public static String getDefaultName(String versionId, boolean fabric) {
        return fabric ? "Fabric " + versionId : versionId;
    }

    public static String namePart(String name) {
        Matcher matcher = NUMBER_PATTERN.matcher(name);
        return matcher.matches() ? matcher.group(1) : name;
    }

    public static String getNextValid(String name) throws IOException {
        name = namePart(name);
        if (!Files.exists(MetaHolder.INSTANCE_DIR.resolve(name))) return name;
        long l = 0;
        String numberedName;
        do numberedName = name + " (" + ++l + ")";
        while (Files.exists(MetaHolder.INSTANCE_DIR.resolve(numberedName)));
        return numberedName;
    }
}
