package io.gitlab.jfronny.inceptum.launcher.system.setup.steps;

import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.setup.SetupStepInfo;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Step;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class WriteMetadataStep implements Step {
    @Override
    public void execute(SetupStepInfo info) throws IOException {
        info.setState("Writing metadata");
        Path instance = MetaHolder.INSTANCE_DIR.resolve(info.name());
        Path metaPath = instance.resolve(Instance.CONFIG_NAME);
        if (!Files.exists(metaPath)) {
            InstanceMeta meta = new InstanceMeta();
            meta.gameVersion = info.version().id;
            GC_InstanceMeta.serialize(meta, metaPath, GsonPreset.CONFIG);
        }
        if (!Files.exists(instance.resolve(".gitignore"))) {
            Files.writeString(instance.resolve(".gitignore"), """
                    realms_persistence.json
                    saves/
                    screenshots/
                    logs/
                    fabricloader.log
                    .mixin.out/
                    .fabric/
                    *.lock
                    eula.txt
                    world/
                    usercache.json
                    realms_persistence.json""");
        }
        if (!Files.exists(instance.resolve(".iceignore"))) {
            Files.writeString(instance.resolve(".iceignore"), Instance.CONFIG_NAME);
        }
    }

    @Override
    public String getName() {
        return "Writing Metadata";
    }
}
