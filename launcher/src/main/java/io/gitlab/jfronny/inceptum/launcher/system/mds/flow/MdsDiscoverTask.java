package io.gitlab.jfronny.inceptum.launcher.system.mds.flow;

import io.gitlab.jfronny.commons.throwable.ThrowingFunction;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_ModMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath;
import io.gitlab.jfronny.inceptum.launcher.system.mds.*;
import io.gitlab.jfronny.inceptum.launcher.system.mds.noop.NoopMod;
import io.gitlab.jfronny.inceptum.launcher.util.ExponentialBackoff;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public record MdsDiscoverTask(HashMap<String, ExponentialBackoff> backoffs, ProtoInstance instance, String gameVersion) implements ThrowingFunction<Path, Mod, IOException> {
    @Override
    public Mod apply(Path file) throws IOException {
        if (!Files.exists(file)) return null;
        if (Files.isDirectory(file)) return null; // Directories are not supported
        if (ModPath.isJar(file)) return discover(file, ModPath.appendImod(file));
        else if (ModPath.isImod(file)) return discover(ModPath.trimImod(file), file);
        else return new NoopMod(file);
    }

    private Mod discover(Path jarPath, Path imodPath) throws IOException {
        String key = imodPath.getFileName().toString() + "/" + MdsDiscoverTask.class.getTypeName();
        ExponentialBackoff backoff = backoffs.computeIfAbsent(key, _ -> new ExponentialBackoff());
        if (!backoff.shouldTry()) return null;
        try {
            ModMeta meta;
            if (Files.exists(imodPath)) meta = GC_ModMeta.deserialize(imodPath, GsonPreset.CONFIG);
            else {
                meta = ModMeta.fromJar(jarPath);
                GC_ModMeta.serialize(meta, imodPath, GsonPreset.CONFIG);
            }
            Mod result = new MdsMod(instance, imodPath, jarPath, meta);
            backoff.success();
            return result;
        } catch (RuntimeException | IOException e) {
            backoff.fail();
            throw e;
        }
    }
}
