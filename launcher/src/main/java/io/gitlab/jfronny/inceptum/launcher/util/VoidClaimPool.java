package io.gitlab.jfronny.inceptum.launcher.util;

import java.io.Closeable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class VoidClaimPool {
    private final Runnable onClaim;
    private final Runnable onRelease;
    private int content = 0;

    public VoidClaimPool(Runnable onClaim, Runnable onRelease) {
        this.onClaim = onClaim;
        this.onRelease = onRelease;
    }

    public Claim claim() {
        return new Claim();
    }

    public boolean isEmpty() {
        return content == 0;
    }

    public class Claim implements Closeable {
        private final AtomicBoolean active = new AtomicBoolean(true);

        private Claim() {
            synchronized (VoidClaimPool.this) {
                if (content++ == 0) onClaim.run();
            }
        }

        @Override
        public void close() {
            if (!active.getAndSet(false))
                throw new UnsupportedOperationException("Cannot release claim that is already released");
            synchronized (VoidClaimPool.this) {
                if (--content == 0) onRelease.run();
            }
        }
    }
}
