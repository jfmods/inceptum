package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Map;

@GSerializable
public record AssetIndex(Map<String, Asset> objects) {
    @GSerializable
    public record Asset(String hash, int size) {
    }
}
