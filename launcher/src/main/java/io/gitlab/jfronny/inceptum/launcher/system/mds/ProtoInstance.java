package io.gitlab.jfronny.inceptum.launcher.system.mds;

import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;

import java.nio.file.Path;

public record ProtoInstance(Path modsDir, ModsDirScanner mds, InstanceMeta meta) {
}
