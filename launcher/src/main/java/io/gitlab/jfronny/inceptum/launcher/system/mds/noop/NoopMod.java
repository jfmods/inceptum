package io.gitlab.jfronny.inceptum.launcher.system.mds.noop;

import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

public class NoopMod extends Mod {
    private final Path path;
    private final ModMeta meta;

    public NoopMod(Path path) {
        this.path = path;
        this.meta = ModMeta.fromJar(path);
    }

    @Override
    public String getName() {
        return path.getFileName().toString();
    }

    @Override
    public String[] getDescription() {
        return new String[]{
                "Noop mod",
                "Any actions on this mod are unsupported"
        };
    }

    @Override
    public boolean getNeedsInject() {
        return false;
    }

    @Override
    public Path getJarPath() {
        return path;
    }

    @Override
    public Path getMetadataPath() {
        return path;
    }

    @Override
    public ScanStage getScanStage() {
        return ScanStage.DISCOVER;
    }

    @Override
    public void delete() throws IOException {
    }

    @Override
    public Path update(ModSource update) throws IOException {
        return path;
    }

    @Override
    public Set<Mod> getDependencies() {
        return Set.of();
    }

    @Override
    public Set<Mod> getDependents() {
        return Set.of();
    }

    @Override
    public void removeDependency(Mod dependency) throws IOException {
    }

    @Override
    public void removeDependent(Mod dependent) throws IOException {
    }

    @Override
    public ModMeta getMetadata() {
        return meta;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
