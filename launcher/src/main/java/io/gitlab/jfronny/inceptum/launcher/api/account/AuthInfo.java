package io.gitlab.jfronny.inceptum.launcher.api.account;

public record AuthInfo(String name, String uuid, String accessToken, String userType) {
    public AuthInfo(MicrosoftAccount account) {
        this(account.minecraftUsername, account.uuid, account.accessToken, "msc");
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return userType.equals("legacy");
        if (obj instanceof AuthInfo info) return info.name.equalsIgnoreCase(name);
        if (obj instanceof MicrosoftAccount info) return info.minecraftUsername.equalsIgnoreCase(name);
        return false;
    }
}
