package io.gitlab.jfronny.inceptum.launcher.system.importer;

import io.gitlab.jfronny.commons.io.JFiles;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;
import io.gitlab.jfronny.inceptum.common.GsonPreset;
import io.gitlab.jfronny.inceptum.common.MetaHolder;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.api.FabricMetaApi;
import io.gitlab.jfronny.inceptum.launcher.api.McApi;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.GC_InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.instance.*;
import io.gitlab.jfronny.inceptum.launcher.system.setup.Steps;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class Importer<T> {
    private final String name;
    private final String manifestFile;
    private final ThrowingFunction<Path, T, IOException> manifestClass;

    protected Importer(String name, String manifestFile, ThrowingFunction<Path, T, IOException> manifestClass) {
        this.name = name;
        this.manifestFile = manifestFile;
        this.manifestClass = manifestClass;
    }

    public boolean canImport(Path root) {
        return Files.exists(root.resolve(manifestFile));
    }

    protected abstract IntermediaryManifest validateManifest(T manifest, Path sourceRoot) throws IOException;

    protected abstract void downloadMods(T manifest, Path instanceDir, ProcessState state) throws IOException;

    private String createVersionString(String gameVersion, @Nullable String fabricVersion) throws IOException {
        if (McApi.getVersions().versions().stream().filter(v -> v.id.equals(gameVersion)).findFirst().isEmpty()) {
            throw new IOException("Invalid minecraft version: " + gameVersion);
        }
        if (fabricVersion == null) {
            return gameVersion;
        } else {
            FabricMetaApi.getLoaderVersion(gameVersion, fabricVersion);
            return GameVersionParser.createVersionWithFabric(gameVersion, fabricVersion);
        }
    }

    public Instance importPack(Path sourceRoot, ProcessState state) throws IOException {
        if (state.isCancelled()) return null;
        state.incrementStep("Generating skeleton");
        T manifest = manifestClass.apply(sourceRoot.resolve(manifestFile));
        IntermediaryManifest man = validateManifest(manifest, sourceRoot);
        String name = man.name();
        if (name == null || !Utils.VALID_FILENAME.matcher(name).matches()) name = "New Pack";
        name = InstanceNameTool.getNextValid(name);
        final Path iDir = MetaHolder.INSTANCE_DIR.resolve(name).toAbsolutePath().normalize();
        Instance.setSetupLock(iDir, true);
        InstanceMeta meta = new InstanceMeta();
        meta.gameVersion = createVersionString(man.gameVersion(), man.fabricVersion());
        GC_InstanceMeta.serialize(meta, iDir.resolve(Instance.CONFIG_NAME), GsonPreset.CONFIG);

        if (state.isCancelled()) {
            JFiles.deleteRecursive(iDir);
            return null;
        }
        state.incrementStep("Downloading mods");
        downloadMods(manifest, iDir, state);

        if (state.isCancelled()) {
            JFiles.deleteRecursive(iDir);
            return null;
        }
        state.incrementStep("Adding overrides");
        if (man.overridesPath() != null) {
            Path overridesPath = sourceRoot.resolve(man.overridesPath());
            JFiles.listTo(overridesPath, path -> {
                JFiles.copyRecursive(path, iDir.resolve(path.getFileName().toString()));
            });
        }
        if (state.isCancelled()) {
            JFiles.deleteRecursive(iDir);
            return null;
        }
        Instance.setSetupLock(iDir, false);
        Instance instance = InstanceList.read(iDir);
        Steps.reDownload(instance, state);
        if (state.isCancelled()) {
            JFiles.deleteRecursive(iDir);
            return null;
        }
        return instance;
    }

    public String getName() {
        return name;
    }
}
