package io.gitlab.jfronny.inceptum.launcher.gson;

import io.gitlab.jfronny.commons.serialize.MalformedDataException;
import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.inceptum.launcher.system.source.*;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

public class ModSourceAdapter {
    public static <TEx extends Exception, Writer extends SerializeWriter<TEx, ?>> void serialize(ModSource src, Writer writer) throws TEx {
        writer.beginObject();
        switch (src) {
            case ModrinthModSource mo -> writer.name("type").value("modrinth")
                    .name("id").value(mo.getVersionId());
            case DirectModSource di -> {
                writer.name("type").value("direct")
                        .name("fileName").value(di.getFileName())
                        .name("url").value(di.url())
                        .name("dependencies");
                writer.beginArray();
                for (ModSource dependency : di.dependencies()) {
                    serialize(dependency, writer);
                }
                writer.endArray();
            }
            case CurseforgeModSource cu -> {
                writer.name("type").value("curseforge");
                if (cu.getShortName().matches("\\d+")) {
                    writer.name("projectId").value(cu.getProjectId());
                } else {
                    writer.name("project").value(cu.getShortName());
                }
                writer.name("fileId").value(cu.getFileId());
            }
            case null, default ->
                    throw new RuntimeException("ModSources with the type " + (src == null ? null : src.getClass()) + " are not supported");
        }
        writer.endObject();
    }

    public static <TEx extends Exception, Reader extends SerializeReader<TEx, ?>> ModSource deserialize(Reader reader) throws TEx, MalformedDataException {
        String type = null;

        String mr$id = null;

        Integer cf$projectId = null;
        String cf$project = null;
        Integer cf$fileId = null;

        String direct$fileName = null;
        String direct$url = null;
        Set<ModSource> direct$dependencies = null;

        reader.beginObject();
        while (reader.hasNext()) {
            switch (reader.nextName()) {
                case "type" -> type = reader.nextString();
                case "id" -> mr$id = reader.nextString();
                case "projectId" -> cf$projectId = reader.nextInt();
                case "project" -> cf$project = reader.nextString();
                case "fileId" -> cf$fileId = reader.nextInt();
                case "fileName" -> direct$fileName = reader.nextString();
                case "url" -> direct$url = reader.nextString();
                case "dependencies" -> {
                    direct$dependencies = new LinkedHashSet<>();
                    reader.beginArray();
                    while (reader.hasNext()) direct$dependencies.add(deserialize(reader));
                    reader.endArray();
                }
            }
        }
        reader.endObject();
        if (type == null) throw new MalformedDataException("Expected ModSource to contain a type");
        try {
            return switch (type) {
                case "modrinth" -> {
                    if (mr$id == null) throw new MalformedDataException("Expected ModrinthModSource to contain a version ID");
                    yield new ModrinthModSource(mr$id);
                }
                case "curseforge" -> {
                    if (cf$fileId == null)
                        throw new MalformedDataException("Expected a fileId in this curseforge project");
                    if (cf$projectId != null) yield new CurseforgeModSource(cf$projectId, cf$fileId);
                    else if (cf$project != null) yield new CurseforgeModSource(cf$project, cf$fileId);
                    else throw new MalformedDataException("Expected a projectId in this curseforge project");
                }
                case "direct" -> {
                    if (direct$fileName == null) throw new MalformedDataException("Expected direct download to have a fileName");
                    if (direct$url == null) throw new MalformedDataException("Expected direct download to have a url");
                    if (direct$dependencies == null) yield new DirectModSource(direct$fileName, direct$url, Set.of());
                    else {
                        yield new DirectModSource(direct$fileName, direct$url, direct$dependencies);
                    }
                }
                default -> throw new MalformedDataException("Unexpected ModSource type: " + type);
            };
        } catch (MalformedDataException mde) {
            throw mde;
        } catch (IOException e) {
            throw new MalformedDataException("Failed to read ModSource", e);
        }
    }
}
