package io.gitlab.jfronny.inceptum.launcher.system.mds;

import io.gitlab.jfronny.inceptum.launcher.model.inceptum.ModMeta;
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;

public abstract class Mod implements Comparable<Mod> {
    public abstract String getName();
    public abstract String[] getDescription();
    public abstract boolean getNeedsInject();
    public abstract Path getJarPath();
    public abstract Path getMetadataPath();

    public abstract ScanStage getScanStage();

    public abstract void delete() throws IOException;
    public abstract Path update(ModSource update) throws IOException;
    public abstract Set<Mod> getDependencies();
    public abstract Set<Mod> getDependents();
    public abstract void removeDependency(Mod dependency) throws IOException;
    public abstract void removeDependent(Mod dependent) throws IOException;

    public abstract ModMeta getMetadata();
    public abstract boolean isEnabled();

    @Override
    public int compareTo(@NotNull Mod mod) {
        return getName().compareTo(mod.getName());
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Mod mod && getName().equals(mod.getName());
    }

    @Override
    public String toString() {
        return getName();
    }
}
