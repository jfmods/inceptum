package io.gitlab.jfronny.inceptum.launcher.model.microsoft.response;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GPrefer;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.Date;

@GSerializable
public record OauthTokenResponse(
        @SerializedName("token_type") String tokenType,
        @SerializedName("expires_in") int expiresIn,
        @SerializedName("expires_at") Date expiresAt,
        String scope,
        @SerializedName("access_token") String accessToken,
        @SerializedName("refresh_token") String refreshToken,
        @SerializedName("user_id") String userId,
        String foci) {
    @GPrefer
    public OauthTokenResponse(String tokenType, int expiresIn, Date expiresAt, String scope, String accessToken, String refreshToken, String userId, String foci) {
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
        this.expiresAt = getExpiresAt(expiresAt, expiresIn);
        this.scope = scope;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.userId = userId;
        this.foci = foci;
    }

    private static Date getExpiresAt(Date expiresAt, long expiresIn) {
        if (expiresAt == null) {
            expiresAt = new Date();
            expiresAt.setTime(expiresAt.getTime() + expiresIn * 1000);
        }
        return expiresAt;
    }
}
