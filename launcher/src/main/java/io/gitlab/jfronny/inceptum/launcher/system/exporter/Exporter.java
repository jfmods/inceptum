package io.gitlab.jfronny.inceptum.launcher.system.exporter;

import io.gitlab.jfronny.commons.StreamIterable;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance;
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod;
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage;
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState;
import io.gitlab.jfronny.inceptum.launcher.util.gitignore.IgnoringWalk;

import java.io.IOException;
import java.nio.file.*;
import java.util.Objects;

public abstract class Exporter<Manifest> {
    private final String name;
    private final String fileExtension;
    private final String overridesDirName;

    public Exporter(String name, String fileExtension, String overridesDirName) {
        this.name = Objects.requireNonNull(name);
        this.fileExtension = Objects.requireNonNull(fileExtension);
        this.overridesDirName = Objects.requireNonNull(overridesDirName);
    }

    protected abstract Manifest generateManifests(Path root, Instance instance, String version) throws IOException;

    protected abstract void addMods(Path root, Instance instance, Iterable<Mod> mods, Manifest manifest, Path modsOverrides) throws IOException;

    public void generate(ProcessState state, Instance instance, Path exportPath) throws IOException {
        if (Files.exists(exportPath)) Files.delete(exportPath);
        try (FileSystem fs = Utils.openZipFile(exportPath, true)) {
            Path root = fs.getPath(".");
            Path overrides = fs.getPath(overridesDirName);
            state.incrementStep("Preparing manifests");
            Manifest manifest = generateManifests(root, instance, instance.meta().instanceVersion);
            if (instance.isFabric()) {
                state.incrementStep("Adding mods");
                addMods(root, instance, instance.completeModsScan(ScanStage.CROSSREFERENCE).stream().filter(mod -> {
                    if (!mod.isEnabled()) return false;
                    state.updateStep(mod.getName());
                    return true;
                }).toList(), manifest, overrides.resolve("mods"));
            }
            state.incrementStep("Adding files");
            filesLoop: for (Path path : new StreamIterable<>(IgnoringWalk.walk(instance.path()))) {
                Path relativePath = instance.path().relativize(path).normalize();
                Path target = overrides;
                for (Path segment : relativePath) {
                    if (target == overrides && segment.toString().equals("mods")) continue filesLoop;
                    if (segment.toString().startsWith(".")) continue filesLoop;
                    target = target.resolve(segment.toString());
                }
                state.updateStep(relativePath.toString());
                Files.createDirectories(target.getParent());
                Files.copy(path, target);
            }
            state.incrementStep("Cleaning up");
            Files.walkFileTree(root, new CleanupFileVisitor());
        } catch (Throwable t) {
            if (Files.exists(exportPath)) Files.delete(exportPath);
            throw t;
        }
    }

    public String getName() {
        return name;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public String getDefaultFileName(Instance instance) {
        return instance.getName() + " " + instance.meta().instanceVersion + " (" + name + ")." + fileExtension;
    }
}
