package io.gitlab.jfronny.inceptum.launcher.model.microsoft.request;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

@GSerializable
public record XblTokenRequest(@SerializedName("Properties") Properties properties,
                              @SerializedName("RelyingParty") String relyingParty,
                              @SerializedName("TokenType") String tokenType) {
    @GSerializable
    public record Properties(@SerializedName("AuthMethod") String authMethod,
                             @SerializedName("SiteName") String siteName,
                             @SerializedName("RpsTicket") String rpsTicket) {
    }
}
