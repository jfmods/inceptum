package io.gitlab.jfronny.inceptum.launcher.model.microsoft.request;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

@GSerializable
public record LoginRequest(String xtoken, String platform) {
}
