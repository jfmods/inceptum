package io.gitlab.jfronny.inceptum.launcher.api;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.inceptum.common.GList;
import io.gitlab.jfronny.inceptum.common.Net;
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.*;

import java.io.IOException;
import java.io.StringReader;
import java.util.*;

public class ModrinthApi {
    private static final String API_HOST = "https://api.modrinth.com/";
    private static final int ITEMS_PER_PAGE = 20;

    //TODO search by categories: facets:[["versions:$ver","versions:$ver"],["categories:$cat","categories:$cat"]]
    //TODO filter server/client-only mods
    public static ModrinthSearchResult search(String query, int page, String version, ModrinthProjectType type) throws IOException {
        return Net.downloadJObject(Net.buildUrl(API_HOST, "v2/search", Map.of(
                "query", query,
                "facets", "[[\"versions:" + version + "\"],[\"categories:fabric\"],[\"project_type:" + type + "\"]]",
                "index", "relevance",
                "offset", Integer.toString(page * ITEMS_PER_PAGE),
                "limit", Integer.toString(ITEMS_PER_PAGE)
        )), GC_ModrinthSearchResult::deserialize);
    }

    public static ModrinthProject getMod(String id) throws IOException {
        return Net.downloadJObject(API_HOST + "v2/project/" + id, GC_ModrinthProject::deserialize);
    }

    public static List<ModrinthVersion> getVersions(String mod) throws IOException {
        List<ModrinthVersion> versions = Net.downloadJObject(API_HOST + "v2/project/" + mod + "/version", s -> GList.read(s, GC_ModrinthVersion::deserialize));
        versions.sort(Comparator.comparing(ModrinthVersion::date_published));
        return versions;
    }

    public static ModrinthVersion getVersion(String id) throws IOException {
        return Net.downloadJObject(API_HOST + "v2/version/" + id, GC_ModrinthVersion::deserialize);
    }

    public static ModrinthLatest getLatestVersions(String mod, String gameVersion) throws IOException {
        ModrinthVersion stable = null;
        ModrinthVersion beta = null;
        ModrinthVersion latest = null;
        for (ModrinthVersion version : ModrinthApi.getVersions(mod)) {
            if (version.game_versions().contains(gameVersion) && version.loaders().contains("fabric")) {
                latest = version;
                if (version.version_type() == ModrinthVersion.VersionType.beta || version.version_type() == ModrinthVersion.VersionType.release)
                    beta = version;
                if (version.version_type() == ModrinthVersion.VersionType.release)
                    stable = version;
            }
        }
        return new ModrinthLatest(stable, beta, latest);
    }

    public static ModrinthVersion getVersionByHash(String sha1) throws IOException {
        return Net.downloadJObject(API_HOST + "v2/version_file/" + sha1 + "?algorithm=sha1", GC_ModrinthVersion::deserialize);
    }
}
