package io.gitlab.jfronny.inceptum.launcher.api.account;

import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class AccountManager {
    private static MicrosoftAccount SELECTED_ACCOUNT;
    private static final List<MicrosoftAccount> ACCOUNTS = new ArrayList<>();
    public static final AuthInfo NULL_AUTH = new AuthInfo("Joe", "2536abce90e8476a871679918164abc5", "99abe417230342cb8e9e2168ab46297a", "legacy");

    public static AuthInfo getSelectedAccount() {
        if (SELECTED_ACCOUNT == null) return NULL_AUTH;
        else if (SELECTED_ACCOUNT.ensureAccessTokenValid()) return new AuthInfo(SELECTED_ACCOUNT);
        else {
            Utils.LOGGER.error("Couldn't login properly, using offline mode");
            return NULL_AUTH;
        }
    }

    public static boolean accountMissing() {
        return ACCOUNTS.isEmpty();
    }

    public static List<MicrosoftAccount> getAccounts() {
        return List.copyOf(ACCOUNTS);
    }

    public static int getSelectedIndex() {
        if (SELECTED_ACCOUNT == null) return ACCOUNTS.size();
        return ACCOUNTS.indexOf(SELECTED_ACCOUNT);
    }

    public static void saveAccounts() {
        try (JsonWriter w = new JsonWriter(Files.newBufferedWriter(MetaHolder.ACCOUNTS_PATH))) {
            GsonPreset.Config.configure(w);
            GList.write(w, ACCOUNTS, GC_MicrosoftAccount::serialize);
        } catch (IOException e) {
            Utils.LOGGER.error("Could not save accounts", e);
        }
    }

    public static void loadAccounts() {
        Utils.LOGGER.info("Loading accounts");
        if (Files.exists(MetaHolder.ACCOUNTS_PATH)) {
            try (JsonReader r = new JsonReader(Files.newBufferedReader(MetaHolder.ACCOUNTS_PATH))) {
                GsonPreset.Config.configure(r);
                ACCOUNTS.addAll(GList.read(r, GC_MicrosoftAccount::deserialize));
            } catch (IOException e) {
                Utils.LOGGER.error("Could not load accounts", e);
            }
        }
        for (MicrosoftAccount account : ACCOUNTS) {
            account.refreshAccessToken();
            if (account.accountId.equalsIgnoreCase(InceptumConfig.lastAccount)) {
                SELECTED_ACCOUNT = account;
            }
        }
        Utils.LOGGER.info("Finished loading accounts");
    }

    public static void addAccount(MicrosoftAccount account) {
        ACCOUNTS.add(account);
        if (ACCOUNTS.size() > 1) {
            LauncherEnv.showOkCancel("Account added successfully. Switch to it now?", "Success", () -> switchAccount(account));
        } else switchAccount(account);
        saveAccounts();
    }

    public static void removeAccount(MicrosoftAccount account) {
        if (SELECTED_ACCOUNT == account) {
            if (ACCOUNTS.size() == 1)
                switchAccount(null);
            else
                switchAccount(ACCOUNTS.getFirst());
        }
        ACCOUNTS.remove(account);
        saveAccounts();
    }

    public static void switchAccount(MicrosoftAccount account) {
        if (account == null) {
            Utils.LOGGER.info("Logging out");
            SELECTED_ACCOUNT = null;
            InceptumConfig.lastAccount = null;
        } else {
            Utils.LOGGER.info("Changed account to " + account);
            SELECTED_ACCOUNT = account;
            InceptumConfig.lastAccount = account.accountId;
        }
        InceptumConfig.saveConfig();
    }

    public static Object getAccountByName(String username) {
        for (MicrosoftAccount account : ACCOUNTS) {
            if (account.accountId.equalsIgnoreCase(username))
                return account;
        }
        return null;
    }

    public static boolean isAccountByName(String username) {
        for (MicrosoftAccount account : ACCOUNTS) {
            if (account.accountId.equalsIgnoreCase(username))
                return true;
        }
        return false;
    }
}
