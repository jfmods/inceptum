package io.gitlab.jfronny.inceptum.launcher.util.gitignore;

import java.util.function.Function;
import java.util.regex.*;

public class Replacer {
    private final String name;
    private final Pattern regex;
    private final Function<MatchResult, String> matchEvaluator;

    public Replacer(String name, String regex, Function<MatchResult, String> matchEvaluator) {
        this.name = name;
        this.regex = Pattern.compile(regex);
        this.matchEvaluator = matchEvaluator;
    }

    public String invoke(String pattern) {
        return regex.matcher(pattern).replaceAll(result -> Matcher.quoteReplacement(matchEvaluator.apply(result)));
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public String getRegex() {
        return regex.toString();
    }
}
