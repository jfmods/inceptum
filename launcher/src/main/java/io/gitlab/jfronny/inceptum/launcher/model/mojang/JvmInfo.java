package io.gitlab.jfronny.inceptum.launcher.model.mojang;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;

import java.util.List;
import java.util.Map;

@GSerializable
public record JvmInfo(Map<String, List<Jvm>> linux,
                      @SerializedName("mac-os") Map<String, List<Jvm>> macOs,
                      @SerializedName("windows-x64") Map<String, List<Jvm>> windowsX64) {
    @GSerializable
    public record Jvm(Availability availability, MojangFileDownload manifest, Version version) {
        @GSerializable
        public record Availability(int group, int progress) {
        }

        @GSerializable
        public record Version(String name, String released) {
        }
    }
}
