package io.gitlab.jfronny.inceptum.launcher.system.mds;

import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.inceptum.common.Utils;
import io.gitlab.jfronny.inceptum.launcher.model.inceptum.InstanceMeta;
import io.gitlab.jfronny.inceptum.launcher.system.mds.flow.FlowMds;
import io.gitlab.jfronny.inceptum.launcher.system.mds.noop.NoopMds;
import io.gitlab.jfronny.inceptum.launcher.system.mds.threaded.ThreadedMds;
import io.gitlab.jfronny.inceptum.launcher.util.GameVersionParser;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

public interface ModsDirScanner extends Closeable {
    String MDS_KIND = switch (System.getProperty("inceptum.mds.kind", "flow").toLowerCase()) {
        case "flow" -> "flow";
        case "threaded" -> "threaded";
        case "noop" -> "noop";
        case String s -> {
            Utils.LOGGER.error("Unknown mds kind: " + s + ", using flow");
            yield "flow";
        }
    };
    static ModsDirScanner get(Path modsDir, InstanceMeta meta) throws IOException {
        //TODO use a primitive pattern and guard once available
        return switch (MDS_KIND) {
            case String s when s.equals("flow") && Files.exists(modsDir) -> FlowMds.get(modsDir, meta);
            case String s when s.equals("threaded") && Files.exists(modsDir) -> ThreadedMds.get(modsDir, meta);
            default -> new NoopMds(GameVersionParser.getGameVersion(meta.gameVersion));
        };
    }

    static void closeAll() {
        switch (MDS_KIND) {
            case "flow" -> FlowMds.closeAll();
            case "threaded" -> ThreadedMds.closeAll();
        }
    }

    boolean isComplete(ScanStage stage);

    void start();

    Closeable focus();

    String getGameVersion();

    Set<Mod> getMods() throws IOException;

    Mod get(Path path);

    void invalidate(Path path);

    default void invalidate(Mod mod) {
        invalidate(mod.getMetadataPath());
    }

    boolean hasScanned(Path path);

    default boolean hasScanned(Mod mod) {
        return hasScanned(mod.getMetadataPath());
    }

    void runOnce(ScanStage targetStage, ThrowingBiConsumer<Path, Mod, IOException> discovered);
}
