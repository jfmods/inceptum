package io.gitlab.jfronny.inceptum.launcher.model.curseforge;

import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import io.gitlab.jfronny.inceptum.common.GsonPreset;

import java.util.Date;
import java.util.List;

@GSerializable
public record CurseforgeFile(
        int id,
        int gameId,
        int modId,
        boolean isAvailable,
        String displayName,
        String fileName,
        /* Possible values:
        1=Release
        2=Beta
        3=Alpha*/
        int releaseType,
        /* Possible values:
        1=Processing
        2=ChangesRequired
        3=UnderReview
        4=Approved
        5=Rejected
        6=MalwareDetected
        7=Deleted
        8=Archived
        9=Testing
        10=Released
        11=ReadyForReview
        12=Deprecated
        13=Baking
        14=AwaitingPublishing
        15=FailedPublishing*/
        int fileStatus,
        List<Hash> hashes,
        Date fileDate,
        int fileLength,
        long downloadCount,
        String downloadUrl,
        List<String> gameVersions,
        List<GameVersion> sortableGameVersions,
        List<Dependency> dependencies,
        int alternateFileId,
        boolean isServerPack,
        long fileFingerprint, // murmur5 hash
        List<Module> modules
) {
    /* Possible algorithms:
    1=Sha1
    2=Md5*/
    @GSerializable
    public record Hash(String value, int algo) {
    }

    @GSerializable
    public record GameVersion(String gameVersionName,
                              String gameVersionPadded,
                              String gameVersion,
                              Date gameVersionReleaseDate,
                              int gameVersionTypeId) {
    }

    /* Possible relationship types:
    1=EmbeddedLibrary
    2=OptionalDependency
    3=RequiredDependency
    4=Tool
    5=Incompatible
    6=Include*/
    @GSerializable
    public record Dependency(int modId, int relationType) {
    }

    @GSerializable
    public record Module(String name, long fingerprint) {
    }
}
