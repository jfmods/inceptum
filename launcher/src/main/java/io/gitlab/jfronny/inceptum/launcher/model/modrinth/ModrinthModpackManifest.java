package io.gitlab.jfronny.inceptum.launcher.model.modrinth;

import io.gitlab.jfronny.commons.serialize.annotations.SerializedName;
import io.gitlab.jfronny.commons.serialize.generator.annotations.GSerializable;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@GSerializable
public record ModrinthModpackManifest(
        int formatVersion, // 1
        String game, // "minecraft"
        String versionId,
        String name,
        String summary,
        List<File> files,
        Dependencies dependencies
) {
    @GSerializable
    public record File(String path, ModrinthHashes hashes, @Nullable Env env, List<String> downloads, long fileSize) {
        @GSerializable
        public record Env(ModrinthDependencyType client, ModrinthDependencyType server) {
        }
    }

    // All are nullable
    @GSerializable
    public record Dependencies(String minecraft,
                               String forge,
                               @SerializedName("fabric-loader") String fabricLoader,
                               @SerializedName("quilt-loader") String quiltLoader) {
    }
}
