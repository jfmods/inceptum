package io.gitlab.jfronny.inceptum.launcher.util.gitignore;

public class Replacers {
    public static final Replacer TRAILING_SPACES = new Replacer(
            "TrailingSpaces",
            "\\\\?\\s+$",
            match -> match.group().indexOf('\\') == 0 ? " " : ""
    );

    public static final Replacer ESCAPED_SPACES = new Replacer(
            "EscapedSpaces",
            "\\\\\\s",
            match -> " "
    );

    public static final Replacer LITERAL_PLUS = new Replacer(
            "LiteralPlus",
            "\\+",
            match -> "\\+"
    );

    // a ? matches any character other than a /
    public static final Replacer QUESTION_MARK = new Replacer(
            "QuestionMark",
            "(?!\\\\)\\?",
            match -> "[^/]"
    );

    // a leading / matches the beginning of the path
    // eg. /fake.c only matches fake.c, not src/fake.c
    public static final Replacer LEADING_SLASH = new Replacer(
            "LeadingSlash",
            "^\\/",
            match -> "^"
    );

    public static final Replacer METACHARACTER_SLASH_AFTER_LEADING_SLASH = new Replacer(
            "MetacharacterSlashAfterLeadingSlash",
            "\\/",
            match -> "\\/"
    );

    // A leading "**" followed by a slash means match in all directories.
    // For example, "**/foo" matches file or directory "foo" anywhere, the same as pattern "foo".
    // "**/foo/bar" matches file or directory "bar" anywhere that is directly under directory "foo".
    public static final Replacer LEADING_DOUBLE_STAR = new Replacer(
            "LeadingDoubleStar",
            "^(\\*\\*/|\\*\\*)",
            match -> ".*"
    );

    // A slash followed by two consecutive asterisks then a slash matches zero or more directories.
    // For example, "a/**/b" matches "a/b", "a/x/b", "a/x/y/b" and so on.
    public static final Replacer MIDDLE_DOUBLE_STAR = new Replacer(
            "MiddleDoubleStar",
            "(?<=/)\\*\\*/",
            match -> "(.*/)?"
    );

    // A trailing "/**" matches everything inside. For example,
    // "abc/**" matches all files inside directory "abc",
    // relative to the location of the .gitignore file, with infinite depth.
    public static final Replacer TRAILING_DOUBLE_STAR = new Replacer(
            "TrailingDoubleStar",
            "\\*\\*$",
            match -> ".*$"
    );

    // Undocumented cases like foo/**.ps
    // Treat ** as match any character other than /.
    public static final Replacer OTHER_DOUBLE_STAR = new Replacer(
            "TrailingDoubleStar",
            "\\*\\*",
            match -> "[^/]*"
    );

    // If there is a separator at the beginning or middle (or both) of the pattern,
    // then the pattern is relative to the directory level of the particular .gitignore file itself.
    // Otherwise the pattern may also match at any level below the .gitignore level.
    // The leading slash should be gone after using LeadingSlash.
    // So put a ^ in the beginning of the match.
    public static final Replacer MIDDLE_SLASH = new Replacer(
            "MiddleSlash",
            "^([^/\\^]+/[^/]+)",
            match -> "^" + match.group(1)
    );

    // If there is a separator at the end of the pattern then the pattern will only match directories,
    // otherwise the pattern can match both files and directories.
    // This regex handles the paths with trailing slash present.
    public static final Replacer TRAILING_SLASH = new Replacer(
            "TrailingSlash",
            "^([^/]+)/$",
            match -> "(/|^)" + match.group(1) + "/"
    );

    // If there is a separator at the end of the pattern then the pattern will only match directories,
    // otherwise the pattern can match both files and directories.
    // This pattern handles the paths with no trailing slash.
    public static final Replacer NO_TRAILING_SLASH = new Replacer(
            "NoTrailingSlash",
            "([^/$]+)$",
            match -> match.group(1) + "(/.*)?$"
    );

    // An asterisk "*" matches anything except a slash.
    // Replaces single * with anything other than a /.
    // Unless the star is in the beginning of the pattern.
    public static final Replacer NON_LEADING_SINGLE_STAR = new Replacer(
            "NonLeadingSingleStar",
            "(?<!^)(?<!\\*)\\*(?!\\*)",
            match -> "[^/]*"
    );

    public static final Replacer LEADING_SINGLE_STAR = new Replacer(
            "LeadingSingleStar",
            "^(?<!\\*)\\*(?!\\*)",
            match -> ".*"
    );

    public static final Replacer ENDING = new Replacer(
            "Ending",
            "([^/$]+)$",
            match -> match.group(1) + "$"
    );

    public static final Replacer LITERAL_DOT = new Replacer(
            "LiteralDot",
            "\\.",
            match -> "\\."
    );

    public static final Replacer NO_SLASH = new Replacer(
            "NoSlash",
            "(^[^/]*$)",
            match -> "(^|/)" + match.group(1)
    );
}
