module io.gitlab.jfronny.inceptum.launcher.gtk {
    requires transitive io.gitlab.jfronny.inceptum.launcher;
    requires static org.jetbrains.annotations;
    requires kotlin.stdlib;
    requires org.gnome.glib;
    requires org.gnome.gtk;
    requires org.gnome.adw;
    requires org.gnome.pango;

    // Should theoretically already be included transitively through inceptum.launcher and inceptum.common
    requires io.gitlab.jfronny.commons;
    requires io.gitlab.jfronny.commons.io;
    requires io.gitlab.jfronny.commons.logger;
    requires java.desktop;
}