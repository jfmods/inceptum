package io.gitlab.jfronny.inceptum.gtk.menu

import org.gnome.gio.SimpleAction
import org.gnome.glib.Variant

class BuiltRadioItem<T>(action: SimpleAction, private val options: List<T>) : BuiltMenuItem(action, null) {
    var selected: T
        get() = options[action.state!!.int32]
        set(selected) {
            action.state = Variant.int32(options.indexOf(selected))
        }
}
