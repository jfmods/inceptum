package io.gitlab.jfronny.inceptum.gtk.window

import io.gitlab.jfronny.inceptum.common.Utils
import io.gitlab.jfronny.inceptum.gtk.GtkMenubar
import io.gitlab.jfronny.inceptum.gtk.control.InstanceListEntryFactory
import io.gitlab.jfronny.inceptum.gtk.menu.MenuBuilder
import io.gitlab.jfronny.inceptum.gtk.util.*
import io.gitlab.jfronny.inceptum.gtk.window.settings.launcher.LauncherSettingsWindow
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceListWatcher
import io.gitlab.jfronny.inceptum.launcher.system.launch.LaunchType
import org.gnome.adw.Clamp
import org.gnome.adw.StatusPage
import org.gnome.gio.*
import org.gnome.glib.GLib
import org.gnome.gtk.*
import org.gnome.gtk.Application
import java.io.IOException
import java.net.URI

class MainWindow(app: Application) : ApplicationWindow(app) {
//    private val listButton: Button
//    private val gridButton: Button
    private val stack: Stack
    private val empty: StatusPage
    private val listContainer: Clamp
//    private val gridView: GridView
    private val instanceList: MutableList<Instance>
    private val instanceListModel: StringList

    init {
        val header = HeaderBar()
        val newButton = MenuButton()
        newButton.iconName = "list-add-symbolic"
        newButton.menuModel = GtkMenubar.newMenu!!.menu
        val accountsButton = MenuButton()
        accountsButton.iconName = "avatar-default-symbolic"
        accountsButton.menuModel = GtkMenubar.accountsMenu!!.menu
//        listButton = Button.newFromIconName("view-list-symbolic")
//        listButton.onClicked {
//            InceptumConfig.listView = true
//            InceptumConfig.saveConfig()
//            generateWindowBody()
//        }
//        gridButton = Button.newFromIconName("view-grid-symbolic")
//        gridButton.onClicked {
//            InceptumConfig.listView = false
//            InceptumConfig.saveConfig()
//            generateWindowBody()
//        }

        //TODO search button like boxes

        val uiMenu = MenuBuilder(app, Menu(), "hamburger")
        uiMenu.button("support") { Utils.openWebBrowser(URI("https://git.frohnmeyer-wds.de/JfMods/Inceptum/issues")) }
        uiMenu.button("preferences") { LauncherSettingsWindow(app).visible = true }
        uiMenu.button("about") { AboutWindow.createAndShow() }
        val menuButton = MenuButton()
        menuButton.iconName = "open-menu-symbolic"
        menuButton.menuModel = uiMenu.menu

        header.packStart(newButton)

        header.packEnd(menuButton)
//        header.packEnd(gridButton)
//        header.packEnd(listButton)
        header.packEnd(accountsButton)

        instanceList = ArrayList()
        instanceListModel = StringList(arrayOf())
        val selection = NoSelection<StringObject>(instanceListModel)

        val listView = ListView(selection, InstanceListEntryFactory(app, instanceList))
        listView.addCssClass("rich-list")
        listView.showSeparators = true
        listView.onActivate { position: Int ->
            // Double click
            GtkMenubar.launch(instanceList[position], LaunchType.Client)
        }
        val frame = Frame(null as String?)
        frame.child = listView
        frame.marginHorizontal = 24
        frame.marginVertical = 12
        frame.valign = Align.START
        listContainer = Clamp()
        listContainer.maximumSize = 900
        listContainer.child = frame
//        gridView = GridView(selection, InstanceGridEntryFactory(instanceList))
        empty = StatusPage()
        empty.title = I18n["main.empty.title"]
        empty.description = I18n["main.empty.description"]
        //TODO empty.setIconName(new Str());
        stack = Stack()
        stack.addChild(listContainer)
//        stack.addChild(gridView)
        stack.addChild(empty)

        val scroll = ScrolledWindow()
        scroll.setPolicy(PolicyType.NEVER, PolicyType.AUTOMATIC)
        scroll.child = stack

        setDefaultSize(720, 360)
        title = "Inceptum"
        titlebar = header
        showMenubar = false
        child = scroll

        generateWindowBody()
        //TODO DropTarget to add mods/instances

        try {
            setupDirWatcher()
        } catch (e: IOException) {
            Log.error(
                "Could not set up watch service, live updates of the instance dir will be unavailable",
                e
            )
        }
    }

    @Throws(IOException::class)
    private fun setupDirWatcher() {
        val isw = InstanceListWatcher()
        addTickCallback { _, _ ->
            try {
                if (isw.poll()) generateWindowBody()
            } catch (e: IOException) {
                Log.error("Could not run update task", e)
            }
            GLib.SOURCE_CONTINUE
        }
        onCloseRequest {
            try {
                isw.close()
            } catch (ignored: IOException) {}
            false
        }
    }

    private fun generateWindowBody() {
//        listButton.visible = !InceptumConfig.listView
//        gridButton.visible = InceptumConfig.listView
        try {
            // Unbind then clear
            instanceListModel.clear()
            instanceList.clear()

            // Add new entries
            instanceList.addAll(InstanceList.ordered())
            instanceListModel.addAll(instanceList.map { it.id }.toTypedArray())

            // Choose view for this amount of entries
//            stack.visibleChild = if (InstanceList.isEmpty()) empty
//            else if (InceptumConfig.listView) listContainer
//            else gridView
            stack.visibleChild = if (InstanceList.isEmpty()) empty else listContainer

            // This is called from a tick callback, so re-render
            stack.queueResize()
            stack.queueDraw()
        } catch (e: IOException) {
            Log.error("Could not generate window body", e)
        }
    }
}
