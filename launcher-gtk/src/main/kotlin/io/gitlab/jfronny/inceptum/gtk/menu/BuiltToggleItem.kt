package io.gitlab.jfronny.inceptum.gtk.menu

import org.gnome.gio.MenuItem
import org.gnome.gio.SimpleAction
import org.gnome.glib.Variant

class BuiltToggleItem(action: SimpleAction, menuItem: MenuItem?) : BuiltMenuItem(action, menuItem) {
    var state: Boolean
        get() = action.state!!.boolean
        set(state) {
            action.state = Variant.boolean_(state)
        }

    fun toggle(): Boolean {
        val toggled = !state
        state = toggled
        return toggled
    }
}
