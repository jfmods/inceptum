package io.gitlab.jfronny.inceptum.gtk.control.settings

import io.gitlab.jfronny.inceptum.gtk.control.ILabel
import io.gitlab.jfronny.inceptum.gtk.control.KDropDown
import io.gitlab.jfronny.inceptum.gtk.control.KEntry
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import io.gitlab.jfronny.inceptum.gtk.util.margin
import org.gnome.gtk.*
import org.jetbrains.annotations.PropertyKey
import java.util.function.Consumer
import java.util.function.DoubleConsumer
import java.util.function.IntConsumer

class IRow(
    title: @PropertyKey(resourceBundle = I18n.BUNDLE) String,
    subtitle: @PropertyKey(resourceBundle = I18n.BUNDLE) String?,
    vararg args: Any?
) : Box(Orientation.HORIZONTAL, 40) {
    init {
        margin = 8
        val head: Widget
        val lab = ILabel(title, *args)
        lab.halign = Align.START
        if (subtitle != null) {
            val headB = Box(Orientation.VERTICAL, 0)
            headB.append(lab)
            val lab1 = ILabel(subtitle, ILabel.Mode.SUBTITLE, *args)
            lab1.halign = Align.START
            headB.append(lab1)
            head = headB
        } else {
            head = lab
        }
        head.halign = Align.START
        head.valign = Align.CENTER
        append(head)
    }

    fun setButton(text: @PropertyKey(resourceBundle = I18n.BUNDLE) String, action: Button.ClickedCallback?): Button =
        Button.withLabel(I18n[text]).apply {
            packSmallEnd()
            onClicked(action)
        }

    fun setDropdown(options: Array<String>, defaultIndex: Int, changed: IntConsumer): KDropDown<String> =
        KDropDown(options, { it } , defaultIndex).apply {
            onChange(changed)
            packSmallEnd()
        }

    fun setSwitch(value: Boolean, changed: Consumer<Boolean>): Switch =
        Switch().apply {
            packSmallEnd()
            active = value
            onStateSet { state: Boolean ->
                changed.accept(state)
                false
            }
        }

    fun setSpinButton(value: Double, min: Double, max: Double, step: Double, changed: DoubleConsumer): SpinButton =
        SpinButton.withRange(min, max, step).apply {
            packSmallEnd()
            this.value = value
            onValueChanged { changed.accept(this.value) }
        }

    fun setEntry(value: String?, changed: Consumer<String>): KEntry =
        KEntry(value).apply {
            hexpand = true
            valign = Align.CENTER
            halign = Align.FILL
            onChange(changed)
            append(this)
        }

    private fun Widget.packSmallEnd() {
        firstChild!!.hexpand = true
        valign = Align.CENTER
        halign = Align.END
        this@IRow.append(this)
    }
}