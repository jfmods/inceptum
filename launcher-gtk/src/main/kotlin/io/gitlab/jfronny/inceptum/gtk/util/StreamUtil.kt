package io.gitlab.jfronny.inceptum.gtk.util

import java.util.stream.Stream

inline fun <reified T> Stream<T>.toTypedArray(): Array<T> = toArray(::arrayOfNulls)