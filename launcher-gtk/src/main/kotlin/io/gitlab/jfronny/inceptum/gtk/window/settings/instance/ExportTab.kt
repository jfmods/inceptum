package io.gitlab.jfronny.inceptum.gtk.window.settings.instance

import io.gitlab.jfronny.inceptum.common.Utils
import io.gitlab.jfronny.inceptum.gtk.control.settings.SectionedSettingsTab
import io.gitlab.jfronny.inceptum.gtk.schedule
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import io.gitlab.jfronny.inceptum.gtk.window.dialog.ProcessStateWatcherDialog
import io.gitlab.jfronny.inceptum.launcher.system.exporter.Exporter
import io.gitlab.jfronny.inceptum.launcher.system.exporter.Exporters
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import io.gitlab.jfronny.inceptum.launcher.util.ProcessState
import org.gnome.gio.Cancellable
import org.gnome.gtk.*
import java.nio.file.Path

class ExportTab(window: InstanceSettingsWindow) : SectionedSettingsTab<InstanceSettingsWindow>(window) {
    private val instance: Instance = window.instance
    init {
        section(null) {
            row("instance.settings.export.version", "instance.settings.export.version.subtitle") {
                setEntry(instance.meta.instanceVersion) {
                    instance.meta.instanceVersion = it
                    instance.writeMeta()
                }
            }
            for (exporter in Exporters.EXPORTERS) {
                row("instance.settings.export.title", "instance.settings.export.subtitle", exporter.name, exporter.fileExtension) {
                    setButton("instance.settings.export") {
                        run {
                            val dialog = FileDialog()
                            dialog.title = I18n["instance.settings.export.dialog.title", exporter.name]
                            val filters = AnyFilter()
                            val filter = FileFilter()
                            filter.name = exporter.name + " Pack"
                            filter.addPattern("*." + exporter.fileExtension)
                            filters.append(filter)
                            dialog.filters = filters
                            dialog.initialName = exporter.getDefaultFileName(instance)
                            dialog.save(window, Cancellable()) { _, res, _ ->
                                export(exporter, Path.of(dialog.saveFinish(res)?.path ?: return@save))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun export(exporter: Exporter<*>, path: Path) {
        val state = ProcessState(Exporters.STEP_COUNT, "Initializing...")
        ProcessStateWatcherDialog.show(
            window,
            I18n["instance.settings.export.dialog.title", exporter.name],
            I18n["instance.settings.export.dialog.error", instance.name],
            state
        ) {
            exporter.generate(state, instance, path)
            schedule {
                val success = MessageDialog(
                    window,
                    setOf(DialogFlags.MODAL, DialogFlags.DESTROY_WITH_PARENT),
                    MessageType.INFO,
                    ButtonsType.NONE,
                    I18n["instance.settings.export.dialog.success", instance.name, path.toString()]
                )
                success.title = I18n["instance.settings.export.dialog.success.title"]
                success.addButton(I18n["show"], ResponseType.OK.value)
                success.addButton(I18n["ok"], ResponseType.CANCEL.value)
                success.onResponse { responseId1: Int ->
                    when (ResponseType.of(responseId1)) {
                        ResponseType.OK -> {
                            success.close()
                            Utils.openFile(path.toFile())
                        }

                        ResponseType.CLOSE, ResponseType.CANCEL -> success.close()
                        ResponseType.DELETE_EVENT -> success.destroy()
                        else -> {}
                    }
                }
                success.visible = true
            }
        }
    }
}
