package io.gitlab.jfronny.inceptum.gtk.control

import org.gnome.gtk.Entry
import java.util.function.Consumer

class KEntry(value: String? = ""): Entry() {
    private val onChange = ArrayList<Consumer<String>>()

    init {
        text = value ?: ""
        onChanged { onChange.forEach { it.accept(text) } }
    }

    fun onChange(changed: Consumer<String>) {
        onChange.add(changed)
    }
}