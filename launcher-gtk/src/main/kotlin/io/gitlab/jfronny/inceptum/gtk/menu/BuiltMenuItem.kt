package io.gitlab.jfronny.inceptum.gtk.menu

import org.gnome.gio.MenuItem
import org.gnome.gio.SimpleAction
import org.gnome.gio.ThemedIcon

abstract class BuiltMenuItem protected constructor(action: SimpleAction, protected val menuItem: MenuItem?) {
    protected val action: SimpleAction

    init {
        this.action = action
    }

    var enabled: Boolean
        get() = action.enabled
        set(enabled) {
            action.enabled = enabled
        }

    var iconName: String?
        set(iconName) { menuItem!!.setIcon(ThemedIcon(iconName)) }
        get() = throw NotImplementedError()
}
