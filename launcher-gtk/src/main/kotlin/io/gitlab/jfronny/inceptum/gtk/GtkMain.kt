package io.gitlab.jfronny.inceptum.gtk

import io.gitlab.jfronny.inceptum.common.BuildMetadata
import io.gitlab.jfronny.inceptum.common.MetaHolder
import io.gitlab.jfronny.inceptum.gtk.util.GtkLog
import io.gitlab.jfronny.inceptum.gtk.util.Log
import io.gitlab.jfronny.inceptum.gtk.window.MainWindow
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv
import io.gitlab.jfronny.inceptum.launcher.api.account.AccountManager
import io.gitlab.jfronny.inceptum.launcher.system.instance.InstanceList
import org.gnome.gio.ApplicationFlags
import org.gnome.glib.GLib
import org.gnome.gtk.*
import java.io.IOException
import java.util.*
import kotlin.system.exitProcess

object GtkMain {
    const val ID = "io.gitlab.jfronny.inceptum"
    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        GLib.logSetWriterFunc(GtkLog)
        LauncherEnv.initialize(GtkEnvBackend)
        Log.info("Launching Inceptum v" + BuildMetadata.VERSION)
        Log.info("Loading from " + MetaHolder.BASE_PATH)
        exitProcess(try {
            showGui(args)
        } catch (_: Throwable) {
            -1
        } finally {
            LauncherEnv.terminate()
        })
    }

    fun showGui(args: Array<String>): Int = setupApplication(args) {
        //TODO update check
        AccountManager.loadAccounts()
        try {
            InstanceList.forEach<IOException> { it.mds.start() }
        } catch (e: IOException) {
            Log.error("Could not initialize MDS", e)
        }
        GtkMenubar.create(this)
        val window = MainWindow(this)
        window.visible = true
        GtkEnvBackend.dialogParent = window
        window.onCloseRequest {
            GtkEnvBackend.dialogParent = null
            this.quit()
            false
        }
    }

    fun setupApplication(args: Array<String>, onActivate: Application.() -> Unit): Int {
        val app = Application(ID, ApplicationFlags.FLAGS_NONE)
        app.onActivate {
            GLib.idleAdd(GLib.PRIORITY_DEFAULT_IDLE) {
                runScheduledTasks()
                true
            }
            onActivate(app)
        }
        return app.run(args)
    }
}