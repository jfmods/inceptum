package io.gitlab.jfronny.inceptum.gtk

import io.gitlab.jfronny.inceptum.gtk.util.Log
import java.util.*

private val SCHEDULED: Queue<Runnable> = ArrayDeque()

fun schedule(task: Runnable) {
    SCHEDULED.add(task)
}

fun backgroundTask(task: Runnable) {
    Thread.ofVirtual().start(task)
}

fun runScheduledTasks() {
    var r: Runnable?
    while (SCHEDULED.poll().also { r = it } != null) {
        try {
            r!!.run()
        } catch (t: Throwable) {
            Log.error("Could not run scheduled task", t)
        }
    }
}