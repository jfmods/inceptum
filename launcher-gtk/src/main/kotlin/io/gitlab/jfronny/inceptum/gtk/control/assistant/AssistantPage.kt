package io.gitlab.jfronny.inceptum.gtk.control.assistant

import org.gnome.gtk.AssistantPageType
import org.gnome.gtk.Box
import org.gnome.gtk.Orientation

class AssistantPage(val title: String, val type: AssistantPageType): Box(Orientation.VERTICAL, 8) {
    lateinit var setComplete: (Boolean) -> Unit
    private val onOpen = ArrayList<Runnable>()
    fun onOpen(action: Runnable) {
        onOpen.add(action)
    }

    fun emitOpen() = onOpen.forEach { it.run() }
}