package io.gitlab.jfronny.inceptum.gtk.util

import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance

operator fun List<Instance>.get(id: String) = firstOrNull { it.id == id }