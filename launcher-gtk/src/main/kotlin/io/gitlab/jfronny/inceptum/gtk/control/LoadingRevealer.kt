package io.gitlab.jfronny.inceptum.gtk.control

import org.gnome.gtk.Align
import org.gnome.gtk.ProgressBar
import org.gnome.gtk.Revealer
import org.gnome.gtk.RevealerTransitionType

class LoadingRevealer: Revealer() {
    private val progressBar: ProgressBar

    init {
        transitionType = RevealerTransitionType.CROSSFADE
        revealChild = false
        vexpand = false
        hexpand = false
        valign = Align.CENTER
        halign = Align.FILL
        progressBar = ProgressBar().apply {
            hexpand = true
            vexpand = false
            addCssClass("osd")
        }
        child = progressBar
    }

    fun setRunning(state: Boolean) {
        revealChild = state
    }

    fun setProgress(progress: Double) {
        progressBar.fraction = progress.coerceIn(0.0, 1.0)
    }

    fun pulse() {
        progressBar.pulse()
    }
}