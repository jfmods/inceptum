package io.gitlab.jfronny.inceptum.gtk.window.dialog

import io.gitlab.jfronny.inceptum.common.Utils
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import io.gitlab.jfronny.inceptum.gtk.util.Log
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAuthAPI
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAuthServer
import org.gnome.gtk.*
import java.net.URI
import java.net.URISyntaxException

class MicrosoftLoginDialog(
    parent: Window?,
    account: MicrosoftAccount? = null,
    onClose: Runnable? = null
) : MessageDialog(
    parent,
    flags(parent != null),
    MessageType.QUESTION,
    ButtonsType.CLOSE,
    I18n["auth.description"]
) {
    constructor(parent: Window?, onClose: Runnable?) : this(parent, null, onClose)

    init {
        title = I18n["auth.title"]
        val server = MicrosoftAuthServer(account)
        try {
            server.start()
        } catch (e: Exception) {
            Log.error("Could not start mc login server", e)
        }
        val finalize = Runnable {
            server.close()
            onClose?.run()
        }
        onResponse { responseId: Int ->
            when (ResponseType.of(responseId)) {
                ResponseType.CLOSE, ResponseType.CANCEL -> {
                    finalize.run()
                    close()
                }

                ResponseType.DELETE_EVENT -> {
                    finalize.run()
                    destroy()
                }

                else -> Log.error("Unexpected response type: $responseId")
            }
        }
        val btn = Button.withLabel(I18n["auth.open-browser"])
        (messageArea as Box).append(btn)
        btn.onClicked {
            try {
                Utils.openWebBrowser(URI(MicrosoftAuthAPI.MICROSOFT_LOGIN_URL))
            } catch (e: URISyntaxException) {
                Log.error("Could not open browser", e)
            }
        }
        onCloseRequest {
            finalize.run()
            false
        }
    }

    companion object {
        private fun flags(modal: Boolean): Set<DialogFlags> {
            return if (!modal) setOf(DialogFlags.DESTROY_WITH_PARENT) else setOf(DialogFlags.DESTROY_WITH_PARENT, DialogFlags.MODAL)
        }
    }
}
