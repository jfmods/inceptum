package io.gitlab.jfronny.inceptum.gtk

import io.gitlab.jfronny.commons.StringFormatter
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import io.gitlab.jfronny.inceptum.gtk.util.Log
import io.gitlab.jfronny.inceptum.gtk.window.dialog.MicrosoftLoginDialog
import io.gitlab.jfronny.inceptum.gtk.window.dialog.StringInputDialog
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv.EnvBackend
import io.gitlab.jfronny.inceptum.launcher.api.account.MicrosoftAccount
import org.gnome.gio.Cancellable
import org.gnome.gtk.*
import java.util.function.Consumer

object GtkEnvBackend : EnvBackend {
    var dialogParent: Window? = null

    override fun showError(message: String, title: String) {
        Log.error(message)
        simpleDialog(message, title, null, null)
    }

    override fun showError(message: String, t: Throwable) {
        simpleDialog(StringFormatter.toString(t), message, null, null)
    }

    override fun showInfo(message: String, title: String) {
        Log.info(message)
        simpleDialog(message, title, null, null)
    }

    override fun showOkCancel(message: String, title: String, ok: Runnable, cancel: Runnable, defaultCancel: Boolean) {
        Log.info(message)
        simpleDialog(message, title, ok, cancel)
    }

    override fun getInput(
        prompt: String,
        details: String,
        defaultValue: String,
        ok: Consumer<String>,
        cancel: Runnable
    ) = schedule {
        val dialog = StringInputDialog(
            dialogParent,
            if (dialogParent == null) setOf(DialogFlags.DESTROY_WITH_PARENT) else setOf(DialogFlags.DESTROY_WITH_PARENT, DialogFlags.MODAL),
            MessageType.QUESTION,
            ButtonsType.OK_CANCEL,
            details,
            defaultValue
        )
        dialog.title = prompt
        dialog.onResponse(processResponses(dialog, { ok.accept(dialog.input) }, cancel))
        dialog.visible = true
    }

    override fun showLoginRefreshPrompt(account: MicrosoftAccount) =
        schedule { MicrosoftLoginDialog(dialogParent, account).visible = true }

    private fun simpleDialog(
        markup: String,
        title: String,
        ok: Runnable?,
        cancel: Runnable?
    ) = schedule { simpleDialog(dialogParent, markup, title, ok, cancel) }

    fun simpleDialog(
        parent: Window?,
        markup: String,
        title: String,
        ok: Runnable?,
        cancel: Runnable?
    ) {
        val dialog = AlertDialog.builder()
            .setMessage(title)
            .setDetail(markup)
            .setModal(true)
        when {
            cancel == null -> dialog.setButtons(arrayOf(I18n["ok"]))
                .setDefaultButton(0)
                .setCancelButton(-1)
            ok == null -> dialog.setButtons(arrayOf("Cancel"))
                .setDefaultButton(-1)
                .setCancelButton(0)
            else -> dialog.setButtons(arrayOf("OK", "Cancel"))
                .setDefaultButton(0)
                .setCancelButton(1)
        }
        dialog.build().apply {
            choose(parent, Cancellable()) { _, res, _ ->
                val result = chooseFinish(res)
                val cancelIdx = cancelButton
                val defaultIdx = defaultButton
                if (result == cancelIdx) cancel?.run()
                if (result == defaultIdx) ok?.run()
            }
        }
    }

    private fun processResponses(dialog: Dialog, ok: Runnable?, cancel: Runnable?): Dialog.ResponseCallback = Dialog.ResponseCallback { responseId: Int ->
        when (ResponseType.of(responseId)) {
            ResponseType.OK -> {
                dialog.close()
                ok?.run()
            }

            ResponseType.CLOSE, ResponseType.CANCEL -> {
                dialog.close()
                cancel?.run()
            }

            ResponseType.DELETE_EVENT -> dialog.destroy()
            else -> Log.error("Unexpected response type: $responseId")
        }
    }
}
