package io.gitlab.jfronny.inceptum.gtk.control

import io.gitlab.jfronny.inceptum.gtk.GtkMain
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import org.gnome.gtk.CssProvider
import org.gnome.gtk.Gtk
import org.gnome.gtk.Label
import org.jetbrains.annotations.PropertyKey

class ILabel(
    str: @PropertyKey(resourceBundle = I18n.BUNDLE) String,
    mode: Mode,
    vararg args: Any?
) : Label(I18n.get(str, *args)) {
    constructor(str: @PropertyKey(resourceBundle = I18n.BUNDLE) String, vararg args: Any?) : this(str, Mode.NORMAL, *args)

    init {
        theme(this, mode)
    }

    enum class Mode {
        NORMAL,
        HEADING,
        SUBTITLE
    }

    companion object {
        private val provider by lazy {
            val provider = CssProvider()
            try {
                GtkMain::class.java.classLoader.getResourceAsStream("inceptum.css")!!
                    .use {
                        val bytes = it.readAllBytes()
                        provider.loadFromData(String(bytes), bytes.size.toLong())
                    }
            } catch (t: Throwable) {
                throw RuntimeException(t)
            }
            provider
        }

        fun theme(label: Label, mode: Mode) = when (mode) {
            Mode.HEADING -> label.addCssClass("heading")
            Mode.SUBTITLE -> {
                label.addCssClass("jf-subtitle")
                label.styleContext.addProvider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
            }

            Mode.NORMAL -> {}
        }
    }
}
