package io.gitlab.jfronny.inceptum.gtk.control.assistant

import io.gitlab.jfronny.inceptum.gtk.util.Log
import org.gnome.gtk.Application
import org.gnome.gtk.Assistant
import org.gnome.gtk.AssistantPageType

open class KAssistant(app: Application) : Assistant() {
    private val pages = ArrayList<AssistantPage>()

    init {
        application = app
        onPrepare { next ->
            val page = pages.firstOrNull { it.handle() == next.handle() }
            if (page == null) {
                Log.error("Unknown page opened in assistant")
            } else {
                page.emitOpen()
            }
        }
    }

    fun page(title: String, type: AssistantPageType, setup: AssistantPage.() -> Unit): Int {
        val page = AssistantPage(title, type)
        val idx = appendPage(page)
        pages.add(page)
        page.setComplete = { setPageComplete(page, it) }
        page.setup()
        setPageType(page, page.type)
        setPageTitle(page, page.title)
        return idx
    }
}