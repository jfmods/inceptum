package io.gitlab.jfronny.inceptum.gtk.window.settings.instance

import io.gitlab.jfronny.commons.concurrent.AsyncRequest
import io.gitlab.jfronny.commons.concurrent.VoidFuture
import io.gitlab.jfronny.inceptum.gtk.backgroundTask
import io.gitlab.jfronny.inceptum.gtk.control.ILabel
import io.gitlab.jfronny.inceptum.gtk.control.KDropDown
import io.gitlab.jfronny.inceptum.gtk.control.KSignalListItemFactory
import io.gitlab.jfronny.inceptum.gtk.control.LoadingRevealer
import io.gitlab.jfronny.inceptum.gtk.control.settings.SettingsTab
import io.gitlab.jfronny.inceptum.gtk.util.*
import io.gitlab.jfronny.inceptum.launcher.LauncherEnv
import io.gitlab.jfronny.inceptum.launcher.api.CurseforgeApi
import io.gitlab.jfronny.inceptum.launcher.api.ModrinthApi
import io.gitlab.jfronny.inceptum.launcher.model.modrinth.ModrinthProjectType
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import io.gitlab.jfronny.inceptum.launcher.system.mds.Mod
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModManager
import io.gitlab.jfronny.inceptum.launcher.system.instance.ModPath
import io.gitlab.jfronny.inceptum.launcher.system.mds.ModsDirScanner
import io.gitlab.jfronny.inceptum.launcher.system.mds.ScanStage
import io.gitlab.jfronny.inceptum.launcher.system.source.CurseforgeModSource
import io.gitlab.jfronny.inceptum.launcher.system.source.ModSource
import io.gitlab.jfronny.inceptum.launcher.system.source.ModrinthModSource
import org.gnome.adw.ActionRow
import org.gnome.adw.Leaflet
import org.gnome.adw.LeafletTransitionType
import org.gnome.glib.GLib
import org.gnome.gtk.*
import org.jetbrains.annotations.PropertyKey
import java.util.concurrent.ForkJoinPool
import kotlin.jvm.optionals.getOrNull

class ModsTab(window: InstanceSettingsWindow) : SettingsTab<Leaflet, InstanceSettingsWindow>(window, Leaflet(), false) {
    private val instance: Instance = window.instance
    private val mds: ModsDirScanner = instance.mds
    private val listModel: StringList
    private val loadingRevealer = LoadingRevealer().apply {
        halign = Align.CENTER
        valign = Align.START
    }
    private val descriptionLabel: ILabel

    private var page: Page = Page.LOCAL

    enum class Page {
        LOCAL, MODRINTH, CURSEFORGE
    }

    init {
        content.apply {
            //TODO consider filter panel via Flap
            homogeneous = false
            canNavigateBack = true
            transitionType = LeafletTransitionType.OVER
            var vsblChld: Box?
            append(Box(Orientation.VERTICAL, 6).apply {
                vsblChld = this
                append(
                    KDropDown(
                        "instance.settings.mods.local",
                        "instance.settings.mods.modrinth",
                        "instance.settings.mods.curseforge"
                    ).apply {
                        onChange { newFilter ->
                            when (newFilter) {
                                0 -> switchTo(Page.LOCAL)
                                1 -> switchTo(Page.MODRINTH)
                                2 -> switchTo(Page.CURSEFORGE)
                            }
                        }
                    })
                append(Entry().apply {
                    setIconFromIconName(EntryIconPosition.PRIMARY, "edit-find-symbolic")
                    onChanged { updateSearch(text) }
                })
                listModel = StringList(arrayOf())
                append(Overlay().apply {
                    child = ModsListView(listModel)
                    addOverlay(loadingRevealer)
                })
            })
            visibleChild = vsblChld!!
            append(Separator(Orientation.VERTICAL)).navigatable = false
            append(Box(Orientation.VERTICAL, 6).apply {
                descriptionLabel = ILabel("instance.settings.mods.select")
                append(descriptionLabel)
                //TODO content
            })

            var previous: List<String> = emptyList()
            addTickCallback { _, _ ->
                val toShow = mutableListOf<String>()
                var contentChanged = false
                fun discover(state: ModState) {
                    if (mods.put(state.name, state) != state) {
                        contentChanged = true
                    }
                    toShow.add(state.name)
                }
                if (page == Page.LOCAL) {
                    loadingRevealer.setRunning(!mds.isComplete(ScanStage.DISCOVER))
                    val mods = window.instance.mds.mods // avoid instance.mods to allow rendering before MDS is finished
                    loadingRevealer.setProgress((mods.filter { mds.hasScanned(it) }.size.toDouble() / mods.size))
                    for (mod in mods) {
                        //TODO improve this search
                        if (mod.name.contains(currentSearchString, true)) {
                            discover(ModState.Installed(mod))
                        }
                    }
                } else {
                    loadingRevealer.setRunning(searchResult == null || !mds.isComplete(ScanStage.CROSSREFERENCE))
                    loadingRevealer.pulse()
                    if (searchResult != null) {
                        for (mod in searchResult.orEmpty()) {
                            discover(mod)
                        }
                    }
                }
                if (toShow != previous || contentChanged) {
                    listModel.replaceAll(toShow.toTypedArray())
                    previous = toShow
                }
                GLib.SOURCE_CONTINUE
            }
        }
    }

    private var currentSearchString: String = ""
    private var searchResult: List<ModState>? = null

    //TODO search pagination
    private val search = AsyncRequest({
        searchResult = null
        loadingRevealer.setRunning(true)
        VoidFuture(ForkJoinPool.commonPool().submit {
            val sources: List<ModSource> = when (page) {
                Page.CURSEFORGE -> {
                    //TODO allow user to choose sort mode
                    CurseforgeApi.search(instance.gameVersion, currentSearchString, 0, "Popularity").mapNotNull { cf ->
                        if (isCancelled) return@submit
                        val file =
                            cf.latestFilesIndexes.firstOrNull { it.gameVersion == instance.gameVersion } //TODO support compatible minor versions
                        if (file == null) null
                        else CurseforgeModSource(cf.id, file.fileId)
                    }
                }

                Page.MODRINTH -> {
                    ModrinthApi.search(
                        currentSearchString,
                        0,
                        instance.gameVersion,
                        ModrinthProjectType.mod
                    ).hits.mapNotNull { mr ->
                        if (isCancelled) return@submit
                        val file = ModrinthApi.getLatestVersions(mr.project_id, instance.gameVersion).best
                        if (file == null) null
                        else ModrinthModSource(file.id)
                    }
                }

                Page.LOCAL -> listOf()
            }
            searchResult = sources.map { ModState(it) }
        })
    }, {
        loadingRevealer.setRunning(false)
    })

    fun switchTo(page: Page) {
        listModel.clear()
        mods.clear()
        this.page = page
        updateSearch(currentSearchString)
    }

    fun updateSearch(search: String) {
        currentSearchString = search
        backgroundTask { this.search.request() }
    }

    fun selectMod(mod: ModState) {
        //TODO detailed menu for version selection, ...
        descriptionLabel.markup = mod.description
    }

    inner class ModsListView(model: StringList) : ListView(ModsListSelectionModel(model), ModsListItemFactory()) {
        init {
            vscrollPolicy = ScrollablePolicy.NATURAL
            addCssClass("navigation-sidebar")
            vexpand = true
            showSeparators = true
        }
    }

    inner class ModsListSelectionModel(model: StringList) : SingleSelection<StringObject>(model) {
        init {
            autoselect = false
            onSelectionChanged { position, _ ->
                if (position != -1) {
                    val v = selectedItem?.string
                    if (v != null) {
                        mods[v]?.let { selectMod(it) }
                    }
                }
            }
        }
    }

    private val mods = LinkedHashMap<String, ModState>()

    //TODO https://gitlab.gnome.org/World/gfeeds/-/blob/master/data/ui/sidebar_listbox_row.blp
    inner class ModsListItemFactory : KSignalListItemFactory<Decomposed, ActionRow>() {
        override fun setup(): ActionRow {
            val row = ActionRow()

            val quickAction = Button.fromIconName("folder-download-symbolic")
            quickAction.addCssClass("flat")
            quickAction.tooltipText = I18n["instance.settings.mods.download"]

            row.addSuffix(quickAction)
            row.fixSubtitle()

            return row
        }

        override fun BindContext.bind(row: ActionRow, data: Decomposed) {
            row.title = data.mod!!.name.escapedMarkup
            row.subtitle = data.mod.summary.escapedMarkup
            fun setupQuickAction(
                iconName: String,
                description: @PropertyKey(resourceBundle = I18n.BUNDLE) String,
                handler: Button.ClickedCallback
            ) {
                data.quickAction.iconName = iconName
                data.quickAction.tooltipText = I18n[description]
                registerForUnbind(data.quickAction.onClicked(handler))
            }
            when (data.mod) {
                is ModState.Installed -> if (data.mod.outdated) setupQuickAction(
                    "software-update-available-symbolic",
                    "instance.settings.mods.update"
                ) {
                    data.mod.updates[0]()
                }
                else setupQuickAction("edit-delete-symbolic", "instance.settings.mods.delete") {
                    data.mod.remove()
                }

                is ModState.Available -> setupQuickAction(
                    "folder-download-symbolic",
                    "instance.settings.mods.download"
                ) {
                    data.mod.install()
                }

                else -> setupQuickAction("dialog-question-symbolic", "instance.settings.mods.unknown") {
                    LauncherEnv.showError(
                        I18n["instance.settings.mods.unknown.description"],
                        I18n["instance.settings.mods.unknown"]
                    )
                }
            }
        }

        override fun UnbindContext.unbind(widget: ActionRow, data: Decomposed) {
            // Not needed currently
        }

        override fun ActionContext.castWidget(widget: Widget): ActionRow = widget as ActionRow
        override fun ActionContext.lookup(id: String, widget: ActionRow): Decomposed {
            val mod = mods[id]
            val suffixes = widget.firstChild!!.lastChild as Box
            val quickAction = suffixes.firstChild as Button
            return Decomposed(mod, quickAction)
        }
    }

    data class Decomposed(val mod: ModState?, val quickAction: Button)

    interface ModState {
        val name: String
        val summary: String
        val description: String

        data class Installed(private val mod: Mod) : ModState {
            private val sources = mod.metadata.sources
            val updates: List<() -> Unit> = sources.mapNotNull { it.value.getOrNull() }.map { { mod.update(it) } }
            val outdated get() = updates.isNotEmpty()
            fun remove() = mod.delete()
            override val name: String = mod.name
            override val summary: String by lazy { mod.metadata.sources.bestSummary }
            override val description: String by lazy {
                sources.bestDescription ?: mod.description.joinToString(separator = "\n")
            }
        }

        data class Available(private val mod: ModSource, private val instance: Instance) : ModState {
            fun install() {
                //TODO thread and possibly show progress
                ModManager.download(mod, instance.modsDir.resolve(mod.shortName + ModPath.EXT_IMOD), instance.mds)
                    .write()
            }

            override val name: String = mod.name
            override val summary: String = mod.summary
            override val description: String = mod.description
        }
    }

    fun ModState(mod: ModSource): ModState {
        val installed = mds.mods.filter { it.metadata.sources.keys.any { mod.projectMatches(it) } }
        return if (installed.isEmpty()) ModState.Available(mod, instance)
        else ModState.Installed(installed[0])
    }
}
