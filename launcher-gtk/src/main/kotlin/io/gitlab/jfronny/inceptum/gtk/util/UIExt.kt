package io.gitlab.jfronny.inceptum.gtk.util

import io.gitlab.jfronny.inceptum.gtk.control.ILabel
import org.gnome.adw.ActionRow
import org.gnome.gtk.EntryBuffer
import org.gnome.gtk.Label
import org.gnome.gtk.MessageDialog
import org.gnome.gtk.Widget

var Widget.margin: Int
    set(value) {
        marginVertical = value
        marginHorizontal = value
    }
    get() = throw NotImplementedError()

var Widget.marginVertical: Int
    set(value) {
        marginTop = value
        marginBottom = value
    }
    get() = throw NotImplementedError()

var Widget.marginHorizontal: Int
    set(value) {
        marginStart = value
        marginEnd = value
    }
    get() = throw NotImplementedError()

var MessageDialog.markup: String
    set(value) { setMarkup(value.escapedMarkup) }
    get() = throw NotImplementedError()

var Label.markup: String
    set(value) { setMarkup(value.escapedMarkup) }
    get() = throw NotImplementedError()

val String.escapedMarkup: String
    get() = replace(unmatchedAnd, "&amp;")

private val unmatchedAnd = Regex("&(?![a-z]{1,6};|#[0-9]+;|#x[0-9A-F]+;)")

fun ActionRow.fixSubtitle() = ILabel.theme(firstChild!!.lastChild!!.prevSibling!!.lastChild as Label, ILabel.Mode.SUBTITLE)

fun EntryBuffer.clear() = deleteText(0, length)