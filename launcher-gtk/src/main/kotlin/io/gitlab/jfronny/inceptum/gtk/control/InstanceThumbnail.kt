package io.gitlab.jfronny.inceptum.gtk.control

import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import org.gnome.gtk.Image
import org.gnome.gtk.Spinner
import org.gnome.gtk.Stack
import java.lang.foreign.MemorySegment

class InstanceThumbnail : Stack {
    private constructor(address: MemorySegment) : super(address)

    constructor() : super() {
        val spinner = Spinner()
        val image = Image()
        val generic = Image()
        spinner.name = SPINNER
        image.name = IMAGE
        generic.name = GENERIC
        generic.setFromIconName("media-playback-start-symbolic") //TODO better default icon
        addNamed(spinner, SPINNER)
        addNamed(image, IMAGE)
        addNamed(generic, GENERIC)
    }

    fun bind(entry: Instance) {
        val spinner = getChildByName(SPINNER) as Spinner
        val image = getChildByName(IMAGE) as Image //TODO
        val generic = getChildByName(GENERIC) as Image
        //TODO mark instance being played
        visibleChild = if (entry.isSetupLocked) {
            spinner
        } else if (false) { // if the instance has an image, load the image data and set it as the visible child
            image
        } else {
            generic
        }
    }

    companion object {
        private const val SPINNER = "spinner"
        private const val IMAGE = "image"
        private const val GENERIC = "generic"

        fun castFrom(stack: Stack): InstanceThumbnail = InstanceThumbnail(stack.handle())
    }
}
