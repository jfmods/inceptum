package io.gitlab.jfronny.inceptum.gtk.util

import org.jetbrains.annotations.PropertyKey
import java.util.*

object I18n {
    const val BUNDLE = "inceptum"
    private val bundle = ResourceBundle.getBundle(BUNDLE)

    operator fun get(key: @PropertyKey(resourceBundle = BUNDLE) String): String =
        bundle.getString(key)

    operator fun get(key: @PropertyKey(resourceBundle = BUNDLE) String, vararg args: Any?): String =
        String.format(bundle.getString(key), *args)
}
