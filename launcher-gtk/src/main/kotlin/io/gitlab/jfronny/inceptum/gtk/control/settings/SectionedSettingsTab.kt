package io.gitlab.jfronny.inceptum.gtk.control.settings

import io.gitlab.jfronny.inceptum.gtk.control.ILabel
import io.gitlab.jfronny.inceptum.gtk.util.I18n
import io.gitlab.jfronny.inceptum.gtk.util.marginHorizontal
import org.gnome.gtk.*
import org.jetbrains.annotations.PropertyKey
import java.util.concurrent.atomic.AtomicInteger

open class SectionedSettingsTab<W : Window>(window: W?): SettingsTab<Box, W>(window, Box(Orientation.VERTICAL, 8), true) {
    init {
        content.marginHorizontal = 24
        content.marginTop = 12
    }

    fun append(child: Widget) = content.append(child)

    protected fun section(title: @PropertyKey(resourceBundle = I18n.BUNDLE) String?, builder: Section.() -> Unit) {
        if (title != null) append(ILabel(title, ILabel.Mode.HEADING))
        val frame = Frame(null as String?)
        val listBox = ListBox()
        listBox.selectionMode = SelectionMode.NONE
        listBox.showSeparators = true
        frame.child = listBox
        val count = AtomicInteger(0)
        builder(object : Section {
            override fun row(title: String, subtitle: String?, vararg args: Any?, build: IRow.() -> Unit): IRow {
                val row = IRow(title, subtitle, *args)
                row(row)
                build(row)
                return row
            }

            override fun row(row: Widget): ListBoxRow {
                listBox.append(row)
                return listBox.getRowAtIndex(count.getAndIncrement())!!
            }

            override fun remove(row: Widget) {
                listBox.remove(row)
                count.decrementAndGet()
            }

            override fun clear() {
                var i = 0
                val len = count.getAndSet(0)
                while (i < len) {
                    listBox.remove(listBox.getRowAtIndex(0))
                    i++
                }
            }
        })
        append(frame)
    }

    interface Section {
        fun row(
            title: @PropertyKey(resourceBundle = I18n.BUNDLE) String,
            subtitle: @PropertyKey(resourceBundle = I18n.BUNDLE) String?,
            vararg args: Any?
        ): IRow = row(title, subtitle, *args, build = {})

        fun row(
            title: @PropertyKey(resourceBundle = I18n.BUNDLE) String,
            subtitle: @PropertyKey(resourceBundle = I18n.BUNDLE) String?,
            vararg args: Any?,
            build: IRow.() -> Unit
        ): IRow

        fun row(row: Widget): ListBoxRow?
        fun remove(row: Widget)
        fun clear()
    }
}