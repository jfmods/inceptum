package io.gitlab.jfronny.inceptum.gtk.control

import io.gitlab.jfronny.inceptum.gtk.util.get
import io.gitlab.jfronny.inceptum.launcher.system.instance.Instance
import org.gnome.gtk.*
import org.gnome.pango.EllipsizeMode
import org.gnome.pango.WrapMode

class InstanceGridEntryFactory(
    private val instanceList: List<Instance>
) : KSignalListItemFactory<InstanceGridEntryFactory.Decomposed, Box>() {
    //TODO better design
    override fun setup(): Box {
        val box = Box(Orientation.VERTICAL, 5)

        val thumbnail = InstanceThumbnail()
        box.append(thumbnail)

        val label = Label(null as String?)
        label.setSizeRequest(192, -1)
        label.maxWidthChars = 20
        label.justify = Justification.CENTER
        label.halign = Align.START
        label.hexpand = true
        label.valign = Align.CENTER
        label.ellipsize = EllipsizeMode.MIDDLE
        label.lines = 3
        label.wrap = true
        label.wrapMode = WrapMode.WORD_CHAR
        label.marginTop = 10
        box.append(label)
//            Label label = new Label(Str.NULL);
//            label.setXalign(0);
//            label.setWidthChars(20);
//            label.setMarginEnd(10);
//            box.append(label);
//
//            Button launch = new Button();
//            launch.setIconName(new Str("computer-symbolic"));
//            launch.setTooltipText(I18n.str("instance.launch"));
//            launch.setHasTooltip(GTK.TRUE);
//            box.append(launch);
//
//            Button openDir = new Button();
//            openDir.setIconName(new Str("folder-symbolic"));
//            openDir.setTooltipText(I18n.str("instance.directory"));
//            openDir.setHasTooltip(GTK.TRUE);
//            box.append(openDir);
        //TODO server launch with network-server-symbolic
        //TODO kill current instance
        return box
    }

    override fun BindContext.bind(widget: Box, data: Decomposed) {
//            Label label = new Label(item.getChild().getFirstChild().cast());
//            Button launch = new Button(label.getNextSibling().cast());
//            Button openDir = new Button(launch.getNextSibling().cast());
//            InstanceList.Entry instance = instanceList.get(ListIndex.toIndex(item));
//            label.setText(new Str(instance.toString()));
//            launch.onClicked(() -> GtkMenubar.launch(instance));
//            openDir.onClicked(() -> Utils.openFile(instance.path().toFile()));
        val thumbnail = InstanceThumbnail.castFrom(widget.firstChild as Stack)
        val label = thumbnail.nextSibling as Label

        val instance = instanceList[data.id]
        thumbnail.bind(instance!!)
        label.text = instance.toString()
    }

    override fun UnbindContext.unbind(widget: Box, data: Decomposed) {
    }

    override fun ActionContext.castWidget(widget: Widget): Box = widget as Box
    override fun ActionContext.lookup(id: String, widget: Box): Decomposed = Decomposed(id)

    class Decomposed(val id: String)
}
