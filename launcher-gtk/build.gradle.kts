plugins {
    inceptum.application
    com.gradleup.shadow
    kotlin("jvm") version libs.versions.kotlin
    kotlin("plugin.sam.with.receiver") version libs.versions.kotlin
}

application {
    mainClass.set("io.gitlab.jfronny.inceptum.gtk.GtkMain")
}

samWithReceiver {
    annotation("io.gitlab.jfronny.commons.SamWithReceiver")
}

dependencies {
    implementation(libs.bundles.javagi)
    implementation(projects.launcher)
}

tasks.shadowJar {
    mergeServiceFiles()
}

tasks.runShadow {
    if (project.hasProperty("showcase")) {
        environment("GTK_THEME", "Adwaita")
        environment("GDK_BACKEND", "broadway")
        environment("BROADWAY_DISPLAY", ":5")
        var proc: Process? = null
        doFirst {
            proc = Runtime.getRuntime().exec(arrayOf("gtk4-broadwayd", ":5"))
            Runtime.getRuntime().exec(arrayOf("xdg-open", "http://127.0.0.1:8085"))
            Thread.sleep(1000)
        }
        doLast {
            if (proc != null) Runtime.getRuntime().exec(arrayOf("kill", proc!!.pid().toString()))
        }
    }
    workingDir = rootProject.projectDir
    environment("GTK_DEBUG", "interactive") // interactive:actions
    jvmArgs("--enable-preview", "--enable-native-access=ALL-UNNAMED")
}
