# About

Inceptum is advanced FOSS Launcher for Minecraft written in Java

Features:

- Syncing instances between multiple clients and servers through git
- Easy fabric mod management (installing, removing, updating) from CurseForge and Modrinth through a GUI
- ".imod" format to keep mod jars out of git
- Exports to common formats
- Cross-Platform
- Automatic updates through Inceptum Wrapper
- Support for running without OpenGL drivers
- Proxy support
- Integrated headless server launcher for inceptum packs
- LWJGL-based GUI (not a web browser!)
- Uses Mojangs JVMs to launch instances
- Needs nothing but a JVM, OpenGL or mesa and an internet connection during setup
