import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
    title: "Inceptum",
    description: "Inceptum Docs",
    themeConfig: {
        // https://vitepress.dev/reference/default-theme-config
        sidebar: [
            { text: 'About', link: '/index.md' },
            { text: 'Installing', link: '/Installing.md' },
            { text: 'CLI', link: '/Commands.md' },
            { text: 'Syncing instances', link: '/Syncing.md' },
            { text: 'File Formats', link: '/FileFormats.md' },
            { text: 'Modules', link: '/Modules.md' }
        ],

        socialLinks: [
            { icon: 'github', link: 'https://git.frohnmeyer-wds.de/JfMods/Inceptum' }
        ],

        search: {
            provider: "local"
        },
    },
    outDir: '../public',
    base: '/JfMods/Inceptum/',
    mpa: true,
    lang: 'en-US',
    sitemap: {
        hostname: 'https://pages.frohnmeyer-wds.de/JfMods/Inceptum/'
    }
})
