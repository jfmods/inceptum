# File Formats

Inceptum uses several json formats to store metadata and configs.
All of these are subject to change, though automatic migrations will likely be provided.

## inceptum.json (Main Config)

```json5
{
  // Whether to show snapshots in the version selector for new instances
  "snapshots": false,
  // Whether to launch the GUI in dark mode
  // Configurable in Settings->Dark Theme
  "darkTheme": true,
  // Whether to require an account to launch the game
  // Intended to allow running the game from USB sticks on constrained networks
  "enforceAccount": false,
  // The currently selected account
  // Used to launch the game
  "lastAccount": "some UUID",
  // The last name used for an offline session
  "offlineAccountLastName": "some name",
  // The update channel. Either "CI" or "Stable"
  // I personally recommend the CI channel as it gets the latest fixes and features quicker
  "channel": "CI",
  // The author name to add to packs where the metadata format requires specifying one
  "authorName": "Inceptum"
}
```

## accounts.json

Do not EVER use this file manually!
NEVER upload it anywhere!
It stores your minecraft account login and CAN BE USED TO IMPERSONATE YOU!

## instance.json (Instance Metadata)

Please note that all entries except for "version" are optional

```json5
{
  // The version to use for launching this
  // Can be a fabric loader version (as seen here) or a normal minecraft version (like "1.17.1")
  "version": "fabric-loader-0.12.12-1.17.1",
  // A custom java executable to use for launching this instance
  "java": "/path/to/java",
  // Sets the initial size of the Java heap
  // In bytes (this is 2MB)
  "minMem": 2097152,
  // Sets the maximum size to which the Java heap can grow
  // In bytes (this is 2GB)
  "maxMem": 2147483648,
  // Additional arguments to pass to minecraft
  "arguments": {
    // JVM arguments to add
    // Use this for things like aikars flags or java agents
    "jvm": [
      "-Dsome.argument=value"
    ],
    // Arguments to add when launching as a client
    // Generally not needed, but can be used to do things like launching a server (I think)
    "client": [
      "someArgument"
    ],
    // Arguments to add when launching as a server
    // Things like "nogui" go here
    "server": [
      "nogui"
    ]
  }
}
```

## *.imod (Mod Metadata)

```json5
{
  // Where the JAR file for this mod can be obtained
  "sources": [
    {
      // The type of the source
      // In this case, Modrinth
      "type": "modrinth",
      // The ID of the version on modrinth
      "id": "31ES0yWr"
    },
    {
      // The type of the source
      // In this case, CurseForge
      "type": "curseforge",
      // The ID of the project on CurseForge
      "projectId": 306612,
      // The ID of the file on CurseForge
      "fileId": 3609590
    },
    {
      // The type of the source
      // In this case, a direct download
      "type": "direct",
      // The name of the file this should be saved as
      "fileName": "fabric-api-0.46.1+1.17.jar",
      // The URL to download from
      "url": "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.46.1+1.17/fabric-api-0.46.1+1.17.jar",
      // Dependencies for this file (optional)
      "dependencies": [
        // Dependencies in the same dependency format used int "sources"
      ]
    }
  ],
  // The sha1 hash of this file
  // Used to verify downloads and for modrinth
  "sha1": "a7d86f36c5b27bdb0008a84c3ce91e2b095a2834",
  // The murmur2 hash of this file
  // Used for curseforge
  "murmur2": "1020327834",
  // A list of dependent mods by their file names
  "dependents": [
    "someMod.imod"
  ],
  // A list of dependencies by their file names
  "dependencies": [
    "someOtherMod.imod"
  ],
  // Whether this mod was explicitly installed (used during mod removal)
  "explicit": true
}
```
