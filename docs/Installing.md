# Installing

Inceptum can be installed in a number of ways, all of which are documented below.

## Simple installation

First, download the Inceptum build appropriate for your system:

- [Windows x86_64](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/Inceptum-windows.jar)
- [MacOS x86_64](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/Inceptum-macos.jar) (untested)
- [Linux x86_64](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/Inceptum-linux.jar)

You can also download a [single jar](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/Inceptum.jar)
that contains support for all of these systems (though doing so is not recommended)

Once you have a jar, run it with an [up-to-date java version](https://adoptium.net/).
Inceptum will use your config directory ($XDG_CONFIG_HOME, ~/.config/Inceptum, %APPDATA%\Inceptum or ~/Library/Application Support/Inceptum) for saving instances/caches/etc.
If you want them somewhere else, read on

### Config files in current directory

If the Inceptum jar detects a directory called "run" in the directory it is placed in,
it will use that instead of the users config directory.

Create a directory (= Folder) next to the inceptum jar.

### Config files in a custom location

You may specify the java VM parameter `-Dinceptum.base=$DIRECTORY` to use a custom directory for Inceptum.
If this parameter is specified, all other locations will be ignored.

## Simple installation with updates

To use automatic updates, you must use the Inceptum Wrapper.
Simply launch this [cross-platform jar](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/wrapper.jar)
and Inceptum will launch as described above (though the initial startup may take a bit longer). The same rules for config locations apply.

You may also download the [windows exe](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/wrapper.exe)
which uses fabric-installer-native-bootstrap to locate the JVM used by the official minecraft launcher and launch Inceptum using that.
Please be aware that this is pretty much untested

## Installation from the AUR

Inceptum is available in the AUR as [`inceptum-git`](https://aur.archlinux.org/packages/inceptum-git)
If you use arch linux or a derivative, you may use this package instead of a manual installation.
It also includes a template systemd service which you can copy and customize to launch your minecraft server.
For more information on the syntax of this services config file, go [here](Commands.md)

## Using Inceptum on Windows without OpenGL drivers

Download the portable build from [here](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/artifacts/portable.7z)
This archive includes Inceptum using the Inceptum wrapper, a JVM and a Mesa build for CPU-based graphics.
Please be aware that using this WILL result in worse performance.