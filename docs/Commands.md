# CLI
Inceptum provides a CLI which performs similar functions to the GUI. If you have feature requests, [open an issue](https://helpdesk.frohnmeyer-wds.de/issue?repo=Inceptum&owner=JfMods)
To view up-to-date information on the commands provided by Inceptum, run `inceptum help` or `inceptum help <command>`, this page is intended to explain more advanced features

## The inceptum wrapper

Inceptum Wrapper looks through the libraries dir and launches the latest available Inceptum version.
If it doesn't find a usable version, it will download the latest available version automatically.
Launching is performed through a custom ClassLoader and an internal flag is set to inform the launched version about the wrappers' presence.

## The "batch" command

This command will go through every line in the file provided in its arguments and executes the command written there.
It is intended to be used in scripts that only want to run inceptum once, such as the systemd unit in the AUR package.
The batch command has the additional advantage of allowing caches to stay loaded between commands, making execution faster.
For example,

```
mod update all icesrv
run server restart icesrv
run server restart server2
```

is equivalent to the bash script

```shell
#!/bin/sh
inceptum mod update all icesrv
inceptum run server restart icesrv
inceptum run server restart server2
```
