# Syncing instances

Inceptum supports syncing repositories between instances through git.
This is roughly comparable to updatable modpacks in other launchers.
There are two workflows for this, you can choose one based on your use case.

## Uploading a local pack to a game server

This approach assumes that your intention is to have a game server running Inceptum
and to regularly push updates built and tested in an Inceptum instance on your PC.
To set this up, initialize a repo on your server and clone it locally

Server:

```shell
cd /srv/inceptum/instances
mkdir icesrv
cd icesrv
git init
git config receive.denyCurrentBranch ignore
```

Client:

```shell
git clone inceptum@example.com:/srv/inceptum/instances/icesrv
cd icesrv
git branch --set-upstream-to origin/master
# copy your instance content to your new instance
git add .
git commit -m "Initial commit"
git push
```

After that, you can use a script similar to the following to push changes:

```shell
git push -u origin master
rconc icesrv stop
sleep 1
ssh -t inceptum@example.com "cd /srv/inceptum/instances/icesrv; git reset --hard"
ssh -t admin@example.com "sudo systemctl restart inceptum-icesrv"
```

icesrv is the name of the instance in this example and inceptum-icesrv is a systemd service based on the one included
in the AUR package.

## Providing a pack to multiple users

To do this, create a new repository on a git hosting site (like GitHub or GitLab) and push your local instance there.
Since a git repository is created for every instance, simply following the instructions for pushing an existing
repository or using any graphical tool is enough to set this up.

Users can add an instance created this way in the GUI under File->New Instance->Inceptum.

To update the pack, simply create a new commit and push it to your repository.
Users can then update the pack by pulling the changes into their instance directory through the normal git CLI.
You may also export the pack to other formats to upload it on sites like CurseForge