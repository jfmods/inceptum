plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

dependencies {
    implementation(libs.plugin.shadow)
    implementation(libs.plugin.download)
    implementation(libs.plugin.jf.convention)
    implementation(libs.plugin.jlink)
}