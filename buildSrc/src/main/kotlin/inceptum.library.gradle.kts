plugins {
     id("inceptum.java")
}

publishing {
     publications {
         create<MavenPublication>("mavenJava") {
             from(components["java"])
         }
     }
}