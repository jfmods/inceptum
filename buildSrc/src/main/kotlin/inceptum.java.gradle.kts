plugins {
    jf.java
    `maven-publish`
}

repositories {
    mavenCentral()
    maven("https://maven.frohnmeyer-wds.de/artifacts")
}

val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")
dependencies {
    compileOnly(libs.findLibrary("annotations").orElseThrow())
}

publishing {
    repositories {
        mavenLocal()

        if (rootProject.extra["isPublic"] == true) {
            maven("https://maven.frohnmeyer-wds.de/artifacts") {
                name = "public"

                credentials(PasswordCredentials::class) {
                    username = System.getenv()["MAVEN_NAME"]
                    password = System.getenv()["MAVEN_TOKEN"]
                }
                authentication {
                    create<BasicAuthentication>("basic")
                }
            }
        }
    }
}

afterEvaluate {
    if (hasProperty("offline")) {
        tasks.withType(JavaExec::class) {
            environment("G_ORIGINAL_EXECUTABLE", executable ?: "java")
            val originalMetadata = javaLauncher.get().metadata
            val field = org.gradle.api.internal.provider.AbstractProperty::class.java.getDeclaredField("value")
            field.isAccessible = true
            val customLauncher = object: JavaLauncher {
                override fun getMetadata(): JavaInstallationMetadata = originalMetadata
                override fun getExecutablePath(): RegularFile = rootProject.layout.projectDirectory
                    .dir("buildSrc")
                    .file("java-offline")
            }
            field.set(javaLauncher, org.gradle.api.internal.provider.Providers.of(customLauncher))
        }
    }
}