package io.gitlab.jfronny.inceptum.common.model.maven;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public record Pom(String modelVersion,
                  String groupId,
                  String artifactId,
                  String version,
                  String classifier,
                  String snapshotVersion,
                  @Nullable String packaging,
                  @Nullable List<MavenDependency> dependencies) {
}
