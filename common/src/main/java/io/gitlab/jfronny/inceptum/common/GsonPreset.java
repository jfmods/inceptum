package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.json.JsonReader;
import io.gitlab.jfronny.commons.serialize.json.JsonTransport;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class GsonPreset {
    public static Config CONFIG = new Config();
    public static Api API = new Api();

    public static class Config extends JsonTransport {
        public static <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void configure(Reader reader) {
            reader.setSerializeSpecialFloatingPointValues(true);
            reader.setLenient(true);
        }

        public static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void configure(Writer writer) {
            writer.setSerializeNulls(true);
            writer.setSerializeSpecialFloatingPointValues(true);
            writer.setLenient(true);
            if (writer instanceof JsonWriter jw) {
                jw.setIndent("    ");
                jw.setOmitQuotes(true);
                jw.setNewline("\n");
                jw.setCommentUnexpectedNames(true);
            }
        }

        @Override
        public JsonWriter createWriter(Writer target) throws IOException {
            JsonWriter jw = super.createWriter(target);
            configure(jw);
            return jw;
        }

        @Override
        public JsonReader createReader(Reader source) {
            JsonReader jr = super.createReader(source);
            configure(jr);
            return jr;
        }
    }

    public static class Api extends JsonTransport {
        public static <TEx extends Exception, Reader extends SerializeReader<TEx, Reader>> void configure(Reader reader) {
            reader.setSerializeSpecialFloatingPointValues(true);
            reader.setLenient(true);
        }

        public static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> void configure(Writer writer) {
            writer.setSerializeNulls(false);
            writer.setSerializeSpecialFloatingPointValues(true);
            writer.setLenient(false);
        }

        @Override
        public JsonWriter createWriter(Writer target) throws IOException {
            JsonWriter jw = super.createWriter(target);
            configure(jw);
            return jw;
        }

        @Override
        public JsonReader createReader(Reader source) {
            JsonReader jr = super.createReader(source);
            configure(jr);
            return jr;
        }
    }
}
