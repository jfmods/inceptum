package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.OSUtils;

import java.nio.file.*;

public class MetaHolder {
    public static final Path BASE_PATH;

    static {
        if (System.getProperty("inceptum.base") == null) {
            Path runDir = getPath("run");
            if (!BuildMetadata.IS_RELEASE) BASE_PATH = runDir;
            else if (Files.exists(runDir)) BASE_PATH = runDir;
            else {
                Path configsDir = switch (OSUtils.TYPE) {
                    case WINDOWS -> getPath(System.getenv("APPDATA"));
                    case MAC_OS -> getPath(System.getProperty("user.home")).resolve("Library").resolve("Application Support");
                    case LINUX -> {
                        String s = System.getenv("XDG_CONFIG_HOME");
                        if (s == null)
                            yield getPath(System.getProperty("user.home")).resolve(".config");
                        else
                            yield getPath(s);
                    }
                };
                BASE_PATH = configsDir.resolve("Inceptum");
            }
        } else {
            BASE_PATH = getPath(System.getProperty("inceptum.base"));
        }
    }

    public static final Path ACCOUNTS_PATH = BASE_PATH.resolve("accounts.json");
    public static final Path CONFIG_PATH = BASE_PATH.resolve("inceptum.json5");
    public static final Path WRAPPER_CONFIG_PATH = BASE_PATH.resolve("wrapper.json");
    public static final Path NATIVES_DIR = BASE_PATH.resolve("natives");
    public static final Path FORCE_LOAD_PATH = NATIVES_DIR.resolve("forceload");
    public static final Path LIBRARIES_DIR = BASE_PATH.resolve("libraries");
    public static final Path ASSETS_DIR = BASE_PATH.resolve("assets");
    public static final Path INSTANCE_DIR = BASE_PATH.resolve("instances");
    public static final Path CACHE_DIR = BASE_PATH.resolve("cache");
    private static boolean isWrapper = false;

    private static Path getPath(String text) {
        return Paths.get(text).toAbsolutePath().normalize();
    }

    public static void setWrapperFlag() {
        isWrapper = true;
    }

    public static boolean isWrapper() {
        return isWrapper;
    }
}
