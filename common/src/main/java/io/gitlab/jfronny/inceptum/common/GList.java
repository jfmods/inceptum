package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.serialize.SerializeReader;
import io.gitlab.jfronny.commons.serialize.Token;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;
import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class GList {
    public static <T, TEx extends Exception, Reader extends SerializeReader<TEx, ?>> List<T> read(Reader reader, ThrowingFunction<Reader, T, TEx> read) throws TEx {
        if (reader.isLenient() && reader.peek() != Token.BEGIN_ARRAY) return List.of(read.apply(reader));
        reader.beginArray();
        List<T> res = new LinkedList<>();
        while (reader.hasNext()) {
            if (reader.peek() == Token.NULL) {
                reader.nextNull();
                res.add(null);
            } else res.add(read.apply(reader));
        }
        reader.endArray();
        return res;
    }

    public static <T> void write(JsonWriter writer, List<T> list, ThrowingBiConsumer<T, JsonWriter, IOException> write) throws IOException {
        writer.beginArray();
        for (T t : list) write.accept(t, writer);
        writer.endArray();
    }
}
