package io.gitlab.jfronny.inceptum.common.model.inceptum;

public enum UpdateChannel {
    Stable, CI
}
