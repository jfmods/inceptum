package io.gitlab.jfronny.inceptum.common;

import io.gitlab.jfronny.commons.http.client.HttpClient;
import io.gitlab.jfronny.commons.logger.HotswapLoggerFinder;
import io.gitlab.jfronny.commons.logger.StdoutLogger;
import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;

import java.io.IOException;

public class InceptumEnvironmentInitializer {
    public static void initialize() throws IOException {
        HttpClient.scheduleOnlineCheck();
        HotswapLoggerFinder.updateAllStrategies(InceptumEnvironmentInitializer::defaultFactory);
        HttpClient.setUserAgent("jfmods/inceptum/" + BuildMetadata.VERSION);
        InceptumConfig.load();
    }

    public static SystemLoggerPlus defaultFactory(String name, Module module, System.Logger.Level level) {
        return StdoutLogger.fancy(name, module, level);
    }
}
