package io.gitlab.jfronny.inceptum.launchwrapper.test;

import java.lang.invoke.*;
import java.lang.reflect.Method;

public class InvokeTest {
    public static void main(String[] args) throws Throwable {
        Class<?> klazz = TestClass.class;
        Method mainMethod = klazz.getMethod("main", String[].class);
        MethodHandles.Lookup lookup = MethodHandles.lookup();

        String[] yes = new String[] {"Shesh"};
        Runnable main = (Runnable) LambdaMetafactory.metafactory(
                lookup,
                "run",
                MethodType.methodType(Runnable.class, String[].class),
                MethodType.methodType(Void.TYPE),
                lookup.unreflect(mainMethod),
                MethodType.methodType(Void.TYPE)
        ).getTarget().invokeExact(yes);

        new Thread(main).start();
    }
}
