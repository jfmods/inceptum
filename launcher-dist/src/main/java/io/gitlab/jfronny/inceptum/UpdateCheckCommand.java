package io.gitlab.jfronny.inceptum;

import io.gitlab.jfronny.inceptum.cli.Command;
import io.gitlab.jfronny.inceptum.cli.CommandArgs;
import io.gitlab.jfronny.inceptum.common.*;
import io.gitlab.jfronny.inceptum.common.model.inceptum.UpdateMetadata;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class UpdateCheckCommand extends Command {
    private final boolean install;

    public UpdateCheckCommand() {
        super("Checks for inceptum updates", "", List.of("update"), List.of(
                new UpdateCheckCommand("Automatically install updates", "install", true)
        ));
        install = false;
    }

    private UpdateCheckCommand(String help, String name, boolean install) {
        super(help, "", name);
        this.install = install;
    }

    @Override
    protected void invoke(CommandArgs args) {
        if (install && !MetaHolder.isWrapper()) {
            Utils.LOGGER.error("Automatic updates are not supported without the wrapper");
            return;
        }
        UpdateMetadata update;
        try {
            update = BuildMetadata.IS_PUBLIC ? Updater.getUpdate(true, true) : null;
        } catch (Updater.UpdateCheckException e) {
            Utils.LOGGER.error("Latest update is not compatible: " + e.message);
            return;
        }
        if (update == null) {
            Utils.LOGGER.info("No update was found");
        } else {
            if (install) {
                Utils.LOGGER.info("Installing from " + update);
                try {
                    Updater.update(update, false);
                } catch (IOException | URISyntaxException e) {
                    Utils.LOGGER.error("Could not download update", e);
                }
            } else {
                Utils.LOGGER.info("An update was found: " + update);
            }
        }
    }
}
