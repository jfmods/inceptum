module io.gitlab.jfronny.inceptum.launcher.dist {
    requires io.gitlab.jfronny.inceptum.launcher.imgui;
    requires io.gitlab.jfronny.inceptum.launcher.cli;
}