import io.gitlab.jfronny.scripts.*
import java.nio.file.Files
import java.nio.file.Path

plugins {
    inceptum.`application-standalone`
    org.beryx.jlink
}

application {
    mainClass.set("io.gitlab.jfronny.inceptum.Inceptum")
    mainModule.set("io.gitlab.jfronny.inceptum.launcher.dist")
    applicationName = "Inceptum"
}

dependencies {
    implementation(projects.launcher)
    implementation(projects.launcherCli)
    implementation(projects.launcherImgui)
}

tasks.shadowJar {
    archiveClassifier.set(rootProject.extra["flavorProp"] as String)
    archiveBaseName.set("Inceptum")
    exclude("about.html")
    exclude("plugin.properties")
//    exclude("META-INF/**")
    mergeServiceFiles()
}

(components["java"] as AdhocComponentWithVariants).withVariantsFromConfiguration(configurations["shadowRuntimeElements"]) {
    skip()
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
        }
    }
}

val flavor: String by rootProject.extra
val os = org.gradle.internal.os.OperatingSystem.current()!!
val crosscompile = flavor == "windows" && os.isLinux && project.hasProperty("crosscompile")

val verifyFlavorConfiguration by tasks.registering {
    when (flavor) {
        "macos" -> if (!os.isMacOsX) throw IllegalArgumentException("Cross-compilation to MacOS is unsupported!")
        "windows" -> if (!os.isWindows && !crosscompile) {
            if (os.isLinux) throw IllegalArgumentException("Cross-compilation to Windows is not enabled, please do so to build this")
            else throw IllegalArgumentException("Cross-compilation to Windows from this platform is unsupported!")
        }
        "linux" -> if (!os.isLinux) throw IllegalArgumentException("Cross-compilation to Linux is unsupported!")
        else -> throw IllegalArgumentException("Unexpected flavor: $flavor")
    }
}

tasks.prepareMergedJarsDir { dependsOn(verifyFlavorConfiguration) }
tasks.createMergedModule { dependsOn(verifyFlavorConfiguration) }
tasks.createDelegatingModules { dependsOn(verifyFlavorConfiguration) }
tasks.prepareModulesDir { dependsOn(verifyFlavorConfiguration) }
tasks.jlink { dependsOn(verifyFlavorConfiguration) }
tasks.jlinkZip { dependsOn(verifyFlavorConfiguration) }
tasks.suggestMergedModuleInfo { dependsOn(verifyFlavorConfiguration) }
tasks.jpackageImage { dependsOn(verifyFlavorConfiguration) }
tasks.jpackage { dependsOn(verifyFlavorConfiguration) }

fun computeDebugVersion(): String {
    val time = System.currentTimeMillis() / 1000 / 60 / 5 // 5 minute intervals
    // installer versions MUST be  [0-255].[0-255].[0-65535]
    // in other words,  8,8,16 bits
    val major = (time / (256 * 65536))
    val minor = (time / 65536) % 256
    val patch = time % 65536
    return "$major.$minor.$patch"
}

if (crosscompile) System.setProperty("badass.jlink.jpackage.home", "/root/jpackage-win")
jlink {
    if (crosscompile) javaHome.set("/root/jpackage-win")
    addOptions(
        "--strip-debug",
        "--compress", "2",
        "--no-header-files",
        "--no-man-pages",
        "--verbose"
    )
    launcher {
        name = application.applicationName
    }
    if (crosscompile) targetPlatform("win", "/root/java")
    jpackage {
        imageName = application.applicationName
        if (crosscompile) {
            outputDir = "/root/jpackage-out"
            imageOutputDir = File(outputDir)
            installerOutputDir = File(outputDir)
        }
        appVersion = versionStripped
        // vendor
        when (flavor) {
            "macos" -> {
                installerType = "app-image"
                installerOptions.addAll(listOf(
                    "--mac-package-name", "inceptum"
                ))
            }
            "windows" -> {
                val release = project.hasProperty("release")
                installerType = "msi"
                installerOptions.addAll(listOf(
                    "--win-per-user-install",
                    "--win-dir-chooser",
                    "--win-menu",
                    "--win-upgrade-uuid", if (release) "3cda7403-4c00-4f9f-bcc3-6ff304566633" else "180becd8-a867-40d4-86ef-20949cae68b5" // Update these UUIDs if you fork the project!!!
                ))
                if (!release) appVersion = computeDebugVersion()
                //imageOptions.add("--win-console") // Enable this for debugging
            }
            else -> {
                // might also be able to push rpm with appropriate image
                installerType = "deb"
                installerOptions.addAll(listOf(
                    "--linux-package-name", "inceptum",
                    "--linux-shortcut"
                ))
            }
        }
    }
}

if (crosscompile) {
    tasks.jpackage {
        doLast {
            val src = Path.of("/root/jpackage-out")
            val trg = layout.buildDirectory.dir("jpackage").get().asFile.toPath()
            Files.createDirectories(trg)
            Files.list(src).use {
                it.filter { Files.isRegularFile(it) }.forEach {
                    val t = trg.resolve(it.fileName.toString())
                    println("Moving $it to $t")
                    Files.move(it, t)
                }
            }
        }
    }
}
