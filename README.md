# Inceptum Launcher

An advanced FOSS Launcher for Minecraft written in Java

For documentation on how to use or install Inceptum, please head to the [wiki](https://pages.frohnmeyer-wds.de/JfMods/Inceptum/)

## Licenses

Inceptum utilizes code/libraries/assets from:

- [ATLauncher](https://github.com/ATLauncher/ATLauncher): Microsoft authentication
- [JLHTTP](https://www.freeutils.net/source/jlhttp/): Used in MS Auth
- [MultiMC](https://github.com/MultiMC/Launcher): Used as a reference for some implementations
- [imgui-java](https://github.com/SpaiR/imgui-java): The library used for UI
- [Dear ImGui](https://github.com/ocornut/imgui): Included and wrapped in imgui-java, UI library
- [LWJGL](https://github.com/LWJGL/lwjgl3): Used as a backend for imgui-java
- [java-gi](https://github.com/jwharm/java-gi): The library used for the new UI
- [GTK4](https://www.gtk.org/) (and dependencies): Wrapped in java-gi-generated code, the core UI library
- [gson](https://github.com/google/gson): Used for interacting with various APIs and configs
- [Ubuntu](https://design.ubuntu.com/font/): Used with nerd font symbols as the font
- [meteor-client](https://github.com/MeteorDevelopment/meteor-client): A simple HTTP client
- Several of [my other projects](https://git.frohnmeyer-wds.de/explore/repos)
